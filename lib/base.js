const EventEmitter = require('events').EventEmitter;
var timezoneJS = require('timezone-js');
String.prototype.reverse=function(){return this.split('').reverse().join('');};

class MyBase extends EventEmitter {
	constructor(config) {
		super(); 
		this.config=config;
		Object.assign(this, config);
		this.init();
	}
	init(){
		var self = this;
		self.componentClass = 'MyBase';
	}
	getTime(){
		this.time=new timezoneJS.Date('Europe/Istanbul');
		return this.time;
	}
	_setTime(){
		this.time=new timezoneJS.Date();
	}
	mjd(obj){
		if(typeof obj=='string')
			console.log(this.getFullLocalDateString()+' '+obj);
		else{
			console.log(this.getFullLocalDateString());
			for(let p in obj){
				console.log(p+':'+obj[p]);
			}
		}
	}
	getTimeString(){
		this._setTime();
		return this.time.toString('HH:mm:ss.SSS');
	}
	getFullLocalDateString(){
		this._setTime();
		return this.time.toString('yyyy-MM-dd HH:mm:ss.SSS');
	}
	convertFullLocalDateString(_time,_ms){
		var dt = new timezoneJS.Date(_time);
		return dt.toString('yyyy-MM-dd HH:mm:ss'+(_ms==true?'.SSS':''));
	}
	convertLocalDateString(_time){
		var dt = new timezoneJS.Date(_time,'Europe/Istanbul');
		return dt.toString('yyyy-MM-dd');
	}
	randomIntInc(low, high) {
		return Math.floor(Math.random() * (high - low + 1) + low);
	}
	zfill(num, len) {
		return (Array(len).join('0') + num).slice(-len);
	}
	getIndex(array,key,keyValue){
		if(typeof array.length!='undefined')
			for(let i = 0, len = array.length; i < len; i++){
				if(array[i][key]==keyValue)	return i;
			}
		else
			for(let i in array)
				if(typeof array[i]!='function')
					if(array[i][key]==keyValue)	return i;
		return -1;
	}
	getIndexMulti(array,key,keyValue){
		if(typeof array.length!='undefined')
			for(let i = 0, len = array.length; i < len; i++){
				if(key.indexOf('|')==-1){
					if(array[i][key]==keyValue)	return i;
				}else{
					let f=true;
					let arr_key=key.split('|');
					let arr_val=keyValue.split('|');
					for(let j=0;j<arr_key.length;j++){
						let v_row=array[i][arr_key[j]];
						let v_val=((arr_val[j]=='true'||arr_val[j]=='false')?Boolean(arr_val[j]):arr_val[j]);
						v_row=(v_row==null||v_row=='null')?null:v_row;
						v_val=(v_val==null||v_val=='null')?null:v_val;
						if(v_row!=v_val){
							f=false;
						}
					}
					if(f) return i;
				}
			}
		else
			for(let i in array)
				if(typeof array[i]!='function'){
					if(key.indexOf('|')==-1){
						if(array[i][key]==keyValue)	return i;
					}else{
						let f=true;
						let arr_key=key.split('|');
						let arr_val=keyValue.split('|');
						for(let j=0;j<arr_key.length;j++){
							if(array[i][arr_key[j]]!=arr_val[j]){
								f=false;
							}
						}
						if(f) return i;
					}
				}
		return -1;
	}
	destroy(){
		let t=this;
		var _i;
		t.emit('beforedestroy',t);
		t.removeAllListeners('beforedestroy');
		if (t.relatedItems)
			for (var i = 0, l = t.relatedItems.length; i < l; i++){
				if (_i == t.relatedItems[i]){
					(_i.componentClass && typeof _i.destroy == 'function') ? _i.destroy() : (typeof _i.remove == 'function' ? _i.remove() : null);
				}
			}
				
		t.emit('destroy', t);
		t.emit('afterdestroy', t);
		t.removeAllListeners('destroy');
		t.removeAllListeners('afterdestroy');
	}
	getIp() {
		var os=require('os');
		const name = os.hostname();
		if (name.indexOf('oguzhan') > -1) {
			if (name.indexOf('oguzhan-d') > -1) {
				return '192.168.1.40';
			}
			if (name.indexOf('oguzhan-tb') > -1) {
				return '192.168.1.10';
			}
			return '192.168.1.40';
		}else{
			var i, v, value;
			var networkInterfaces = os.networkInterfaces();
			/* jshint -W015: true */
			interfaces: for (i in networkInterfaces) {
				for (v in i) {
					if (networkInterfaces[i][v] && networkInterfaces[i][v].address && networkInterfaces[i][v].address.length && !networkInterfaces[i][v].internal&&networkInterfaces[i][v].family==='IPv4'&& networkInterfaces[i][v].address!=='10.10.100.100') {
						value = networkInterfaces[i][v].address;
						break interfaces;
					}
				}
			}
			return value;
		}
	}
}
exports.MyBase = MyBase;