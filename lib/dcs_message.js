var classBase=require('./base');
var { v4: uuidv4 } = require('uuid');
class dcs_message extends classBase.MyBase{
	constructor(config) {
		super(config); 
	}
	init(){
		var t = this;
		t.componentClass='dcs_message';
		t.version='0.1';
		t.id=0;
		t.tryCount=0;
		t.type='';
		t.ack=false;
		t.data=false;
		t.queued=0;
		this.messageId=uuidv4();
		Object.assign(this, this.config);
	}
	getdata(){
		return {messageId:this.messageId ,tryCount:this.tryCount ,type:this.type ,ack:this.ack ,data:this.data ,queued:this.queued};
	}
}
module.exports=dcs_message;