const formidable = require('formidable');
const fs=require('fs');
const http=require('http');
const path = require('path');
const processModule = require('process');
const timezoneJS = require('timezone-js');
const url=require('url');
const util = require('util');
const Sentry = require('@sentry/node');
Sentry.init({
	dsn: 'https://4e61d0b3664d41c9a849f8edbb2b2255@sentry.kaitek.com.tr/3'
});
let __clients=[];//socket dizisi
let _clients=[];//client tablosu satırları
let _io=false;
let _pid=false;
let _pin=false;
let _ip=false;
let _port=false;
let _server=false;
//let _odbc_merga=false;
processModule.on('message', function (obj) {
	let event = obj.event;
	let param = obj.param;
	switch (event) {
	case 'start_server':{
		_pid=obj.pid;
		_pin=obj.pin;
		_ip=obj.ip;
		_port=obj.port;
		//_odbc_merga=obj.odbc_merga;
		start_server(param);
		break;
	}
	case 'set_clients':{
		for(let i = 0; i < obj.data.length; i++){
			let item=obj.data[i];
			let idx=getIndex(_clients,'uuid',item.uuid);
			if(idx===-1){
				_clients.push(item);
			}
		}
		break;
	}
	case 'req_getDataAllClients':{
		processModule.send({event:'res_getDataAllClients',data:_clients,ip:obj.ip});
		for(let i=0;i<_clients.length;i++){
			let idx=getIndex(__clients,'ip',_clients[i].ip);
			if(idx!==-1){
				let _tclient=__clients[idx];
				_tclient.emit('sn_cn',{event:'req_watch_info'});
			}
		}
		break;
	}
	case 'socket_client_setuser_response':{
		let _client=false;
		let idx=getIndex(__clients,'ip',obj.data.ip);
		if(idx!==-1){
			_client=__clients[idx];
		}	
		if(_client!==false){
			_client.emit('sn_cn',{event:'setuser',ip:obj.data.ip,buildNumber:0,id:obj.data.id,code:obj.data.code,pin:_pin,pincode:obj.data.pincode});
		}
		break;
	}
	case 'activity_control_questions_response':{
		let _client=false;
		let idx=getIndex(__clients,'ip',obj.ip);
		if(idx!==-1){
			_client=__clients[idx];
		}	
		if(_client!==false){
			_client.emit('sn_cn',{event:'activity_control_questions_response',ip:obj.ip,data:obj.data});
		}
		break;
	}
	//case 'documents_response':{
	//	let _client=false;
	//	let idx=getIndex(__clients,'ip',obj.ip);
	//	if(idx!==-1){
	//		_client=__clients[idx];
	//	}	
	//	if(_client!==false){
	//		_client.emit('sn_cn',{event:'documents_response',ip:obj.ip,client:obj.client,day:obj.day,jobrotation:obj.jobrotation,opnames:obj.opnames,employee:obj.employee,data:obj.data});
	//	}
	//	break;
	//}
	case 'res|case_movements|get':{
		let _client=false;
		let idx=getIndex(__clients,'ip',obj.ip);
		if(idx!==-1){
			_client=__clients[idx];
		}	
		if(_client!==false){
			_client.emit('sn_cn',{event:event,ip:obj.ip,client:obj.client,data:obj.data});
		}
		break;
	}
	case 'res|task_plan_employees|get':{
		let _client=false;
		let idx=getIndex(__clients,'ip',obj.ip);
		if(idx!==-1){
			_client=__clients[idx];
		}	
		if(_client!==false){
			_client.emit('sn_cn',{event:event,ip:obj.ip,client:obj.client,data:obj.data});
		}
		break;
	}
	case 'penetrations_response':{
		let _client=false;
		let idx=getIndex(__clients,'ip',obj.ip);
		if(idx!==-1){
			_client=__clients[idx];
		}	
		if(_client!==false){
			_client.emit('sn_cn',{event:'penetrations_response',ip:obj.ip,title:obj.title,text:obj.text});
		}
		break;
	}
	case 'device_connect':{
		let idx=getIndex(_clients,'ip',obj.ip);
		if(idx!==-1){
			_clients[idx].connected=true;
		}
		break;
	}
	case 'device_disconnect':{
		let idx=getIndex(_clients,'ip',obj.ip);
		if(idx!==-1){
			_clients[idx].connected=false;
		}
		break;
	}
	case 'watch_calc_tempo':{
		let idx=getIndex(_clients,'code',obj.queue);
		if(idx!==-1){
			if(_clients[idx].processVariables){
				_clients[idx].processVariables.cpd_pctime=obj.data.cpd_pctime;
				_clients[idx].processVariables.cpd_time=obj.data.cpd_time;
				_clients[idx].processVariables.lastprodtime=obj.data.lastprodtime;
				_clients[idx].processVariables.lastprodstarttime=obj.data.lastprodstarttime;
			}
			processModule.send({event:'res_watch_calc_tempo',data:obj.data,ip:obj.ip});
		}
		break;
	}
	case 'watch_info':{
		let idx=getIndex(_clients,'code',obj.queue);
		if(idx!==-1){
			let _cpd_time;
			let _cpd_ptime;
			let _cpd_pctime;
			if(_clients[idx].processVariables){
				_cpd_time=_clients[idx].processVariables.cpd_time;
				_cpd_ptime=_clients[idx].processVariables.cpd_ptime;
				_cpd_pctime=_clients[idx].processVariables.cpd_pctime;
			}
			Object.assign(_clients[idx], obj.data);
			if(obj.data.processVariables&&!obj.data.processVariables.cpd_time){
				_clients[idx].processVariables.cpd_time=_cpd_time;
				_clients[idx].processVariables.cpd_ptime=_cpd_ptime;
				_clients[idx].processVariables.cpd_pctime=_cpd_pctime;
			}
			processModule.send({event:'res_watch_info',data:_clients[idx],ip:obj.ip});
		}
		break;
	}
	case 'watch_input_signal':{
		let idx=getIndex(_clients,'code',obj.queue);
		if(idx!==-1){
			if(_clients[idx].processVariables&&_clients[idx].processVariables.productions){
				let _idx=getIndex(_clients[idx].processVariables.productions,'code',obj.data.item.code);
				if(_idx!==-1){
					Object.assign(_clients[idx].processVariables.productions[_idx], obj.data.item);
					processModule.send({event:'res_watch_info',data:_clients[idx],ip:obj.ip});
				}
			}
		}
		break;
	}
	case 'query_losttype':{
		for(let i=0;i<__clients.length;i++){
			if(__clients[i].ip==obj.ip){
				__clients[i].emit('sn_cn',{event:event,client:obj.client,record_id:obj.record_id});
			}
		}
		break;
	}
	case 'req_device_is_online':{
		for(let i=0;i<__clients.length;i++){
			let _tclient=__clients[i];
			if(_tclient.ip==obj.parent.ip){
				return processModule.send({event:'res_device_is_online',connected:true,parent:obj.parent});
			}
		}
		processModule.send({event:'res_device_is_online',connected:false,parent:obj.parent});
		break;
	}
	case 'device_io_info':{
		let idx=getIndex(_clients,'code',obj.queue);
		if(idx!==-1){
			processModule.send({event:'res_device_io_info',data:obj.data,ip:_clients[idx].ip});
		}
		break;
	}
	case 'res_lockpc':{
		for(let i=0;i<__clients.length;i++){
			if(__clients[i].ip==obj.ip){
				__clients[i].emit('sn_cn',{event:'routecb',routeparams:obj.routeparams});
			}
		}
		break;
	}
	case 'res_unlockpc':{
		for(let i=0;i<__clients.length;i++){
			if(__clients[i].ip==obj.ip){
				__clients[i].emit('sn_cn',{event:'routecb',routeparams:obj.routeparams});
			}
		}
		break;
	}
	case 'res_update_check':{
		for(let i=0;i<__clients.length;i++){
			if(__clients[i].ip==obj.ip){
				__clients[i].emit('sn_cn',{event:'update_available',path:obj.path,hash:obj.hash,version:obj.version});
			}
		}
		break;
	}
	case 'res_control_delivery_package_info':{
		let _client=false;
		let idx=getIndex(__clients,'ip',obj.ip);
		if(idx!==-1){
			_client=__clients[idx];
		}	
		if(_client!==false){
			_client.emit('sn_cn',{event:'res_control_delivery_package_info',ip:obj.ip,result:obj.result,message:obj.message,data:obj.data});
		}
		break;
	}
	case 'res_refresh_delivery_package_info':{
		let _client=false;
		let idx=getIndex(__clients,'ip',obj.ip);
		if(idx!==-1){
			_client=__clients[idx];
		}	
		if(_client!==false){
			_client.emit('sn_cn',{event:'res_refresh_delivery_package_info',ip:obj.ip,result:obj.result,message:obj.message,data:obj.data});
		}
		break;
	}
	case 'res_case_delivery_manual':{
		let _client=false;
		let idx=getIndex(__clients,'ip',obj.ip);
		if(idx!==-1){
			_client=__clients[idx];
		}	
		if(_client!==false){
			_client.emit('sn_cn',{event:'res_case_delivery_manual',ip:obj.ip,result:obj.result,message:obj.message,casenumber:obj.casenumber,erprefnumber:obj.erprefnumber});
		}
		break;
	}
	case 'calculated_oee':{
		for(let i=0;i<__clients.length;i++){
			if(__clients[i].ip==obj.ip){
				__clients[i].emit('sn_cn',{event:event,data:obj.data});
			}
		}
		break;
	}
	case 'imported_api_call':{
		try {
			fs.unlinkSync(obj.data.path);
		} catch (error) {
			Sentry.captureException(error);
			//log_error('err-socket_server_device-imported_api_call',JSON.stringify(obj.data),'socket_server_device-imported_api_call');
		}
		for(let i=0;i<__clients.length;i++){
			if(__clients[i].ip==obj.ip){
				__clients[i].emit('sn_cn',{event:event,data:obj.data.data});
			}
		}
		if(obj.data.data.event==='holo_start'){
			obj.data.data.event='holo_finish';
			for(let i=0;i<__clients.length;i++){
				if(__clients[i].ip!==obj.ip){
					__clients[i].emit('sn_cn',{event:event,data:obj.data.data});
				}
			}
		}
		break;
	}
	default:{
		//console.log(obj);
		break;
	}
	}
});
processModule.on('uncaughtException', function(err){
	Sentry.captureException(err);
	setTimeout(() => {
		process.exit(20);
	}, 500);
	return;
});
let start_server=()=>{
	function onRequest(req,res){
		try {
			let uri = url.parse(req.url).pathname;
			let x = uri.replace('/','').split('&');
			if(x.length>1){
				let obj={};
				for(let i=0;i<x.length;i++){
					let citem=x[i].split('=');
					obj[citem[0]]=citem[1];
				}
			}else{
				if (uri == '/'){
					uri = '/index.html';
				}
				if(req.url =='/crashreport/'&& req.method.toLowerCase() == 'post'){
					let form = new formidable.IncomingForm();
					form.encoding = 'utf-8';
					form.uploadDir = __dirname+'/../log';
					form.keepExtensions = true;
					form.parse(req, function(err, fields, files) {
						res.writeHead(200, {'content-type': 'text/plain'});
						res.write('received upload:\n\n');
						res.end(util.inspect({fields: fields, files: files}));
					});
					return;
				}else{
					let filename = __dirname+'/..'+unescape(uri);
					let stats;
					let extname = path.extname(filename);
					let contentType = 'text/html';
					switch (extname) {
					case '.asp':
					case '.aspx':
					case '.cc':
					case '.cfm':
					case '.cgi':
					case '.exe':
					case '.gdb':
					case '.htm':
					case '.ini':
					case '.jsp':
					case '.mdb':
					case '.php':
					case '.php3':
					case '.php4':
					case '.pl':
					case '.py':
					case '.rb':
					case '.sh':
					case '.tml':
					case '.wdm':
					case '.wdm.':
						res.writeHead(404, {'Content-Type': 'text/plain'});
						res.write('404 Not Found\n');
						res.end();
						return;
					case '.zip':
						contentType = 'application/zip';
						break;
					case '.js':
						contentType = 'text/javascript';
						break;
					case '.css':
						contentType = 'text/css';
						break;
					case '.txt':
						contentType = 'text/plain';
						break;
					}
					try {
						stats = fs.lstatSync(filename);
					} catch (e) {
						res.writeHead(404, {'Content-Type': 'text/plain'});
						res.write('404 Not Found\n');
						res.end();
						return;
					}
					if (stats.isFile()) {
						let fileStream = fs.createReadStream(filename);
						fileStream.on('error', function () {
							res.writeHead(404, { 'Content-Type': 'text/plain'});
							res.end('file not found');
						});
						fileStream.on('open', function() {
							res.writeHead(200, {'Content-Type': contentType});
							if(extname=='.zip'){
								res.writeHead(200,{'Content-Disposition': 'attachment; filename='+path.basename(uri)});
							}
						});
						fileStream.on('end', function() {
							//
						});
						fileStream.pipe(res);
					} else if (stats.isDirectory()) {
						res.writeHead(200, {'Content-Type': 'text/plain'});
						res.write('Index of '+uri+'\n');
						res.write('TODO, show index?\n');
						res.end();
					} else {
						res.writeHead(200, {'Content-Type': 'text/plain'});
						res.write('200 Internal server error\n');
						res.end();
					}
				}
			}
		} catch(e) {
			res.writeHead(200);
			res.end();     
			Sentry.captureException(e);
		}	
	}
	_server=http.createServer(onRequest);
	_io = require('socket.io')(_server,{
		cors: {
			origin: 'https://'+_ip,
			methods: ['GET', 'POST']
		}
	});
	_io.on('connection', function (client){
		let clientIp = client.request.connection.remoteAddress.replace('::ffff','').replace(':','');
		client.versionApp=0;
		client.buildNumber=0;
		client.dcsClientId=0;
		client.ip=clientIp;
		client.on('error', function(error){
			Sentry.captureException(error);
			setTimeout(() => {
				process.exit(21);
			}, 500);
		});
		client.on('disconnect', function(){
			for(let i=0;i<__clients.length;i++){
				let _client=__clients[i];
				if(_client.ip==client.ip){
					__clients.splice(i,1);
					processModule.send({event:'socket_client_disconnect',uuid: _client.uuid,ip:_client.ip});
				}
			}
		});
		client.on('cn_sn',function(obj){
			let event=obj.event;
			let _client;
			switch(event) {
			case 'connect':
				try{
					let _f=false;
					for(let i=0;i<__clients.length;i++){
						_client=__clients[i];
						if(_client.ip==client.ip){
							_f=true;
						}
					}
					if(!_f){
						client.uuid=obj.dcsClientId;
						__clients.push(client);
					}
					client.emit('sn_cn',{event:'connect',ip:client.ip,pin:_pin,rtc:getFullLocalDateString()});
					processModule.send({event:'socket_client_connect',data:obj,ip:client.ip});
				} catch(err) {
					Sentry.captureException(err);
				}
				break;
			case 'setuser':{
				_client=false;
				try{
					for(let i=0;i<__clients.length;i++){
						let _tclient=__clients[i];
						if(_tclient.ip==client.ip){
							_client=_tclient;
						}
					}
				} catch(err) {
					Sentry.captureException(err);
				}
				if(_client!==false){
					_client.started=true;
					let idx=getIndex(_clients,'ip',_client.ip);
					if(obj.params&&idx>-1){
						_clients[idx].params=obj.params;
						_clients[idx].cp_materialpreparation=obj.params.task_materialpreparation.value;
					}
					processModule.send({event:'socket_client_setuser',sync_data:obj.sync_data,uuid:obj.dcsClientId,ip:_client.ip,code:obj.client_name,params:obj.params});
				}
				break;
			}
			case 'sync_time':
				for(let i=0;i<__clients.length;i++){
					_client=__clients[i];
					if(_client.ip==client.ip){
						_client.emit('sn_cn',{event:'sync_time',rtc:getFullLocalDateString()});
					}
				}
				break;
			case 'update_check':{
				let _v=obj.versionApp.split('.');
				processModule.send({event:'req_update_check',ip:client.ip,major:parseInt(_v[0]),minor:parseInt(_v[1]),patch:parseInt(_v[2]),build:parseInt(_v[3])});
				break;
			}
			case 'control_delivery_package_info':{
				processModule.send({event:'req_control_delivery_package_info',ip:client.ip,barcode:obj.barcode,erprefnumber:obj.erprefnumber,client:obj.client});
				break;
			}
			case 'refresh_delivery_package_info':{
				processModule.send({event:'req_refresh_delivery_package_info',ip:client.ip,barcode:obj.barcode,erprefnumbers:obj.erprefnumbers,client:obj.client});
				break;
			}
			case 'case_delivery_manual':{
				processModule.send({event:'req_case_delivery_manual',ip:client.ip,barcode:obj.barcode,client:obj.client,erprefnumber:obj.erprefnumber});
				break;
			}
			case 'activity_control_questions|get':{
				if(obj.opnames.length>0){
					processModule.send({event:'req_activity_control_questions|get',ip:client.ip,client:obj.client,day:obj.day,jobrotation:obj.jobrotation,opnames:obj.opnames,employee:obj.employee});
				}
				break;
			}
			case 'case_movements|get':{
				processModule.send({event:'case_movements|get',ip:client.ip,client:obj.client});
				break;
			}
			//case 'documents|get':{
			//	if(obj.opnames.length>0){
			//		processModule.send({event:'req_documents|get',ip:client.ip,client:obj.client,day:obj.day,jobrotation:obj.jobrotation,opnames:obj.opnames,employee:obj.employee});
			//	}
			//	break;
			//}
			case 'penetrations|get':{
				if(obj.opnames.length>0){
					processModule.send({event:'req_penetrations|get',ip:client.ip,opnames:obj.opnames});
				}
				break;
			}
			case 'ping':{
				for(let i=0;i<__clients.length;i++){
					_client=__clients[i];
					if(_client.ip==client.ip){
						_client.emit('sn_cn',{event:'pong'});
					}
				}
				processModule.send({event:'socket_client_ping',ip:client.ip});
				break;
			}
			case 'res_query_losttype':{
				processModule.send({event:'res_query_losttype',ip:client.ip,client:obj.client,record_id:obj.record_id,losttype:obj.losttype});
				break;
			}
			case 'task_plan_employees|get':{
				processModule.send({event:'task_plan_employees|get',ip:client.ip,employee:obj.employee});
				break;
			}
			}
		});
	});
	_server.timeout = 20000;
	_server.listen(_port);
	processModule.send({event:'started_socket_server_device',pid:_pid,port:_port});
};
let getFullLocalDateString=()=>{
	let time=new timezoneJS.Date();
	return time.toString('yyyy-MM-dd HH:mm:ss.SSS');
};
let getIndex=(array,key,keyValue)=>{
	if(typeof array.length!='undefined')
		for(let i = 0, len = array.length; i < len; i++){
			if(array[i][key]==keyValue)	return i;
		}
	else
		for(let i in array)
			if(typeof array[i]!='function')
				if(array[i][key]==keyValue)	return i;
	return -1;
};