const processModule = require('process');
const amqp = require('amqplib/callback_api');
const Promise = require('promise');
const fs=require('fs');
const fsE = require('fs-extra');
const nodemailer = require('nodemailer');
const util = require('util');
// const path = require('path');
const fs_writeFile = util.promisify(fs.writeFile);
const Sequelize = require('sequelize');
const timezoneJS = require('timezone-js');
const { v4: uuidv4 } = require('uuid');
const Sentry = require('@sentry/node');
const odbc = require("odbc");
Sentry.init({
	dsn: 'https://4e61d0b3664d41c9a849f8edbb2b2255@sentry.kaitek.com.tr/3'
});
let sequelize;
let conn=null;
let pubChannel = null;
let _pid=false;
let _ip=false;
let _port=false;
let _dbParams=false;
let arr_queue_assert=[];
let arr_tmp_queue_assert=[];
let _url_amqp=false;
let _export_fault=false;
// let _odbc_merga=false;
let _odbc_boys=false;
let _odbc_boys_prod=false;
let mailTransporter=false;
let _messages=[];
let clients=[];
let clientsNew=[];
const path_export=__dirname+'/../_export/';
let _arr_tables=['client_details','client_lost_details','client_params','client_production_details','clients','control_answers','control_questions','reworks','task_reject_details','task_cases'];

processModule.on('message', function (obj) {
	let event = obj.event;
	switch (event) {
	case 'init_server':{
		_pid=obj.pid;
		_ip=obj.ip;
		_port=obj.port;
		_dbParams=obj.param;
		_url_amqp='amqp://admin:q32wxcx@'+obj.ip;
		_export_fault=obj.export_fault;
		// =obj.odbc_merga;
		_odbc_boys=obj.odbc_boys;
		_odbc_boys_prod=obj.odbc_boys_prod;
		connect_amqp();
		break;
	}
	default:{
		//console.log(obj);
		break;
	}
	}
});
processModule.on('uncaughtException', function(err){
	Sentry.captureException(err);
	setTimeout(() => {
		process.exit(400);
	}, 500);
	return;
});
let connect_amqp = ()=>{
	amqp.connect(_url_amqp,function(_err,_conn) {
		if (_err) {
			Sentry.captureMessage('[AMQP-R] connect ' + _err);
			//console.error('[AMQP-R]', _err);
			setTimeout(() => {
				process.exit(401);
			}, 500);
			return;
		}
		if(conn==null){
			conn=_conn;
			conn.on('error', function(err) {
				Sentry.captureMessage('[AMQP-R] conn error ' + err);
				if (err.message && err.message !== 'Connection closing') {
					//console.error('[AMQP-R] conn error', err);
				}
				Sentry.captureException(err);
				setTimeout(function(){
					process.exit(402);
				}, 1000);
				return;
			});
			conn.on('close', function() {
				Sentry.captureMessage('[AMQP-R] close');
				setTimeout(function(){
					process.exit(403);
				}, 1000);
				return;
			});
		}
		createChannel();
	});
};
let createChannel=()=> {
	conn.createChannel(function(err, ch) {
		if (closeOnErr(err)) return;
		if(pubChannel==null){
			pubChannel = ch;
			pubChannel.on('error', function(err) {
				Sentry.captureMessage('[AMQP-R] channel err ' + err);
				//console.error('[AMQP-R] channel error', err);
				Sentry.captureException(err);
				setTimeout(function(){
					process.exit(404);
				}, 1000);
				return;
			});
			pubChannel.on('close', function() {
				Sentry.captureMessage('[AMQP-R] channel close');
				//console.log('[AMQP-R] channel closed');
				setTimeout(function(){
					process.exit(405);
				}, 1000);
				return;
			});
			fsE.ensureDirSync(path_export+'uretim/uretimsonu/');
			fsE.ensureDirSync(path_export+'uretim/kayipbitir/');
			fsE.ensureDirSync(path_export+'penetrations/');
			processModule.send({event:'cp_server_inited',pid:_pid});
			db_connect(_dbParams);
		}
	});
};
let closeOnErr=(err)=> {
	if (!err) return false;
	Sentry.captureMessage('[AMQP-R] error' + err);
	//console.error('[AMQP-R] error', err);
	Sentry.captureException(err);
	setTimeout(function(){
		process.exit(406);
	}, 1000);
	return true;
};
let db_connect = (obj) => {
	sequelize = new Sequelize(obj.database, obj.username, obj.password, {
		host: obj.host,
		dialect: 'postgres',
		logging: false,
		pool: {
			max: 10,
			min: 0,
			acquire: 100000,
			idle: 10000
		},
		timezone:obj.timezone
	});
	return sequelize
		.authenticate()
		.then(() => {
			mailTransporter = nodemailer.createTransport({
				service: 'gmail',
				auth: {
					user: 'verimotrt@gmail.com',
					pass: 'mkktjptwruhmczmg'
				}
			});
			return Promise.all([queue_asserter()]).then(()=>{
				process.nextTick(function(){
					setTimeout(function() {
						return run();
					}, 10);
				});
			});
		}).then(() => {
			return;
			// let sql='SELECT convert_from(cast(name as bytea)::bytea, \'latin5\') personel,name from employees where finish is null and code=:code';
			// return sequelize.query(sql,{replacements:{code:'189'}, type: sequelize.QueryTypes.SELECT}).then((result)=>{
			// 	let name1='employee';
			// 	let name2='employee';
			// 	if(result&&result.length>0){
			// 		name1=result[0].personel;
			// 		name2=result[0].name;
			// 	}
			// 	return new Promise(function(/*resolve, reject*/) {
			// 		let db = require('odbc');
			// 		db.connect(_odbc_boys, function (err,connection) {
			// 			if (err){
			// 				Sentry.captureException(err);
			// 			}else{
			// 					let sql='insert into VERIMOD_TEST (EKIPMANKODU,BAKIMKODU,TALEPEDEN) values '+
			// 					' ( \''+name1+'\',\''+name2+'\',\''+name2+'\')';
			// 					return connection.query(sql).then(function(){
			// 						connection.close((error) => {
			// 							if (error) { 
			// 								return; 
			// 							} // handle
			// 							// console.log('dbclose.done');
			// 						});
			// 					}).catch((err) => {
			// 						//log(sql);
			// 						Sentry.captureException(err);
			// 						Sentry.addBreadcrumb({
			// 							category: 'boys',
			// 							message: 'boys_insert ',
			// 							level: 'error',
			// 							data:sql
			// 						});
			// 						connection.close((error) => {
			// 							if (error) { 
			// 								return; 
			// 							} // handle
			// 							// console.log('dbclose.done.err');
			// 						});
			// 					});
			// 			}
			// 		});
			// 	});
			// });
		})
		.catch(err => {
			Sentry.captureMessage('[AMQP-R-db] ' + err);
			Sentry.captureException(err);
			setTimeout(() => {
				process.exit(407);
			}, 500);
			return;
		});
};
let control_arr_tmp_queue_assert=()=>{
	if(arr_tmp_queue_assert.length>0){
		process.nextTick(function(){
			setTimeout(function() {
				return Promise.all([control_arr_tmp_queue_assert()]);
			}, 5);
		}); 
	}else{
		return true;
	}
};
let queue_asserter=()=>{
	const _w_=(_ip==='192.168.1.40'||_ip==='192.168.1.10')?' and code<>\'03.06.0003\' ':'';
	return sequelize.query('SELECT id,code,uuid,ip,connected,status,statustime,workflow,pincode,versionapp,versionshell,computername,lastprodstarttime,lastprodtime '+
	' from clients where uuid is not null and isactive=true and ip<>\'\' and ip is not null '+_w_+' order by code'
	, { type: sequelize.QueryTypes.SELECT}).then(function(results) {
		try {
			clients=results;
		} catch (error) {
			Sentry.captureException(error);
		}
		for(let i=0;i<results.length;i++){
			let item=results[i];
			if(item.code&&item.code!==null&&item.code!==''&&item.code!=='undefined'){
				if(arr_queue_assert.indexOf(item.code+'_client')===-1){
					if(pubChannel!==null){
						arr_tmp_queue_assert.push(item.code+'_client');
						pubChannel.assertQueue(item.code+'_client',{durable: true},function(err, ok){
							if(err){
								Sentry.captureMessage('[AMQP-R-assert] ' + err);
								//console.error('[AMQP-R] error-assertQueue', err);
								setTimeout(function(){
									process.exit(431);
								}, 1000);
							}
							arr_tmp_queue_assert.splice(arr_tmp_queue_assert.indexOf(ok.queue),1);
							return pubChannel.consume(ok.queue, function(msg) {
								if (msg !== null) {
									_messages.push(msg);
								}
							});
						});
						arr_queue_assert.push(item.code+'_client');
					}
				}
				if(_ip==='10.10.0.10'){
					const qName=item.code + '_status';
					if(arr_queue_assert.indexOf(qName)===-1){
					 	if(pubChannel!==null){
					 		arr_tmp_queue_assert.push(qName);
					 		pubChannel.assertQueue(qName,{durable: true},function(err, ok){
					 			if(err){
					 				Sentry.captureMessage('[AMQP-R-assert] ' + err);
					 				//console.error('[AMQP-R] error-assertQueue', err);
					 				setTimeout(function(){
					 					process.exit(433);
					 				}, 1000);
					 			}
					 			arr_tmp_queue_assert.splice(arr_tmp_queue_assert.indexOf(ok.queue),1);
					 		});
					 		arr_queue_assert.push(qName);
					 	}
					}
				}
			}else{
				if(item.ip&&item.ip!==''&&item.ip!=='undefined'){
					if(arr_queue_assert.indexOf(item.ip+'_client')===-1){
						if(pubChannel!==null){
							arr_tmp_queue_assert.push(item.ip+'_client');
							pubChannel.assertQueue(item.ip+'_client',{durable: true},function(err, ok){
								if(err){
									Sentry.captureMessage('[AMQP-R-assert] ' + err);
									//console.error('[AMQP-R] error-assertQueue', err);
									setTimeout(function(){
										process.exit(432);
									}, 1000);
								}
								arr_tmp_queue_assert.splice(arr_tmp_queue_assert.indexOf(ok.queue),1);
								return pubChannel.consume(ok.queue, function(msg) {
									if (msg !== null) {
										_messages.push(msg);
									}
								});
							});
							arr_queue_assert.push(item.ip+'_client');
						}
					}
				}
			}
		}
		if(_ip==='10.10.0.10'&&(_port===8800||_port==='8800')){
			if(arr_queue_assert.indexOf('KALIPBAKIM')===-1){
				if(pubChannel!==null){
					arr_tmp_queue_assert.push('KALIPBAKIM');
					pubChannel.assertQueue('KALIPBAKIM',{durable: true},function(err, ok){
						if(err){
							Sentry.captureMessage('[AMQP-R-assert] ' + err);
							//console.error('[AMQP-R] error-assertQueue', err);
							setTimeout(function(){
								process.exit(433);
							}, 1000);
						}
						arr_tmp_queue_assert.splice(arr_tmp_queue_assert.indexOf(ok.queue),1);
					});
					arr_queue_assert.push('KALIPBAKIM');
				}
			}
			// if(arr_queue_assert.indexOf('devicestatus')===-1){
			// 	if(pubChannel!==null){
			// 		arr_tmp_queue_assert.push('devicestatus');
			// 		pubChannel.assertQueue('devicestatus',{durable: true},function(err, ok){
			// 			if(err){
			// 				Sentry.captureMessage('[AMQP-R-assert] ' + err);
			// 				//console.error('[AMQP-R] error-assertQueue', err);
			// 				setTimeout(function(){
			// 					process.exit(433);
			// 				}, 1000);
			// 			}
			// 			arr_tmp_queue_assert.splice(arr_tmp_queue_assert.indexOf(ok.queue),1);
			// 		});
			// 		arr_queue_assert.push('devicestatus');
			// 	}
			// }
		}
		return Promise.all([control_arr_tmp_queue_assert()]);
	}).then(function(){
		return sequelize.query('SELECT id,code,uuid,ip,connected,status,statustime,workflow,pincode,versionapp,versionshell,computername,lastprodstarttime,lastprodtime '+
		' from clients where uuid is null order by code'
		, { type: sequelize.QueryTypes.SELECT}).then(function(resultsNew) {
			clientsNew=resultsNew;
		});
	}).then(function(){
		process.nextTick(function(){
			setTimeout(function() {
				queue_asserter();
			}, 10000);
		});
	});
};
let run=()=>{
	let _tmp=[];
	let i=0;
	while(_messages.length>0&&i++<50){
		_tmp.push(_messages.shift());
	}
	let _promises=[];
	while(_tmp.length>0){
		_promises.push(_process(_tmp.shift()));
	}
	return Promise.all(_promises).then(()=>{
		process.nextTick(function(){
			setTimeout(function() {
				run();
			}, 500);
		});
	});
};
let sendMesageToQueue = (channel, queue, content)=>{
	if(channel!==null){
		//@@todo:queue var mı diye kontrol edilmeli
		channel.sendToQueue(queue, new Buffer.from(content), {persistent: true});
	}
	return null;
};
let _process=(msg)=>{
	try{
		let _mes_client=msg.fields.routingKey.replace('_client','');
		let mes=JSON.parse(msg.content);
		//console.log(mes.type);
		//console.log(mes.data);
		switch(mes.type){
		case 'sync_message':{
			try {
				let _promises=[];
				let _tableName=mes.data.tableName;
				let lut='';
				if(mes.data.procType==='insert'||mes.data.procType==='update'){
					lut=mes.data.data.updatedAt;
					if(lut&&lut.length>=19){
						lut=lut.substr(0,19);
					}else{
						//console.log(mes.data);
					}
				}
				let _b_sync=true;
				if(_tableName=='undefined'||!_tableName){
					//console.error('99');
					//console.error(mes);
				}
				//if(_tableName=='task_lists' &&_mes_client==='HP012'){//&& mes.data.procType!=='update'
				//	processModule.send({event:'sync_message',ip:'',data:JSON.stringify(mes.data)});
				//	console.log('------------------');
				//	console.log(_mes_client);
				//	console.log(mes.data);
				//}
				if(mes.data.procType=='insert'&&_tableName=='client_production_details'&&(mes.data.data.type=='c_s'||mes.data.data.type=='c_s_ping')){
					let _start=mes.data.data.start.substr(0,15)+'0:00';
					let sql='select * from client_production_details where finish is null and type in (\'c_s\',\'c_s_ping\') and client=:client and day=:day and jobrotation=:jobrotation and tasklist=:tasklist and start=:start';
					return sequelize.query(sql,{replacements:{client:mes.data.data.client,day:mes.data.data.day,jobrotation:mes.data.data.jobrotation,tasklist:mes.data.data.tasklist,start:_start}, type: sequelize.QueryTypes.SELECT}).then((result)=>{
						if(result.length>0){
							if(result[0].productioncurrent+mes.data.data.productioncurrent!==null){
								let sql_u='update client_production_details set type=:type,production=:production,productioncurrent=:productioncurrent where id=:id';
								return sequelize.query(sql_u, { replacements: {type:'c_s',production:result[0].production+1,productioncurrent:result[0].productioncurrent+mes.data.data.productioncurrent,id:result[0].id}, type: sequelize.QueryTypes.UPDATE }).catch(function(err){
									Sentry.captureException(err);
									//console.log(err.message);
									//console.log('catch-91-'+sql_u);
									setTimeout(() => {
										process.exit(408);
									}, 500);
									return null;
								});
							}
							return null;
						}else{
							let afn=[];
							let afv=[];
							let orep={};
							let val;
							mes.data.data.start=_start;
							for(let prop in mes.data.data){
								if(prop=='updatedAt'){
									prop='updated_at';
								}
								if(prop=='createdAt'){
									prop='created_at';
								}
								if(prop!='id'){
									afn.push('"'+prop+'"');
									afv.push(':'+prop);
									val=mes.data.data[(prop=='updated_at'?'updatedAt':(prop=='created_at'?'createdAt':prop))];
									orep[prop]=val;
								}
							}
							orep.create_user_id=1;
							orep.update_user_id=1;
							orep.type='c_s';
							orep.productioncurrent=0;
							let sql_i='INSERT INTO client_production_details ('+ afn.join(',') +') VALUES ('+ afv.join(',') +')';
							return sequelize.query(sql_i, { replacements: orep, type: sequelize.QueryTypes.INSERT });
						}
					}).then(function(){
						pubChannel.ack(msg);
					}).catch(function(err){
						Sentry.captureException(err);
						//console.log(err.message);
						//console.log('catch-90-'+sql);
						setTimeout(() => {
							process.exit(409);
						}, 500);
						return null;
					});
				}
				if(mes.data.procType=='insert'){
					let afn=[];
					let afv=[];
					let orep={};
					let val;
					let cp_client='';
					let cp_name='';
					//if(_tableName==='task_reject_details'||_tableName==='control_answers'||_tableName==='client_params'){
					if(_tableName==='task_reject_details'||_tableName==='control_answers'){
						delete mes.data.data.record_id;
					}
					for(let prop in mes.data.data){
						if(prop=='updatedAt'){
							prop='updated_at';
						}
						if(prop=='createdAt'){
							prop='created_at';
						}
						//if(_tableName=='client_params'){
						//	if(prop=='client'){
						//		cp_client=mes.data.data.client;
						//	}
						//	if(prop=='name'){
						//		cp_name=mes.data.data.name;
						//	}
						//}
						if(_tableName=='task_lists'&&prop=='erprefnumber'){
							processModule.send({event:'write_verimotkd',erprefnumber:mes.data.data.erprefnumber});
						}
						if(prop!='id'){
							afn.push('"'+prop+'"');
							afv.push(':'+prop);
							val=mes.data.data[(prop=='updated_at'?'updatedAt':(prop=='created_at'?'createdAt':prop))];
							orep[prop]=val;
						}
					}
					orep.create_user_id=1;
					orep.update_user_id=1;
					let sql='INSERT INTO '+ _tableName + ' ('+ afn.join(',') +') VALUES ('+ afv.join(',') +')';
					if(_tableName!=='task_reject_details'&&_tableName!=='client_details'&&_tableName!=='client_params'&&_tableName!=='control_answers'){
						_promises.push(sequelize.query('select * from '+_tableName+' where record_id=:record_id ',{replacements:{record_id: mes.data.data.record_id}, type: sequelize.QueryTypes.SELECT}).then(function(res){
							if(res.length>0){
								return null;
							}else{
								return sequelize.query(sql, { replacements: orep, type: sequelize.QueryTypes.INSERT })
									.then(function(){
										return null;
									})
									.catch(function(err){
										Sentry.captureException(err);
										//console.log('catch-1-'+sql);
										//console.log(mes.data);
										//console.log(afn);
										//console.log(afv);
										//console.log(err);
									});
							}
						}));
					}else{
						if(_tableName=='client_details'){
							let sql='DELETE FROM '+ _tableName + ' WHERE client=:client and start=:start';
							_promises.push(sequelize.query(sql, { replacements: {client:cp_client,start:mes.data.data.start}, type: sequelize.QueryTypes.DELETE })
								.catch(function(err){
									Sentry.captureException(err);
								})
							);
						}
						if(_tableName=='client_params'){
							let sql='DELETE FROM '+ _tableName + ' WHERE client=:client and name=:name';
							_promises.push(sequelize.query(sql, { replacements: {client:cp_client,name:cp_name}, type: sequelize.QueryTypes.DELETE })
								.catch(function(err){
									Sentry.captureException(err);
								})
							);
						}
						_promises.push(sequelize.query(sql, { replacements: orep, type: sequelize.QueryTypes.INSERT })
							.then(function(){
								return null;
							})
							.catch(function(err){
								Sentry.captureException(err);
								//console.log('catch-1-'+sql);
								//console.log(mes.data);
								//console.log(afn);
								//console.log(afv);
								//console.log(err);
							})
						);
					}
				}
				if(mes.data.procType=='update'){
					let asql=[];
					let orep={};
					let val;
					if(_tableName=='task_lists'/*&&prop=='erprefnumber'&&parseInt('0'+mes.data.data.productdonecount)<5&&(mes.data.data.finish==''||mes.data.data.finish==null)*/){
						if(mes.data.data.erprefnumber){
							processModule.send({event:'write_verimotkd',erprefnumber:mes.data.data.erprefnumber,data:mes.data.data});
						}
					}
					for(let prop in mes.data.data){
						if(prop=='updatedAt'){
							prop='updated_at';
						}
						if(prop!='id'&&prop!='record_id'&&((_tableName==='task_lists'&&prop!=='clients')||_tableName!=='task_lists')){
							asql.push('"'+prop+'"'+'=:'+prop);
						}
						val=mes.data.data[prop=='updated_at'?'updatedAt':prop];
						if(prop==='taskinfo'){
							val=JSON.parse(mes.data.data[prop]).replaceAll('"','\\"');
							console.log('taskinfo:',val);
						}
						orep[prop]=val;
					}
					//tablo employees ise
					//cihazdan gelen verinin tarihi
					//sunucudaki o kaydın tarihinden ileri değilse 
					//sunucuda güncelleme yapılmayacak
					let sql='UPDATE ' + _tableName + ' SET ' + asql.join(',') +' WHERE record_id=:record_id ';
					if(_tableName==='employees'){
						if(mes.data.data.strlastseen){
							sql=sql+' and ("strlastseen"<=:strlastseen or strlastseen is null)';
							orep.strlastseen=mes.data.data.strlastseen.substr(0,19);
							//cihazdan gelen verinin tarihi
							//sunucudaki o kaydın tarihinden ileri değilse 
							//cihazlara yayınlama olmayacak
							_promises.push(sequelize.query('select * from "employees" where ("strlastseen"<=:strlastseen or strlastseen is null) and "record_id"=:record_id'
								, { replacements:{strlastseen:mes.data.data.strlastseen.substr(0,19),record_id:mes.data.data.record_id}, type: sequelize.QueryTypes.SELECT}).then(function(_e_result){
								if(_e_result.length>0){
									_b_sync=true;
								}else{
									_b_sync=false;
								}
							}));
						}else{
							sql=sql+' and "updated_at"<:updated_at ';
							orep.updated_at=mes.data.data.updatedAt.substr(0,19);
							//cihazdan gelen verinin tarihi
							//sunucudaki o kaydın tarihinden ileri değilse 
							//cihazlara yayınlama olmayacak
							_promises.push(sequelize.query('select * from "employees" where "updated_at"<:updated_at and "record_id"=:record_id'
								, { replacements:{updated_at:mes.data.data.updatedAt.substr(0,19),record_id:mes.data.data.record_id}, type: sequelize.QueryTypes.SELECT}).then(function(_e_result){
								if(_e_result.length>0){
									_b_sync=true;
								}else{
									_b_sync=false;
								}
							}));
						}					
					}
					if(_tableName==='client_details'){
						sql='UPDATE ' + _tableName + ' SET ' + asql.join(',') +' WHERE client=:client and start=:start ';
					}
					if(_tableName==='client_params'){
						sql='UPDATE ' + _tableName + ' SET ' + asql.join(',') +' WHERE client=:client and name=:name ';
					}
					if(_tableName!=='client_details'&&_tableName!=='client_params'&&_tableName!=='task_reject_details'){
						_promises.push(sequelize.query('select * from '+_tableName+' where record_id=:record_id ',{replacements:{record_id: mes.data.data.record_id}, type: sequelize.QueryTypes.SELECT}).then(function(res){
							if(res.length>0){
								return sequelize.query(sql, { replacements: orep, type: sequelize.QueryTypes.UPDATE })
									.then(function(){
										return null;
									})
									.catch(function(err){
										Sentry.captureException(err);
										//console.log(err.message);
										//console.log('catch-2-'+sql);
										//console.log(orep);
										return null;
									});
							}
						}));
					}else{
						_promises.push(
							sequelize.query(sql, { replacements: orep, type: sequelize.QueryTypes.UPDATE })
								.then(function(){
									return null;
								})
								.catch(function(err){
									Sentry.captureException(err);
									//console.log(err.message);
									//console.log('catch-2-'+sql);
									//console.log(orep);
									return null;
								})
						);
					}
				}
				if(mes.data.procType=='delete'){
					if(_tableName==='client_details'){
						let sql='DELETE FROM "' + _tableName +'" WHERE "client"=:client and start=:start';
						_promises.push(sequelize.query(sql, { replacements: {client:mes.data.data.client,start:mes.data.data.start}, type: sequelize.QueryTypes.DELETE })
							.then(function(){
								return null;
							})
							.catch(function(err){
								Sentry.captureException(err);
								return null;
							}));
					}else{
						let sql='DELETE FROM "' + _tableName +'" WHERE "record_id"=:record_id';
						_promises.push(sequelize.query(sql, { replacements: {record_id:mes.data.data.record_id}, type: sequelize.QueryTypes.DELETE })
							.then(function(){
								return null;
							})
							.catch(function(err){
								Sentry.captureException(err);
								return null;
							}));
					}
				}
				if(mes.data.procType=='replace'){
					let sql='DELETE FROM "' + _tableName +'" WHERE 1=1 ';
					let _rep={};
					for(let i=0;i<mes.data.fieldName.length;i++){
						sql+=' and "'+mes.data.fieldName[i]+'"=:'+mes.data.fieldName[i]+' ';
						_rep[mes.data.fieldName[i]]=mes.data.fieldValue[i];
					}
					_promises.push(sequelize.query(sql, { replacements: _rep, type: sequelize.QueryTypes.DELETE })
						.then(function(){
							let afn=[];
							let afv=[];
							let orep={};
							let val;
							for(let prop in mes.data.data){
								if(prop=='updatedAt'){
									prop='updated_at';
								}
								if(prop=='createdAt'){
									prop='created_at';
								}
								if(prop!='id'){
									afn.push('"'+prop+'"');
									afv.push(':'+prop);
									val=mes.data.data[(prop=='updated_at'?'updatedAt':(prop=='created_at'?'createdAt':prop))];
									orep[prop]=val;
								}
							}
							orep.create_user_id=1;
							orep.update_user_id=1;
							let sql='INSERT INTO '+ _tableName + ' ('+ afn.join(',') +') VALUES ('+ afv.join(',') +')';
							return sequelize.query(sql, { replacements: orep, type: sequelize.QueryTypes.INSERT })
								.then(function(){
									return null;
								})
								.catch(function(err){
									Sentry.captureException(err);
									//console.log('catch-158-'+sql);
									//console.log(mes.data);
									//console.log(afn);
									//console.log(afv);
									//console.log(err);
								});
						})
						.catch(function(err){
							Sentry.captureException(err);
							return null;
						}));
				}
				if((_tableName=='client_production_details'||_tableName=='client_production_details')
					&&mes&&mes.data&&mes.data.data&&mes.data.data.client==='LT001'
				){
					//console.log(mes.data.data);
				}
				return Promise.all(_promises).then(function(){
					if(_tableName!='_sync_updatelogs'&&_b_sync&&_arr_tables.indexOf(_tableName)==-1&&mes.data.procType!=='replace'){
						return sequelize.query('SELECT * from "_sync_updatelogs" where "tableName"=:tableName limit 1'
							, { replacements:{tableName:_tableName},type: sequelize.QueryTypes.SELECT})
							.then(function(result) {
								if(result.length>0){//tabloda kayıt var, son güncelleme zamanı güncellenecek
									result=result[0];
									if(lut>result.strlastupdate.substr(0,19)){
										let sql='UPDATE "_sync_updatelogs" set "strlastupdate"=\''+lut+'\' where "tableName"=\''+_tableName+'\'';
										return sequelize.query(sql, { type: sequelize.QueryTypes.UPDATE});
									}else{
										return null;
									}
								}else{
									return sequelize.query('INSERT INTO "_sync_updatelogs"("tableName","lastUpdate","strlastupdate","createdAt","updatedAt") VALUES (:tableName,:lastUpdate,:strlastupdate,:strlastupdate,:strlastupdate) '
										, { replacements:{tableName:_tableName,lastUpdate:lut,strlastupdate:lut}, type: sequelize.QueryTypes.INSERT});
								}
							})
							.then(function(){
								return sequelize.query('SELECT * from "__sync_updates" where "tablename"=:tableName limit 1'
								, { replacements:{tableName:_tableName},type: sequelize.QueryTypes.SELECT})
								.then(function(result) {
									if(result.length>0){//tabloda kayıt var, son güncelleme zamanı güncellenecek
										result=result[0];
										if(lut>result.lastupdate.substr(0,19)){
											let sql='UPDATE "__sync_updates" set "lastupdate"=\''+lut+'\' where "tablename"=\''+_tableName+'\'';
											return sequelize.query(sql, { type: sequelize.QueryTypes.UPDATE});
										}else{
											return null;
										}
									}else{
										return sequelize.query('INSERT INTO "__sync_updates"("tablename","lastupdate") VALUES (:tableName,:strlastupdate) '
											, { replacements:{tableName:_tableName,strlastupdate:lut}, type: sequelize.QueryTypes.INSERT});
									}
								});
							})
							.then(function(){
								let crecord = (_data)=>{
									//if(_data.data.tableName=='undefined'||!_data.data.tableName){
										//console.error(_data);
										//console.error(mes);
									//}
									let orep=_data;
									let _arr=[];
									for(let prop in _data.data.data){
										if((_data.data.tableName==='task_lists'&&prop==='taskinfo')){
											const valti=JSON.parse(_data.data.data[prop]).replaceAll('"','\\"');
											_arr.push('"'+prop+'":"'+valti+'"');
										}else{
											if((_data.data.tableName==='task_lists'&&prop!=='clients')||_data.data.tableName!=='task_lists'){
												_arr.push('"'+prop+'":"'+_data.data.data[prop]+'"');
											}
										}
									}
									orep.data='{"tableName":"'+_data.data.tableName+'","procType":"'+_data.data.procType+'","data":{'+_arr.join(',')+'},"id":null}';
									orep.createdAt=getFullLocalDateString();
									orep.updatedAt=getFullLocalDateString();
									orep.tryCount=10;
									let sql='INSERT INTO "_sync_messages" ("ip","uuid","messageId","tryCount","type","ack","queued","data","createdAt","updatedAt") VALUES (:ip,:uuid,:messageId,:tryCount,:type,:ack,:queued,:data,:createdAt,:updatedAt)';
									return sequelize.query(sql, { replacements: orep, type: sequelize.QueryTypes.INSERT })
										.catch(function(err){
											Sentry.captureException(err);
											return null;
										});
								};
								let syncMesAdd=(_data)=>{
									return sequelize.query('select * from employees where record_id=:record_id ',{replacements:{record_id: mes.data.data.record_id}, type: sequelize.QueryTypes.SELECT})
									.then(function(res){
										if(res.length>0){
											let empRow=res[0];
											delete empRow.create_user_id;
											delete empRow.update_user_id;
											delete empRow.delete_user_id;
											delete empRow.deleted_at;
											delete empRow.version;
											delete empRow.id;
											empRow.created_at=getFullLocalDateString();
											empRow.updated_at=getFullLocalDateString();
											empRow.client=_data.data.client;
											empRow.tasklist=_data.data.tasklist;
											empRow.day=_data.data.day;
											empRow.jobrotation=_data.data.jobrotation;
											empRow.strlastseen=_data.data.strlastseen;
											_data.data=JSON.stringify(empRow);
											let sqlInsertSyncMessage = `insert into __sync_messages (queuename,messagetype,priority,status,time,data,info) 
											values (:queuename,:messagetype,:priority,:status,:time,:data,:info)`;
											return sequelize.query(sqlInsertSyncMessage, { replacements: _data, type: sequelize.QueryTypes.INSERT })
												.catch(function(err){
													Sentry.captureException(err);
													return null;
												});
										}
										return;
									});
								};
								if(_arr_tables.indexOf(_tableName)==-1&&(mes.data.procType==='insert'||mes.data.procType==='update')){
									let _promises=[];
									for(let i = 0; i < clients.length; i++){
										let item=clients[i];
										let b_sync_2=true;
										if(_mes_client==item.code){
											b_sync_2=false;
										}
										if(mes.data.procType=='insert'&&item.connected==false){
											b_sync_2=false;
										}
										if(_tableName=='task_lists'&&mes.data.data.clients&&mes.data.data.clients.indexOf(item.code)==-1){
											b_sync_2=false;
										}
										if(b_sync_2==true){
											let obj_mes={
												ip:(item.code!==''&&item.code!==null?item.code:item.ip)+'_server'
												,uuid:item.uuid
												,messageId:uuidv4()
												,tryCount:10
												,type:'sync_message'
												,ack:true
												,queued:0
												,data:{tableName:mes.data.tableName,procType:mes.data.procType,data:mes.data.data,id:null}
											};
											_promises.push(crecord(obj_mes));
										}										
									}
									if(_tableName==='employees'){
										let mData=mes.data.data;
										for(let i = 0; i < clientsNew.length; i++){
											let itemNew=clientsNew[i];
											const arrValues = {
												queuename:itemNew.code + '_server'
												, messagetype:'record_update'
												, priority:10
												, status:'wait_for_send'
												, time:getFullLocalDateString()
												, data:mData
												, info:JSON.stringify({tableName:'employees',fieldName:'record_id',fieldValue:mData.record_id})
											};
											_promises.push(syncMesAdd(arrValues));
										}
									}
									return Promise.all(_promises).catch(function(err){
										Sentry.captureException(err);
										return null;
									});
								}
								return null;
							})
							.catch(function(err){
								Sentry.captureException(err);
								//console.log(mes.data.data);
								//console.log(err);
								return null;
							});
					}
					return null;
				}).then(function(){
					pubChannel.ack(msg);
				}).catch(err => {
					pubChannel.ack(msg);
					//console.log('err-sync_message-2');
					//console.log(err);
					Sentry.captureException(err);
					Sentry.addBreadcrumb({
						category: 'sync_message',
						message: 'sync_message err ',
						level: 'error',
						data:mes
					});
				});
			} catch (error) {
				//console.log('err-sync_message-1');
				//console.log(error);
				Sentry.captureException(error);
				Sentry.addBreadcrumb({
					category: 'sync_message',
					message: 'sync_message error ',
					level: 'error',
					data:mes
				});
			}
			break;
		}
		case 'watch_calc_tempo':{
			try {
				processModule.send({event:mes.type,queue:msg.fields.routingKey.replace('_client',''),id:mes.id,data:mes.data,messageId:mes.messageId});
				pubChannel.ack(msg);
			} catch (error) {
				//console.log('err-watch_calc_tempo');
				//console.log(error);
			}
			break;
		}
		case 'watch_info':{
			try {
				processModule.send({event:mes.type,queue:msg.fields.routingKey.replace('_client',''),id:mes.id,data:mes.data,messageId:mes.messageId});
				pubChannel.ack(msg);
			} catch (error) {
				//console.log('err-watch_info');
				//console.log(error);
			}
			break;
		}
		case 'watch_input_signal':{
			try {
				processModule.send({event:mes.type,queue:msg.fields.routingKey.replace('_client',''),id:mes.id,data:mes.data,messageId:mes.messageId});
				pubChannel.ack(msg);
			} catch (error) {
				//console.log('err-watch_input_signal');
				//console.log(error);
			}
			break;
		}
		case 'device_fault_start':{
			return Promise.all([device_fault_start(mes.data)]).then(function(){
				//console.log('pubChannel.ack:device_fault_start');
				let _promises=[];
				if(_ip==='10.10.0.10'&&(_port===8800||_port==='8800')){
					if(mes.data.losttype==='ARIZA KALIP-APARAT'){
						_promises.push(sendMesageToQueue(pubChannel, 'KALIPBAKIM', JSON.stringify({type:mes.type,client:mes.data.client,mould:mes.data.faultdevice,time:mes.data.start,employee:mes.data.employee})));
					}
				}
				pubChannel.ack(msg);
				if(_promises.length>0){
					return Promise.all(_promises);
				}
			}).catch(err => {
				//console.log('err-device_fault_start');
				//console.log(err);
				//console.log(mes);
				Sentry.captureException(err);
				pubChannel.ack(msg);
			});
		}
		case 'employee_fault_start':{
			return Promise.all([employee_fault_start(mes.data)]).then(function(){
				let _promises=[];
				if(_ip==='10.10.0.10'&&(_port===8800||_port==='8800')){
					if(mes.data.faulttype==='ARIZA KALIP-APARAT'){
						_promises.push(sendMesageToQueue(pubChannel, 'KALIPBAKIM', JSON.stringify({type:mes.type,client:mes.data.client,employee:mes.data.employee,time:mes.data.start})));
					}
				}
				pubChannel.ack(msg);
				if(_promises.length>0){
					return Promise.all(_promises);
				}
			}).catch(function(err){
				//console.log('catch-employee_fault_start-'+err);
				Sentry.captureException(err);
			});
		}
		case 'employee_fault_finish':{
			return Promise.all([controlStart(mes.data)]).then(function(res){
				if(!mes.data.start||mes.data.start===''){
					mes.data.start=res[0];
				}
				return Promise.all([employee_fault_finish(mes.data)]);
			}).then(function(){
				pubChannel.ack(msg);
			}).catch(function(err){
				//console.log('catch-employee_fault_finish-'+err);
				Sentry.captureException(err);
				pubChannel.ack(msg);
			});
		}
		case 'device_fault_finish':{
			return Promise.all([device_fault_finish(mes.data)]).then(function(){
				let _promises=[];
				if(_ip==='10.10.0.10'&&(_port===8800||_port==='8800')){
					if(mes.data.faulttype==='ARIZA KALIP-APARAT'){
						_promises.push(sendMesageToQueue(pubChannel, 'KALIPBAKIM', JSON.stringify({type:mes.type,client:mes.data.client,time:mes.data.start})));
					}
				}
				pubChannel.ack(msg);
				if(_promises.length>0){
					return Promise.all(_promises);
				}
			}).catch(function(err){
				//console.log('catch-device_fault_finish-'+err);
				Sentry.captureException(err);
				pubChannel.ack(msg);
			});
		}
		case 'control_answers':{
			return Promise.all([activity_control_answers(mes.data),control_answers(mes.data)]).then(function(){
				pubChannel.ack(msg);
			}).catch(function(err){
				//console.log('catch-control_answers-'+err);
				Sentry.captureException(err);
			});
		}
		case 'document_view_logs':{
			return Promise.all([document_view_logs(mes.data)]).then(function(){
				pubChannel.ack(msg);
			}).catch(function(err){
				//console.log('catch-document_view_logs-'+err);
				Sentry.captureException(err);
			});
		}
		case 'writelabel':{
			return Promise.all([writelabel(mes.data)]).then(function(){
				pubChannel.ack(msg);
			}).catch(function(err){
				//console.log('catch-writelabel-'+err);
				Sentry.captureException(err);
			});
		}
		case 'device_io_info':{
			processModule.send({event:mes.type,queue:msg.fields.routingKey.replace('_client',''),id:mes.id,data:mes.data,messageId:mes.messageId});
			pubChannel.ack(msg);
			break;
		}
		case 'res_query_null_record':{
			return sequelize.query('update '+mes.tableName+' set finish=:finish where finish is null and record_id=:record_id',{replacements:{finish:mes.finish,record_id:mes.record_id},type: sequelize.QueryTypes.UPDATE}).then(()=>{
				pubChannel.ack(msg);
			}).catch(function(err){
				//console.log('catch-res_query_null_record-'+err);
				Sentry.captureException(err);
			});
		}
		case 'device_workstart_outofsystem':{
			return sequelize.query('insert into work_out_system (client,day,jobrotation,time,description,employee,opnames) values(:client,:day,:jobrotation,:time,:description,:employee,:opnames)',{replacements:{client:mes.data.client,day:mes.data.day,jobrotation:mes.data.jobrotation,time:mes.data.time,description:mes.data.description,employee:mes.data.employee,opnames:(mes.data.opnames?mes.data.opnames:'')},type: sequelize.QueryTypes.INSERT}).then(()=>{
				pubChannel.ack(msg);
			}).catch(function(err){
				//console.log('catch-device_workstart_outofsystem-'+err);
				Sentry.captureException(err);
			});
		}
		case 'operation_authorization_device_log':{
			return sequelize.query('insert into operation_authorization_device_logs (time,employee,client,opname,authemployee,type,ismailsend) values (:time,:employee,:client,:opname,:authemployee,:type,false)',{replacements:{time:mes.data.time,employee:mes.data.employee,client:mes.data.client,opname:mes.data.opname,authemployee:mes.data.authemployee,type:mes.data.type.substr(0,100)},type: sequelize.QueryTypes.INSERT}).then(()=>{
				pubChannel.ack(msg);
			}).catch(function(err){
				//console.log('catch-operation_authorization_device_log-'+err);
				Sentry.captureException(err);
			});
		}
		case 'task_lists':{
			return Promise.all([task_lists(mes.data)]).then(function(){
				pubChannel.ack(msg);
			}).catch(function(err){
				//console.log('catch-task_lists-'+err);
				Sentry.captureException(err);
			});
		}
		case 'task_case_controls':{
			return Promise.all([task_case_controls(mes.data)]).then(function(){
				pubChannel.ack(msg);
			}).catch(function(err){
				//log('catch-task_case_controls-'+err);
				Sentry.captureException(err);
			});
		}
		case 'device_lost_finish':{
			return Promise.all([device_lost_finish(mes.data)]).then(function(){
				pubChannel.ack(msg);
			}).catch(function(err){
				//console.log('catch-device_lost_finish-'+err);
				Sentry.captureException(err);
			});
		}
		case 'device_work_finish':{
			return Promise.all([device_work_finish(mes.data)]).then(function(){
				pubChannel.ack(msg);
			}).catch(function(err){
				//console.log('catch-device_work_finish-'+err);
				Sentry.captureException(err);
			});
		}
		case 'kanban_info':{
			return sequelize.query('insert into kanban_over_products (client,product,boxcount,currentboxcount,time) values(:client,:product,:boxcount,:currentboxcount,:time)',{replacements:{client:mes.data.client,product:mes.data.product,boxcount:mes.data.boxcount,currentboxcount:mes.data.currentboxcount,time:mes.data.time},type: sequelize.QueryTypes.INSERT}).then(()=>{
				pubChannel.ack(msg);
			}).catch(function(err){
				//console.log('catch-kanban_info-'+err);
				Sentry.captureException(err);
			});
		}
		case 'control_task_new_or_old':{
			let mailOptions = {
				from: 'verimotrt@gmail.com',
				to: 'verimot.kalite@sahince.com.tr',
				subject: 'VerimotRT Yeni/Çok Eski Operasyona Başlama',
				text: mes.data.msg
			};
			mailTransporter.sendMail(mailOptions, function(error, info){
				//if (error) {
					//console.log(error);
				//} else {
					//console.log('Email sent-r: ' + info.response + mes.data.msg);
				//}
			});
			pubChannel.ack(msg);
			break;
		}
		case 'client_no_employee':{
			if(_ip==='10.10.0.10'){
				let mailOptions = {
					from: 'verimotrt@gmail.com',
					to: 'verimot.pres@sahince.com.tr',
					cc: 'kemal.isik@sahince.com.tr',
					subject: 'VerimotRT Vardiya Sonu İstasyondan Ayrılma',
					text: mes.data.client +' istasyonunda '+mes.data.time+' itibariyle kimse kalmamıştır. Son çalışan personel '+ mes.data.name+'.'
				};
				mailTransporter.sendMail(mailOptions, function(error, info){
					//if (error) {
					//	console.log(error);
					//} else {
					//	console.log('Email sent-r: ' + info.response + mes.data.msg);
					//}
				});
			}
			pubChannel.ack(msg);
			break;
		}
		case 'device_state_change':{
			try {
				let _promises=[];
				const notInStates=['Bekliyor','Tamamlandı','Setup Yapıldı','Yeniden İşlem Başladı','Yeniden İşlem Bitti'];
				if(notInStates.indexOf(mes.data.state)===-1){
					//if(_ip==='10.10.0.10'&&(_port===8800||_port==='8800')&&mes.data.client==='P113'){
					if(_ip==='10.10.0.10'){
						const qName=mes.data.client + '_status';
						_promises.push(sendMesageToQueue(pubChannel, qName, JSON.stringify(mes.data)));
					}
					if((_ip==='10.0.0.101'||_ip==='192.168.1.10')&&mes.data.state==='Devam Ediyor'&&mes.data.tasks.length>0){
						_promises.push(getNextTask(mes.data));
					}
				}
				processModule.send({event:mes.type,data:mes.data,messageId:mes.messageId,deliveryTag:msg.fields.deliveryTag});
				pubChannel.ack(msg);
				if(_promises.length>0){
					return Promise.all(_promises);
				}
			} catch (error) {
				//console.log('err-device_state_change');
				//console.log(error);
			}
			break;
		}
		case 'penetrations_response':{
			return Promise.all([penetrations_response(mes.data)]).then(function(){
				pubChannel.ack(msg);
			}).catch(function(err){
				Sentry.captureException(err);
			});
		}
		case 'export_data_machinaide':{
			return Promise.all([export_data_machinaide(mes.data)]).then(function(){
				pubChannel.ack(msg);
			}).catch(function(err){
				Sentry.captureException(err);
			});
		}
		}
	}catch(err){
		//console.log(err);
		Sentry.captureException(err);
		setTimeout(() => {
			process.exit(400);
		}, 500);
	}
};
let getNextTask=(obj)=>{
	const erprefnumber=obj.tasks[0].erprefnumber;
	const opname=obj.tasks[0].opname;
	const opcode=obj.tasks[0].opcode;
	let sql="select concat(SUBSTRING(cast(DATE_PART('year',coalesce(start,CURRENT_TIMESTAMP)) as varchar),3,2), right(concat('000',DATE_PART('doy',coalesce(start,CURRENT_TIMESTAMP))),3) ) bstring from task_lists where finish is null and erprefnumber=:erprefnumber and opname=:opname";
	return sequelize.query(sql,{replacements:{erprefnumber:erprefnumber,opname:opname}, type: sequelize.QueryTypes.SELECT}).then((result)=>{
		if(result.length>0){
			const bString=result[0].bstring;
			//let sqlf="select * from task_lists where finish is null and taskinfo is not null and erprefnumber=:erprefnumber "+
			//" and opname=(SELECT name from product_trees where parent=:opcode and name>:opname and description<>'AMBALAJ' order by number desc limit 1)";
			let sqlf="select * from task_lists where finish is null and taskinfo is not null and erprefnumber=:erprefnumber and opname>:opname limit 1";
			//,opcode:opcode
			return sequelize.query(sqlf,{replacements:{erprefnumber:erprefnumber,opname:opname}, type: sequelize.QueryTypes.SELECT}).then((resultf)=>{
				if(resultf.length>0){
					let clients=resultf[0].client;
					let record_id=resultf[0].record_id;
					let taskinfo=resultf[0].taskinfo;
					let opnamenext=resultf[0].opname;
					try {
						if(typeof taskinfo!=='object'){
							taskinfo=JSON.parse(resultf[0].taskinfo);
						}
						if(typeof taskinfo.printer2!=='undefined'&&typeof taskinfo.printer2.veri2!=='undefined'){
							let veri2=taskinfo.printer2.veri2;
							let arr2=veri2.split(' ');
							arr2[arr2.length-1]=bString;
							//taskinfo.printer2.veri2=arr2.join(' ');
							taskinfo.printer2.veri2=bString;
							return {record_id:record_id,erprefnumber:erprefnumber,opname:opnamenext,taskinfo:taskinfo,clients:clients};
						}
						return null;
					} catch (error) {
						Sentry.captureException(error);
						return null;
					}
				}else{
					return null;
				}
			}).then((obj)=>{
				if(obj!==null){
					return Promise.all([updateTaskInfo(obj)]);
				}
				return null;
			})
		}else{
			return null;
		}
	});
};

let updateTaskInfo=(obj)=>{
	let sql="update task_lists set taskinfo=:taskinfo where erprefnumber=:erprefnumber and opname=:opname and record_id=:record_id";
	return sequelize.query(sql, { replacements:{taskinfo:JSON.stringify(obj.taskinfo),erprefnumber:obj.erprefnumber,opname:obj.opname,record_id:obj.record_id}, type: sequelize.QueryTypes.UPDATE})
	.then(()=>{
		let arrClients=obj.clients.split(',');
		let p=[];
		for(let i=0;i<arrClients.length;i++){
			p.push(insert_sync_message(arrClients[i],'update_taskinfo',obj));
			p.push(insert__sync_message(arrClients[i],'update_taskinfo',obj));
		}
		return Promise.all(p);
	})
	.catch(function(err){
		Sentry.captureException(err);
		return null;
	});
};

let insert_sync_message=(client,type,data)=>{
	let sql_sm='insert into _sync_messages (ip,uuid,type,data,queued,"createdAt","updatedAt") '+
	'values (:ip,:uuid,:type,:data,0,CURRENT_TIMESTAMP(0),CURRENT_TIMESTAMP(0))';
	return sequelize.query(sql_sm, { replacements:{ip:client+'_server',uuid:null,type:type,data:JSON.stringify(data)}, type: sequelize.QueryTypes.INSERT})
	.catch(function(err){
		Sentry.captureException(err);
		return null;
	});
};

let insert__sync_message=(client,type,data)=>{
	let sqlInsertSyncMessage = `insert into __sync_messages (queuename,messagetype,priority,status,time,data,info) 
	values (:queuename,:messagetype,10,'wait_for_send',:time,:data,:info)`;
	return sequelize.query(sqlInsertSyncMessage, { replacements: {queuename:client+'_server',messagetype:type,time:getFullLocalDateString(),data:JSON.stringify(data),info:JSON.stringify({ tableName: 'task_lists' })}, type: sequelize.QueryTypes.INSERT })
		.catch(function(err){
			Sentry.captureException(err);
			return null;
		});
};

let controlStart=(obj)=>{
	if(obj.start&&obj.start!==''){
		return obj.start;
	}else{
		let sql='SELECT start from client_lost_details where day=:day and employee=:employee and client=:client and finish is null and start is not null limit 1';
		return sequelize.query(sql,{replacements:{day:obj.day,employee:obj.employee,client:obj.client}, type: sequelize.QueryTypes.SELECT})
			.then((result)=>{
				if(result.length>0){
					return convertFullLocalDateString(result[0].start);
				}else{
					return null;
				}
			});			
	}
};
let getFullLocalDateString=()=>{
	let time=new timezoneJS.Date();
	return time.toString('yyyy-MM-dd HH:mm:ss.SSS');
};
let convertFullLocalDateString=(_time,_ms)=>{
	var dt = new timezoneJS.Date(_time);
	return dt.toString('yyyy-MM-dd HH:mm:ss'+(_ms==true?'.SSS':''));
};
let device_fault_start=(obj)=>{
	switch (_export_fault) {
	case 'full_odbc_boys_ermetal':
	case 'only_start_odbc_boys_ermetal':{
		return new Promise(function(resolve, reject) {
			let _start=convertFullLocalDateString(obj.start);
			let makel=(obj.losttype=='ARIZA MAKİNE MEKANİK'?'T02':(obj.losttype=='ARIZA MAKİNE ELEKTRİK'?'T01':(obj.losttype=='ARIZA ÜRETİM'?'A1':'')));
			let faultdevice=obj.faultdevice;
			let employee=obj.employee;
			let faulttype=obj.faulttype;
			let vmid=obj.tasklist;
			if(makel==''||faultdevice===null){//kalıp arızaları export edilmeyecek 
				resolve();
			}else{
				let sql='SELECT convert_from(cast(name as bytea)::bytea, \'latin5\') personel,name from employees where finish is null and code=:code';
				return sequelize.query(sql,{replacements:{code:employee}, type: sequelize.QueryTypes.SELECT}).then((result)=>{
					let _personel='employee';
					if(result&&result.length>0){
						_personel=result[0].name;
					}
					return new Promise(function(/*resolve1, reject1*/) {
						let db = require('odbc');
						db.connect((makel=='A1'?_odbc_boys_prod:_odbc_boys), function (err,connection) {
							if (err){
								Sentry.captureException(err);
								reject(err);
							}else{
								if(faultdevice!==null){
									let sql='insert into VERIMOD_ENTEGRASYON (VMID,EKIPMANKODU,BAKIMKODU,TALEPEDEN,OLSTAR,OLSSA,BILTAR,BILSA,ARZKOMPKOD,HAT,KAYNAK,ISTIPIKODU) values '+
									' ( \''+vmid+'\',\''+faultdevice+'\',\'B01\',\''+_personel+'\',convert(datetime,\'' + _start.substr(0,10).split('-').join('') +'\',112),\''+_start.substr(11,5)+'\',convert(datetime,\'' + _start.substr(0,10).split('-').join('') +'\',112),\''+_start.substr(11,5)+'\',\''+faulttype+'\',\'\',\''+(makel=='A1'?'U':'B')+'\',\''+makel+'\')';
									return connection.query(sql).then(function(){
										connection.close((error) => {
											if (error) { 
												return; 
											} // handle
											// console.log('dbclose.done');
											resolve();
										});
									}).catch((err) => {
										//log(sql);
										Sentry.captureException(err);
										Sentry.addBreadcrumb({
											category: 'boys',
											message: 'boys_insert ',
											level: 'error',
											data:sql
										});
										connection.close((error) => {
											if (error) { 
												return; 
											} // handle
											// console.log('dbclose.done.err');
										});
									});
								}else{
									return new Promise(function(/*resolve4, reject4*/) {
										connection.close((error) => {
											if (error) { 
												return; 
											} // handle
											// console.log('dbclose.done.err');
											resolve();
										});
									});
								}
							}
						});
					});
				});
			}
		});
	}
	case 'only_start_odbc_boys':{
		return null;
	}
	case 'full_odbc_boys':{
		return null;
	}
	case 'only_start_json':{
		break;
	}
	case 'full_json':{
		break;
	}
	default:
		break;
	}
	let name=uuidv4()+'-f_c_s-'+obj.client;
	//console.log('device_fault_start:'+name);
	//console.log(obj);
	if(obj.faultdescription){
		obj.faultdescription=obj.faultdescription.split(',').join('|');
	}
	if(obj.descriptionlost){
		obj.descriptionlost=obj.descriptionlost.split(',').join('|');
	}
	if(obj.opname&&obj.opname.length>1){
		obj.opname=obj.opname.join('|');
	}
	return fs_writeFile(path_export+'ariza/'+name+'.json', JSON.stringify(obj))
		.catch((error) => {
			Sentry.captureException(error);
			Sentry.addBreadcrumb({
				category: 'f_c_s',
				message: JSON.stringify(obj),
				level: 'error',
				data:JSON.parse(JSON.stringify(error))
			});
		});
};
let employee_fault_start=(obj)=>{
	try {
		switch (_export_fault) {
		case 'full_json':{
			let name=uuidv4()+'-f_e_s-'+obj.client;
			//console.log('employee_fault_start:'+name);
			//console.log(obj);
			if(obj.faultdescription){
				obj.faultdescription=obj.faultdescription.split(',').join('|');
			}
			if(obj.descriptionlost){
				obj.descriptionlost=obj.descriptionlost.split(',').join('|');
			}
			return fs_writeFile(path_export+'ariza/'+name+'.json', JSON.stringify(obj))
				.catch((error) => {
					Sentry.captureException(error);
					Sentry.addBreadcrumb({
						category: 'f_e_s',
						message: JSON.stringify(obj),
						level: 'error',
						data:JSON.parse(JSON.stringify(error))
					});
				});
		}
		}
	} catch (error) {
		Sentry.captureException(error);
	}
};

let employee_fault_finish=(obj)=>{
	switch (_export_fault) {
	case 'full_json':{
		let name=uuidv4()+'-f_e_f-'+obj.client;
		//console.log('employee_fault_finish:'+name);
		//console.log(obj);
		if(obj.faultdescription){
			obj.faultdescription=obj.faultdescription.split(',').join('|');
		}
		if(obj.descriptionlost){
			obj.descriptionlost=obj.descriptionlost.split(',').join('|');
		}
		return fs_writeFile(path_export+'ariza/'+name+'.json', JSON.stringify(obj))
			.catch((error) => {
				Sentry.captureException(error);
				Sentry.addBreadcrumb({
					category: 'f_e_f',
					message: JSON.stringify(obj),
					level: 'error',
					data:JSON.parse(JSON.stringify(error))
				});
			});
	}
	case 'full_odbc_boys_ermetal':{
		return new Promise(function(resolve, reject) {	
			let _start=convertFullLocalDateString(obj.start);
			let _finish=convertFullLocalDateString(obj.finish);
			let d1=new Date(_start);
			let d2=new Date(_finish);
			let _sure=Math.floor((d2.getTime()-d1.getTime())/1000/60);
			let makel=(obj.faulttype=='ARIZA MAKİNE MEKANİK'?'T02':(obj.faulttype=='ARIZA MAKİNE ELEKTRİK'?'T01':(obj.faulttype=='ARIZA ÜRETİM'?'A1':'')));
			let faultdevice=obj.faultdevice;
			let employee=obj.employee;
			let vmid=obj.tasklist;
			if(makel==''||faultdevice===null){//kalıp arızaları export edilmeyecek 
				resolve();
			}else{
				//console.log(obj);
				let sql='SELECT convert_from(cast(name as bytea)::bytea, \'latin5\') personel,name from employees where finish is null and code=:code and (ismaintenance=true or ismould=true)';
				return sequelize.query(sql,{replacements:{code:employee}, type: sequelize.QueryTypes.SELECT}).then((result)=>{
					// let _personel='employee';
					if(result&&result.length>0){
						// _personel=result[0].personel;
						return new Promise(function(/*resolve1, reject1*/) {
							let db = require('odbc');
							db.connect((makel=='A1'?_odbc_boys_prod:_odbc_boys), function (err,connection) {
								if (err){
									Sentry.captureException(err);
									reject(err);
								}else{
									let sql='insert into VERIMOD_ENTEGRASYON_KAYNAK (KAYNAKKOD,BASTAR,BASSA,BITSA,SURE,VMID) values '+
									' ( \''+employee+'\',convert(datetime,\'' + _start.substr(0,10).split('-').join('') +'\',112),\''+_start.substr(11,5)+'\',\''+_finish.substr(11,5)+'\',\''+_sure+'\',\''+vmid+'\')';
									return connection.query(sql).then(function(){
										connection.close((error) => {
											if (error) { 
												return; 
											} // handle
											// console.log('dbclose.done');
											resolve();
										});
									}).catch((err) => {
										//console.log(sql);
										Sentry.captureException(err);
										Sentry.addBreadcrumb({
											category: 'boys',
											message: 'boys_insert ',
											level: 'error',
											data:sql
										});
										connection.close((error) => {
											if (error) { 
												return; 
											} // handle
											// console.log('dbclose.done.err');
											resolve();
										});
									});
								}
							});
						});
					}else{
						return null;
					}
				});
			}
		});
	}
	}
};
let device_fault_finish=(obj)=>{
	switch (_export_fault) {
	case 'full_json':{
		let name=uuidv4()+'-f_c_f-'+obj.client;
		//console.log('device_fault_finish:'+name);
		//console.log(obj);
		if(obj.faultdescription){
			obj.faultdescription=obj.faultdescription.split(',').join('|');
		}
		if(obj.descriptionlost){
			obj.descriptionlost=obj.descriptionlost.split(',').join('|');
		}
		return fs_writeFile(path_export+'ariza/'+name+'.json', JSON.stringify(obj))
			.catch((error) => {
				Sentry.captureException(error);
				Sentry.addBreadcrumb({
					category: 'f_c_f',
					message: JSON.stringify(obj),
					level: 'error',
					data:JSON.parse(JSON.stringify(error))
				});
			});
	}
	}
};
let create_record_acr=(client,day,jobrotation,employee,time,data)=>{
	let sql='insert into activity_control_answers '+
	'(type,client,day,jobrotation,opname,erprefnumber,employee,time,documentnumber '+
	',question,answertype,valuerequire,valuemin,valuemax,answer,create_user_id,created_at) values '+
	'(:type,:client,:day,:jobrotation,:opname,:erprefnumber,:employee,:time,:documentnumber '+
	',:question,:answertype,:valuerequire,:valuemin,:valuemax,:answer,1,CURRENT_TIMESTAMP(0))';
	return sequelize.query(sql,{replacements:{
		type:data.type
		,client:client
		,day:day
		,jobrotation:jobrotation
		,opname:data.opname
		,erprefnumber:data.erprefnumber
		,employee:employee
		,time:time
		,documentnumber:data.documentnumber
		,question:data.question
		,answertype:data.answertype
		,valuerequire:data.valuerequire
		,valuemin:data.valuemin
		,valuemax:data.valuemax
		,answer:data.answer
	},type: sequelize.QueryTypes.INSERT}).catch(function(err){
		//console.log('catch-create_record_acr-'+err);
		Sentry.captureException(err);
	});
};
let activity_control_answers=(obj)=>{
	let name=uuidv4();
	let _promises=[];
	let tmp=[];
	for(let i=0;i<obj.data.length;i++){
		let row=obj.data[i];
		if(row.qtype==='activity_control_questions'){
			tmp.push(row);
			_promises.push(create_record_acr(obj.client,obj.day,obj.jobrotation,obj.employee,obj.time,row));
		}
	}
	return Promise.all(_promises).then(function(){
		if(tmp.length>0){
			obj.data=tmp;
			return fs_writeFile(path_export+'activity_control_answers/'+name+'.json', JSON.stringify(obj)).catch((error) => {
				Sentry.captureException(error);
				Sentry.addBreadcrumb({
					category: 'activity_control_answers',
					message: JSON.stringify(obj),
					level: 'error',
					data:JSON.parse(JSON.stringify(error))
				});
			});
		}
		return;
	}).catch(function(err){
		//console.log('catch-activity_control_answers-'+err);
		Sentry.captureException(err);
	});
};
let control_answers=(obj)=>{
	let name=uuidv4();
	obj.details=[];
	for(let i=0;i<obj.data.length;i++){
		let row=obj.data[i];
		if(row.qtype==='control_questions'||row.qtype==='control_questions_isg'){
			if(row.type==='OTONOM KONTROL'){
				let mailcontent='';
				let tomails='';
				if(row.question==='GİDEN VARDİYA OTONOM BAKIM YAPIP FORMU DOLDURMUŞ MU?'){
					if(row.answer===0||row.answer==='0'){
						mailcontent='Giden vardiya otonom bakım yapmamıştır.\n\nİstasyon: '+obj.client+'\nOperatör sicili: '+obj.employee+'\nVardiya: '+obj.jobrotation+'\nZaman: '+obj.time+'\n';
					}
				}
				if(row.question==='OTONOM BAKIM YAPILIP, FORM DOLDURULDU MU?'){
					if(row.answer===0||row.answer==='0'){
						mailcontent='Otonom bakım yapılmamıştır.\n\nİstasyon: '+obj.client+'\nOperatör sicili: '+obj.employee+'\nVardiya: '+obj.jobrotation+'\nZaman: '+obj.time+'\n';
					}
				}
				if(mailcontent!==''){
					let _cc='';
					if(_ip==='10.10.0.10'){
						_cc='verimot.kalite@sahince.com.tr';
						if(_port===8700||_port==='8700'){
							tomails='verimot.kaynak@sahince.com.tr';
						}
						if(_port===8800||_port==='8800'){
							tomails='verimot.pres@sahince.com.tr';
						}
						if(_port===8900||_port==='8900'){
							tomails='verimot.punta@sahince.com.tr';
						}
					}
					let mailOptions = {
						from: 'verimotrt@gmail.com',
						to: tomails,
						cc: _cc,
						subject: 'VerimotRT Otonom Kontrol Uygunsuz Durum Bildirimi',
						text: mailcontent
					};
					mailTransporter.sendMail(mailOptions, function(error, info){
						//if (error) {
						//	console.log(error);
						//} else {
						//	console.log('Email sent-r2: ' + info.response + mailcontent);
						//}
					});
				}
			}
			if(row.type==='SOP'){
				if( (row.answertype==='Evet-Hayır'&&parseInt(row.valuerequire)!==parseInt(row.answer))  
					|| (row.answertype==='Rakam Değer'&& ( parseInt(row.answer)<parseInt(row.valuemin) || parseInt(row.answer)>parseInt(row.valuemax) ) )
				){
					let mailcontent='İstasyon: '+row.client+'\n'+'Operasyon: '+row.opname+'\n'+'Sicil: '+row.employee+'\n'+row.question +' sorusuna beklenen değer dışında cevap verildi.';
					let tomails='ozilix@gmail.com';
					let _cc='';
					if(mailcontent!==''){
						if(_ip==='10.10.0.10'){
							_cc='verimot.kalite@sahince.com.tr';
							if(_port===8700||_port==='8700'){
								tomails='verimot.kaynak@sahince.com.tr';
							}
							if(_port===8800||_port==='8800'){
								tomails='verimot.pres@sahince.com.tr';
							}
							if(_port===8900||_port==='8900'){
								tomails='verimot.punta@sahince.com.tr';
							}
						}
						let mailOptions = {
							from: 'verimotrt@gmail.com',
							to: tomails,
							cc: _cc,
							subject: 'VerimotRT SOP Uygunsuz Cevap Bildirimi',
							text: mailcontent
						};
						mailTransporter.sendMail(mailOptions, function(error, info){
							//if (error) {
							//	console.log(error);
							//} else {
							//	console.log('Email sent-r3: ' + info.response + mailcontent);
							//}
						});
					}
				}
			}
			
			const idx=getIndex(obj.details,'erprefnumber',row.erprefnumber);
			//const idx=getIndexMulti(obj.details,'erprefnumber|opname',row.erprefnumber);
			if(idx===-1){
				let _x=[];
				const _erp=row.erprefnumber;
				const _tsk=row.taskcode;
				delete obj.data[i].erprefnumber;
				delete obj.data[i].taskcode;
				_x.push(obj.data[i]);
				obj.details.push({erprefnumber:_erp,taskcode:_tsk,data:_x});
			}else{
				delete obj.data[i].erprefnumber;
				delete obj.data[i].taskcode;
				obj.details[idx].data.push(obj.data[i]);
			}
		}
	}
	obj.data=null;
	return fs_writeFile(path_export+'kontrolcevap/'+name+'.json', JSON.stringify(obj))
		.catch((error) => {
			Sentry.captureException(error);
			Sentry.addBreadcrumb({
				category: 'control_answers',
				message: JSON.stringify(obj),
				level: 'error',
				data:JSON.parse(JSON.stringify(error))
			});
		});
};
let create_record_dvl=(client,day,jobrotation,employee,time,data)=>{
	let sql='insert into document_view_logs (type,path,opcode,opnumber,opname,currentversion '+
	',client,day,jobrotation,employee,erprefnumber,time,create_user_id,created_at) '+
	'values (:type,:path,:opcode,:opnumber,:opname,:currentversion '+
	',:client,:day,:jobrotation,:employee,:erprefnumber,:time,1,CURRENT_TIMESTAMP(0))';
	return sequelize.query(sql,{replacements:{
		type:data.type
		,path:data.path
		,opcode:data.opcode
		,opnumber:data.opnumber
		,opname:data.opname
		,currentversion:data.currentversion
		,client:client
		,day:day
		,jobrotation:jobrotation
		,employee:employee
		,erprefnumber:data.erprefnumber
		,time:time
	},type: sequelize.QueryTypes.INSERT}).catch(function(err){
		//console.log('catch-create_record_dvl-'+err);
		Sentry.captureException(err);
	});
};
let document_view_logs=(obj)=>{
	let _promises=[];
	for(let i=0;i<obj.data.length;i++){
		let row=obj.data[i];
		_promises.push(create_record_dvl(obj.client,obj.day,obj.jobrotation,obj.employee,obj.time,row));
	}
	return Promise.all(_promises).catch(function(err){
		//console.log('catch-document_view_logs-'+err);
		Sentry.captureException(err);
	});
};
let task_lists=(obj)=>{
	let name=uuidv4();
	if(_ip==='10.0.0.101'){
		return fs_writeFile(path_export+'task_lists/'+name+'.json', JSON.stringify(obj))
			.catch((error) => {
				Sentry.captureException(error);
				Sentry.addBreadcrumb({
					category: 'task_lists',
					message: JSON.stringify(obj),
					level: 'error',
					data:JSON.parse(JSON.stringify(error))
				});
			});
	}
	return null;
};
let task_case_controls=(obj)=>{
	let name=uuidv4();
	if(_ip==='10.10.0.10'||_ip==='192.168.1.40'||_ip==='192.168.1.10'){
		let __opname=(typeof obj.opcode!=='undefined'?obj.opcode:(typeof obj.opname!=='undefined'?obj.opname:'')).split('\\').join('_').split('/').join('_');
		return fs_writeFile(path_export+'task_case_controls/'+obj.client+'_'+obj.day+'_'+__opname+'_'+name+'.json', JSON.stringify(obj))
			.catch((error) => {
				Sentry.captureException(error);
				Sentry.addBreadcrumb({
					category: 'task_case_controls',
					message: JSON.stringify(obj),
					level: 'error',
					data:JSON.parse(JSON.stringify(error))
				});
			});
	}
	return null;
};
let device_lost_finish=(obj)=>{
	let name=uuidv4();
	return fs_writeFile(path_export+'uretim/kayipbitir/'+name+'.json', JSON.stringify(obj))
		.catch((error) => {
			Sentry.captureException(error);
			Sentry.addBreadcrumb({
				category: 'device_lost_finish',
				message: JSON.stringify(obj),
				level: 'error',
				data:JSON.parse(JSON.stringify(error))
			});
		});
};
let device_work_finish=(obj)=>{
	let name=uuidv4();
	if(_ip==='10.10.0.10'){
		//if(_port===8800||_port==='8800'){
		//	const recData=obj.finish_data[0];
		//	let mailcontent='Zaman:'+obj.start+'\n\nİstasyon:'+obj.client+'\n\nKalıp Kodu:'+recData.mould+'\n\nOperasyon:'+recData.opname;
		//	let tomails='kadir.yasar@sahince.com.tr,mert.bakir@sahince.com.tr,oguzcan.ever@sahince.com.tr';
		//	let mailOptions = {
		//		from: 'verimotrt@gmail.com',
		//		to: tomails,
		//		cc: 'kemal.isik@sahince.com.tr',
		//		subject: 'VerimotRT Üretim Sonu Kalıp Sökme Bildirimi',
		//		text: mailcontent
		//	};
		//	mailTransporter.sendMail(mailOptions, function(error, info){
		//		if (error) {
		//			console.log(error);
		//		} else {
		//			console.log('Email sent-r2: ' + info.response + mailcontent);
		//		}
		//	});
		//}
		let _arr=['YMTFS 00520615780E-PRK-10','YMTFS 00520615790E-PRK-10'];
		let _f=false;
		for(let i=0;i<obj.opname.length;i++){
			const item=obj.opname[i];
			if(!_f&&_arr.indexOf(item)>-1){
				_f=true;
			}
		}
		if(_f){
			const _data={
				type: 'f_c_s'
				,client:obj.client
				,day:obj.day
				,start:obj.start
				,losttype: 'ARIZA MAKİNE ELEKTRİK'
				,descriptionlost:'TEZGAHTAKİ FOTOSELLERİN DEVREYE ALINMASI'
				,faulttype:'ARIZA'
				,faultdescription:'TEZGAHTAKİ FOTOSELLERİN DEVREYE ALINMASI'
				,faultgroupcode: 'ARIZA MAKİNE ELEKTRİK'
				,faultdevice: obj.client
				,employee: obj.employee
				,tasklist: obj.tasklist
				,taskcode: ''
			};
			return fs_writeFile(path_export+'ariza/'+name+'.json', JSON.stringify(_data))
				.catch((error) => {
					Sentry.captureException(error);
					Sentry.addBreadcrumb({
						category: 'device_work_finish',
						message: JSON.stringify(obj),
						level: 'error',
						data:JSON.parse(JSON.stringify(error))
					});
				});
		}
	}
	// Sentry.captureMessage('[device_work_finish] ' + JSON.stringify(obj));
	if(_ip==='172.16.1.143'||_ip==='172.16.1.144'){
		try {
			const recData=obj.finish_data[0];
			if(typeof recData==='undefined'){
				Sentry.captureMessage('[device_work_finish] ' + JSON.stringify(obj));
			}
			// ermetal iş sonu dosya çıkarılması
			//let content=JSON.stringify(recData);
			let content=recData.erprefnumber+';'+recData.opname+';'+recData.productdonecount+';\r\n';
			return fs_writeFile(path_export+'uretim/uretimsonu/'+name+'.json', content)
				.catch((error) => {
					Sentry.captureException(error);
					Sentry.addBreadcrumb({
						category: 'device_work_finish',
						message: JSON.stringify(obj),
						level: 'error',
						data:JSON.parse(JSON.stringify(error))
					});
				});
		} catch (error) {
			Sentry.addBreadcrumb({
				category: 'device_work_finish',
				message: 'device_work_finish ',
				level: 'error',
				data:JSON.stringify(obj)
			});
			Sentry.captureException(error);
		}
	}
	return null;
};
let penetrations_response=(obj)=>{
	let name=uuidv4();
	return fs_writeFile(path_export+'penetrations/'+name+'.json', JSON.stringify(obj))
		.catch((error) => {
			Sentry.captureException(error);
			Sentry.addBreadcrumb({
				category: 'penetrations_response',
				message: JSON.stringify(obj),
				level: 'error',
				data:JSON.parse(JSON.stringify(error))
			});
		});
};
let export_data_machinaide=(obj)=>{
	return new Promise(function(resolve, reject) {	
		const odbcMachinaide = "Driver={SQL Server};Server=10.16.5.2;Database=MNADE;Uid=kaitek;Pwd=nx0KNHWoCQF0MhSV5vv4;Persist Security Info=True;Application Name=Ermetal;";
		odbc.connect(odbcMachinaide, function (err,connection) {
			if (err){
				Sentry.captureException(err);
				reject(err);
			}else{
				const sql = "insert into verimot_losts (client, day, jobrotation, start, losttype, descriptionlost, faulttype, faultdescription, faultgroupcode, faultdevice, employees, productions) values "+
				"('" + obj.client + "',convert(datetime,'" + obj.day.substr(0, 10).split('-').join('') + "',112) ,'" + obj.jobrotation + "','" + obj.start + "','" + obj.losttype + "','" + obj.descriptionlost + "','" + obj.faulttype + "','" + obj.faultdescription + "','" + obj.faultgroupcode + "','" + obj.faultdevice + "','" + JSON.stringify(obj.employees) + "','" + JSON.stringify(obj.productions) + "')";
				return connection.query(sql).then(function(){
					connection.close((error) => {
						if (error) { 
							return; 
						} // handle
						resolve();
					});
				}).catch((err) => {
					Sentry.captureException(err);
					Sentry.addBreadcrumb({
						category: 'boys',
						message: 'export_data_machinaide ',
						level: 'error',
						data:sql
					});
					connection.close((error) => {
						if (error) { 
							return; 
						} // handle
						resolve();
					});
				});
			}
		});
	});
};
let writelabel=(obj)=>{
	let name=uuidv4();
	return fs_writeFile(path_export+'etiket/'+name+'.json', JSON.stringify(obj))
		.catch((error) => {
			Sentry.captureException(error);
			Sentry.addBreadcrumb({
				category: 'writelabel',
				message: JSON.stringify(obj),
				level: 'error',
				data:JSON.parse(JSON.stringify(error))
			});
		});
};
let getIndex=(array,key,keyValue)=>{
	if(typeof array.length!='undefined')
		for(let i = 0, len = array.length; i < len; i++){
			if(array[i][key]==keyValue)	return i;
		}
	else
		for(let i in array)
			if(typeof array[i]!='function')
				if(array[i][key]==keyValue)	return i;
	return -1;
};

//let getIndexMulti=(array,key,keyValue)=>{
//	if(typeof array.length!='undefined')
//		for(let i = 0, len = array.length; i < len; i++){
//			if(key.indexOf('|')==-1){
//				if(array[i][key]==keyValue)	return i;
//			}else{
//				let f=true;
//				let arr_key=key.split('|');
//				let arr_val=keyValue.split('|');
//				for(let j=0;j<arr_key.length;j++){
//					let v_row=array[i][arr_key[j]];
//					let v_val=((arr_val[j]=='true'||arr_val[j]=='false')?Boolean(arr_val[j]):arr_val[j]);
//					v_row=(v_row==null||v_row=='null')?null:v_row;
//					v_val=(v_val==null||v_val=='null')?null:v_val;
//					if(v_row!=v_val){
//						f=false;
//					}
//				}
//				if(f) return i;
//			}
//		}
//	else
//		for(let i in array)
//			if(typeof array[i]!='function'){
//				if(key.indexOf('|')==-1){
//					if(array[i][key]==keyValue)	return i;
//				}else{
//					let f=true;
//					let arr_key=key.split('|');
//					let arr_val=keyValue.split('|');
//					for(let j=0;j<arr_key.length;j++){
//						if(array[i][arr_key[j]]!=arr_val[j]){
//							f=false;
//						}
//					}
//					if(f) return i;
//				}
//			}
//	return -1;
//};