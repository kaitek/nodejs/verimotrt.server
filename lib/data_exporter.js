"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Sentry = require("@sentry/node");
const fs = require("fs");
const nodemailer = require("nodemailer");
const odbc = require("odbc");
const path = require("path");
const processModule = require("process");
const sequelize_1 = require("sequelize");
const timezoneJS = require("timezone-js");
Sentry.init({
    dsn: 'https://4e61d0b3664d41c9a849f8edbb2b2255@sentry.kaitek.com.tr/3'
});
let sequelize;
let pid = 0;
let customer = '';
let ip = '';
let port = 0;
let exportData = '';
let isExportLost = false;
let isExportProd = false;
let isExportReject = false;
let odbcMerga = '';
let isExportMerga = false;
let connMerga = null;
let connMergaTrans = null;
let lastJrwp = '';
let mailTransporter = null;
let prevWaitExpert = [];
let prevWaitForklift = [];
let pathExport = path.join(__dirname, '..', '_export/');
let CompanyUnitCode = '';
let messages = [];
processModule.on('message', (obj) => __awaiter(void 0, void 0, void 0, function* () {
    let event = obj.event;
    switch (event) {
        case 'start_data_exporter': {
            pid = obj.pid;
            exportData = obj.export_data;
            customer = obj.customer;
            ip = obj.ip;
            port = obj.port;
            odbcMerga = (typeof obj.odbc_merga === 'string' ? obj.odbc_merga : '');
            isExportLost = (exportData.indexOf('E_ALL') > -1 || exportData.indexOf('E_LOST') > -1);
            isExportProd = (exportData.indexOf('E_ALL') > -1 || exportData.indexOf('E_PROD') > -1);
            isExportReject = (exportData.indexOf('E_ALL') > -1 || exportData.indexOf('E_REJECT') > -1);
            let param = obj.param;
            processModule.send({
                event: 'data_exporter_started',
                pid,
                export_data: exportData,
                b_e_lost: isExportLost,
                b_e_prod: isExportProd,
                b_e_reject: isExportReject,
                path: pathExport
            });
            if (odbcMerga !== '') {
                if (ip === '172.16.1.143' && port === 8900) {
                    CompanyUnitCode = 'Kaynak2';
                }
                if (ip === '172.16.1.143' && port === 9000) {
                    CompanyUnitCode = 'Mekanik';
                }
                if (ip === '172.16.1.144' && port === 8800) {
                    CompanyUnitCode = 'Kaynak1';
                }
                if (ip === '172.16.1.144' && port === 8900) {
                    CompanyUnitCode = 'Pres';
                }
            }
            yield dbConnect(param);
            yield initMailer();
            yield controlExportData();
            break;
        }
        case 'device_state_change': {
            if ((odbcMerga !== '') || ip === '192.168.1.40') {
                messages.push(obj.data);
            }
            break;
        }
    }
}));
processModule.on('uncaughtException', (err) => {
    Sentry.captureException(err);
    setTimeout(() => {
        process.exit(300);
    }, 500);
    return;
});
let dbConnect = (obj) => {
    sequelize = new sequelize_1.Sequelize(obj.database, obj.username, obj.password, {
        host: obj.host,
        dialect: 'postgres',
        logging: false,
        pool: {
            max: 10,
            min: 0,
            acquire: 100000,
            idle: 10000
        },
        timezone: obj.timezone
    });
    return sequelize
        .authenticate()
        .catch(err => {
        Sentry.captureException(err);
        setTimeout(() => {
            process.exit(301);
        }, 500);
        return;
    });
};
let initMailer = () => __awaiter(void 0, void 0, void 0, function* () {
    mailTransporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'verimotrt@gmail.com',
            pass: 'mkktjptwruhmczmg'
        }
    });
    return;
});
let controlExportData = () => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    let x = new Date();
    try {
        const _updater = (_casenumber, _erprefnumber, _opname, _packcapacity, _quantityremaining, _recordid, _retouch) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                let str = 'Kasa:' + _casenumber + ' ERP Ref No:' + _erprefnumber + ' Operasyon:' + _opname + ' Kasa Kapasite:'
                    + _packcapacity + ' Tesellüm Edilen Miktar:' + _quantityremaining;
                let _filepath = pathExport + 'tesellum/' + _casenumber + '-O.txt';
                if (customer === 'sahince') {
                    str = JSON.stringify({
                        casenumber: _casenumber,
                        erprefnumber: _erprefnumber,
                        opname: _opname,
                        packcapacity: _packcapacity,
                        quantityremaining: _quantityremaining,
                        record_id: _recordid,
                        retouch: _retouch
                    });
                    _filepath = pathExport + 'tesellum/' + _casenumber + '.json';
                }
                fs.writeFileSync(_filepath, str);
                sql = 'UPDATE task_cases SET status=:status,updated_at=CURRENT_TIMESTAMP(0) WHERE record_id=:record_id ';
                yield sequelize.query(sql, { replacements: { status: 'DELIVERED', record_id: _recordid }, type: sequelize.QueryTypes.UPDATE });
                return;
            }
            catch (error) {
                Sentry.captureException(error);
                setTimeout(() => {
                    process.exit(302);
                }, 500);
                return;
            }
        });
        sql = 'SELECT record_id,casenumber,erprefnumber,opname,packcapacity,quantityremaining,status from "task_cases" where status in (:status) order by id limit 10 ';
        const results = yield sequelize.query(sql, { replacements: { status: ['WAIT_FOR_INFO_SEND', 'WAIT_FOR_INFO_SEND_RETOUCH'] }, type: sequelize.QueryTypes.SELECT });
        for (const result of results) {
            yield _updater(result.casenumber, result.erprefnumber, result.opname, result.packcapacity, result.quantityremaining, result.record_id, (result.status === 'WAIT_FOR_INFO_SEND_RETOUCH' ? 1 : 0));
        }
        sql = 'SELECT record_id,casenumber,erprefnumber,opname,packcapacity,quantityremaining,status from "task_cases" where movementdetail is null and status in (:status) order by id limit 10 ';
        const resultsTC = yield sequelize.query(sql, { replacements: { status: ['CLOSE'] }, type: sequelize.QueryTypes.SELECT });
        const _updaterClosedTC = (_casenumber, _erprefnumber, _opname, _packcapacity, _quantityremaining, _recordid) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                let str = 'Kasa:' + _casenumber + ' ERP Ref No:' + _erprefnumber + ' Operasyon:'
                    + _opname + ' Kasa Kapasite:' + _packcapacity + ' İptal Edilen Kasa İçi Miktar:' + _quantityremaining;
                let _filepath = pathExport + 'tesellum/' + _casenumber + '-O.txt';
                if (customer === 'sahince') {
                    str = JSON.stringify({
                        casenumber: _casenumber,
                        erprefnumber: _erprefnumber,
                        opname: _opname,
                        packcapacity: _packcapacity,
                        quantityremaining: _quantityremaining,
                        record_id: _recordid,
                        status: 'CANCELED'
                    });
                    _filepath = pathExport + 'tesellum/' + _casenumber + '.json';
                }
                fs.writeFileSync(_filepath, str);
                sql = 'UPDATE task_cases SET status=:status WHERE record_id=:record_id ';
                yield sequelize.query(sql, { replacements: { status: 'CANCELED', record_id: _recordid }, type: sequelize.QueryTypes.UPDATE });
                return;
            }
            catch (error) {
                Sentry.captureException(error);
                setTimeout(() => {
                    process.exit(302);
                }, 500);
                return;
            }
        });
        if (customer === 'sahince') {
            for (const resultTC of resultsTC) {
                yield _updaterClosedTC(resultTC.casenumber, resultTC.erprefnumber, resultTC.opname, resultTC.packcapacity, resultTC.quantityremaining, resultTC.record_id);
            }
        }
        const eLost = () => __awaiter(void 0, void 0, void 0, function* () {
            if (customer === 'ermetal') {
                return null;
            }
            const sqlCLD = 'select cld.*,lt.extcode,case when cld.type=\'l_c_t\' then (select count(*) from(select count(*) from client_lost_details where client=cld.client and start=cld.start and type=\'l_e\' GROUP BY opname)a) else 0 end empcount from client_lost_details cld left join lost_types lt on lt.code=cld.losttype where cld.day>current_date - interval \'30 day\' and coalesce(cld.losttype,\'\')<>\'\' and cld.finish is not null and cld.isexported is null order by cld.start,cld.id limit 10';
            const resultsCLD = yield sequelize.query(sqlCLD, { type: sequelize.QueryTypes.SELECT });
            const cldUpdate = (item) => __awaiter(void 0, void 0, void 0, function* () {
                try {
                    let _item = item;
                    _item.start = convertFullLocalDateString(new timezoneJS.Date(_item.start), false);
                    _item.finish = convertFullLocalDateString(new timezoneJS.Date(_item.finish), false);
                    if (typeof _item.faultdescription === 'string') {
                        _item.faultdescription = _item.faultdescription.split(',').join('|');
                    }
                    if (typeof _item.descriptionlost === 'string') {
                        _item.descriptionlost = _item.descriptionlost.split(',').join('|');
                    }
                    fs.writeFileSync(pathExport + 'kayip/' + _item.id + '-' + _item.type + '.json', JSON.stringify(_item));
                    const sqlUpdate = 'update client_lost_details set isexported=true where id=:id';
                    yield sequelize.query(sqlUpdate, { replacements: { id: _item.id }, type: sequelize.QueryTypes.UPDATE });
                    return;
                }
                catch (error) {
                    Sentry.captureException(error);
                    setTimeout(() => {
                        process.exit(305);
                    }, 500);
                    return;
                }
            });
            for (const recordCLD of resultsCLD) {
                yield cldUpdate(recordCLD);
            }
            return;
        });
        const eProd = () => __awaiter(void 0, void 0, void 0, function* () {
            let sqlCPD = 'select * from client_production_details where day>current_date - interval \'30 day\' and ((isexported=false and finish is not null) or (isexported is null)) and type not like \'c_s%\' order by start,id limit 10';
            if (customer === 'ermetal') {
                sqlCPD = 'select cpd.id,cpd.type,cpd.client,c.info,cpd.mould,cpd.opname,cpd.opdescription,cpd.production,case when (length(cpd.erprefnumber)<5 or cpd.erprefnumber is null) then \'IS EMRI DISI\' else cpd.erprefnumber end erprefnumber,cpd.start,cpd.finish,cpd.isexported ' +
                    ' from client_production_details cpd ' +
                    ' left join clients c on c.code=cpd.client ' +
                    ' where cpd.day>current_date - interval \'10 day\' and (cpd.isexported is null and cpd.finish is not null) and cpd.type not like \'c_s%\' and c.uuid is not null ' +
                    ' order by cpd.start,cpd.id limit 100';
            }
            const resultsCPD = yield sequelize.query(sqlCPD, { type: sequelize.QueryTypes.SELECT });
            let cpdUpdate = (item) => __awaiter(void 0, void 0, void 0, function* () {
                try {
                    let _item = item;
                    let _recType = 'S';
                    _item.start = convertFullLocalDateString(new timezoneJS.Date(_item.start), false);
                    if (_item.isexported === false) {
                        _recType = 'F';
                        if (_item.taskfinishdescription !== null) {
                            _item.taskfinishdescription = _item.taskfinishdescription.split(',').join('|');
                        }
                        _item.finish = convertFullLocalDateString(new timezoneJS.Date(_item.finish), false);
                    }
                    else if (_item.isexported === null) {
                        _recType = 'S';
                        if (customer === 'ermetal') {
                            _recType = 'F';
                            _item.finish = convertFullLocalDateString(new timezoneJS.Date(_item.finish), false);
                        }
                    }
                    const filePath = pathExport + 'uretim/' + _item.type + '_' + _recType + '/' + _item.id + '-'
                        + (_recType === 'S' ? '0' : '1') + '-' + _item.type + '_' + _recType + '_' + item.client + '.json';
                    let content = JSON.stringify(_item);
                    if (customer === 'ermetal') {
                        content = (_item.client + ';' + _item.info + ';' + _item.mould + ';' + _item.opname + ';'
                            + _item.opdescription + ';' + _item.production + ';' + _item.erprefnumber + ';'
                            + _item.start + ';' + _item.finish + ';\r\n');
                    }
                    fs.writeFileSync(filePath, content);
                    const sqlUpdate = 'update client_production_details set isexported=' + (_recType === 'S' ? 'false' : 'true') + ' where id=:id';
                    yield sequelize.query(sqlUpdate, { replacements: { id: _item.id }, type: sequelize.QueryTypes.UPDATE });
                    return;
                }
                catch (error) {
                    Sentry.captureException(error);
                    setTimeout(() => {
                        process.exit(308);
                    }, 500);
                    return;
                }
            });
            for (const recordCPD of resultsCPD) {
                yield cpdUpdate(recordCPD);
            }
            return;
        });
        const eReject = () => __awaiter(void 0, void 0, void 0, function* () {
            const sqlTRD = 'select trd.*,rt.extcode from task_reject_details trd join reject_types rt on rt.code=trd.rejecttype where trd.day>current_date - interval \'30 day\' and trd.isexported is null order by trd.time,trd.id limit 10';
            const resultsTRD = yield sequelize.query(sqlTRD, { type: sequelize.QueryTypes.SELECT });
            let trdUpdate = (item) => __awaiter(void 0, void 0, void 0, function* () {
                try {
                    let _item = item;
                    _item.time = convertFullLocalDateString(new timezoneJS.Date(_item.time), false);
                    if (typeof _item.description === 'string') {
                        _item.description = _item.description.split(',').join('|');
                    }
                    fs.writeFileSync(pathExport + 'iskarta/' + _item.id + '-' + _item.type + '.json', JSON.stringify(_item));
                    const sqlUpdate = 'update task_reject_details set isexported=true where id=:id';
                    yield sequelize.query(sqlUpdate, { replacements: { id: _item.id }, type: sequelize.QueryTypes.UPDATE });
                    return;
                }
                catch (error) {
                    Sentry.captureException(error);
                    setTimeout(() => {
                        process.exit(311);
                    }, 500);
                    return;
                }
            });
            for (const recordTRD of resultsTRD) {
                yield trdUpdate(recordTRD);
            }
            return;
        });
        if (isExportLost) {
            yield eLost();
        }
        if (isExportProd) {
            yield eProd();
        }
        if (isExportReject) {
            yield eReject();
        }
        if (customer === 'sahince' && port === 8800) {
            let sqlUstaBekle = 'SELECT cld.client,cld.record_id,CURRENT_TIMESTAMP-cld.start sure,cld.start ' +
                ' from client_lost_details cld ' +
                ' join (SELECT cast(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL \'1\' day else current_date end as varchar(10)) as date) jr_day,code jr_code ' +
                ' 	from job_rotations 	 ' +
                ' 	where (finish is null or finish<now())	 ' +
                ' 		and ( ' +
                ' 			(endval>beginval and cast(current_time as varchar(5)) between beginval and endval)	 ' +
                ' 			or (endval<beginval and (cast(current_time as varchar(5))>=beginval or cast(current_time as varchar(5))<=endval) )	 ' +
                ' 		))a on (cld.day=a.jr_day and cld.jobrotation=a.jr_code) ' +
                ' where cld.finish is null and cld.day>current_date - interval \'3\' day and CURRENT_TIMESTAMP-cld.start>\'00:10:00\' and cld.losttype=\'USTA BEKLEME\' and cld.type=\'l_c\' ' +
                ' order by cld.start';
            const resultsUstaBekle = yield sequelize.query(sqlUstaBekle, { type: sequelize.QueryTypes.SELECT });
            for (const recordUstaBekle of resultsUstaBekle) {
                if (prevWaitExpert.indexOf(recordUstaBekle.record_id) === -1) {
                    let mailOptions = {
                        from: 'verimotrt@gmail.com',
                        to: 'verimot.pres@sahince.com.tr',
                        subject: 'VerimotRT Usta Bekleme Kaybı',
                        text: recordUstaBekle.client + ' istasyonunda USTA BEKLEME kaybı yaşanmaktadır. Başlangıç : '
                            + recordUstaBekle.start + ' Süre : ' + recordUstaBekle.sure + '.'
                    };
                    mailTransporter.sendMail(mailOptions, (error, info) => {
                    });
                    prevWaitExpert.push(recordUstaBekle.record_id);
                }
            }
            if (prevWaitExpert.length > 0) {
                const sqlUstaBekleBiten = 'SELECT cld.client,cld.record_id ' +
                    ' from client_lost_details cld ' +
                    ' where cld.finish is not null and cld.day>current_date - interval \'3\' day  and cld.losttype=\'USTA BEKLEME\' and cld.type=\'l_c\' and cld.record_id in (:record_id)';
                const resultsUstaBekleBiten = yield sequelize.query(sqlUstaBekleBiten, { replacements: { record_id: prevWaitExpert }, type: sequelize.QueryTypes.SELECT });
                for (const recordUstaBekleBiten of resultsUstaBekleBiten) {
                    let idx = prevWaitExpert.indexOf(recordUstaBekleBiten.record_id);
                    if (idx !== -1) {
                        prevWaitExpert.splice(idx, 1);
                    }
                }
            }
            let sqlForkliftBekle = 'SELECT cld.client,cld.record_id,CURRENT_TIMESTAMP-cld.start sure,cld.start,cld.losttype ' +
                ' from client_lost_details cld ' +
                ' join (SELECT cast(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL \'1\' day else current_date end as varchar(10)) as date) jr_day,code jr_code ' +
                ' 	from job_rotations 	 ' +
                ' 	where (finish is null or finish<now())	 ' +
                ' 		and ( ' +
                ' 			(endval>beginval and cast(current_time as varchar(5)) between beginval and endval)	 ' +
                ' 			or (endval<beginval and (cast(current_time as varchar(5))>=beginval or cast(current_time as varchar(5))<=endval) )	 ' +
                ' 		))a on (cld.day=a.jr_day and cld.jobrotation=a.jr_code) ' +
                ' where cld.finish is null and cld.day>current_date - interval \'3\' day and CURRENT_TIMESTAMP-cld.start>\'00:10:00\' and cld.losttype in (\'FORKLİFT BEKLEME\',\'KALIP BEKLEME (SETUP)\',\'HURDA KASASI BEKLEME\',\'BOŞ KASA BEKLEME\',\'YARIMAMUL BEKLEME\') and cld.type=\'l_c\' ' +
                ' order by cld.start';
            const resultsForkliftBekle = yield sequelize.query(sqlForkliftBekle, { type: sequelize.QueryTypes.SELECT });
            for (const resultForkliftBekle of resultsForkliftBekle) {
                if (prevWaitForklift.indexOf(resultForkliftBekle.record_id) === -1) {
                    let mailOptions = {
                        from: 'verimotrt@gmail.com',
                        to: 'verimot.lojistik@sahince.com.tr',
                        subject: 'VerimotRT Forklift Bekleme Kaybı',
                        text: resultForkliftBekle.client + ' istasyonunda ' + resultForkliftBekle.losttype + ' kaybı yaşanmaktadır. Başlangıç : '
                            + resultForkliftBekle.start + ' Süre : ' + resultForkliftBekle.sure + '.'
                    };
                    mailTransporter.sendMail(mailOptions, (error, info) => {
                    });
                    prevWaitForklift.push(resultForkliftBekle.record_id);
                }
            }
            if (prevWaitForklift.length > 0) {
                const sqlForkliftBekleBiten = 'SELECT cld.client,cld.record_id ' +
                    ' from client_lost_details cld ' +
                    ' where cld.finish is not null and cld.day>current_date - interval \'3\' day and cld.losttype in (\'FORKLİFT BEKLEME\',\'KALIP BEKLEME (SETUP)\',\'HURDA KASASI BEKLEME\',\'BOŞ KASA BEKLEME\',\'YARIMAMUL BEKLEME\') and cld.type=\'l_c\' and cld.record_id in (:record_id)';
                const resultsForkliftBekleBiten = yield sequelize.query(sqlForkliftBekleBiten, { replacements: { record_id: prevWaitForklift }, type: sequelize.QueryTypes.SELECT });
                for (const recordForkliftBekleBiten of resultsForkliftBekleBiten) {
                    let idx = prevWaitForklift.indexOf(recordForkliftBekleBiten.record_id);
                    if (idx !== -1) {
                        prevWaitForklift.splice(idx, 1);
                    }
                }
            }
        }
        if (customer === 'ermetal' && (ip === '172.16.1.143' || ip === '172.16.1.144') && (port === 8800 || port === 8900)) {
            let sqlJR = `
				select * from (
					SELECT id,code,beginval,endval,concat(current_date,' ',cast(current_time as varchar(8))) zaman,CURRENT_TIMESTAMP(0) ts
						,cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)) "day"
						,concat(cast(case when beginval>endval and cast(current_time as varchar(5))<=endval then current_date - INTERVAL '1 day' else current_date end as varchar(10)),' ',beginval,':00')::timestamp jr_start
						,concat(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)),' ',endval,':59')::timestamp jr_end
					from job_rotations
					where (finish is null or finish<now())
						and (
						(endval>beginval and cast(current_time as varchar(5)) between beginval and endval)
						or (endval<beginval and (cast(current_time as varchar(5))>=beginval or cast(current_time as varchar(5))<=endval) )
					)
				)jr
				where ts-jr_start>'00:30:00'
			`;
            const resultsJR = yield sequelize.query(sqlJR, { type: sequelize.QueryTypes.SELECT });
            for (const recordJR of resultsJR) {
                if (lastJrwp !== recordJR.day + ' ' + recordJR.code) {
                    lastJrwp = recordJR.day + ' ' + recordJR.code;
                    const sqlJRWP = 'SELECT * from job_rotation_work_peoples where day=:day and jobrotation=:jobrotation';
                    const resultsJRWP = yield sequelize.query(sqlJRWP, { replacements: { day: recordJR.day, jobrotation: recordJR.code }, type: sequelize.QueryTypes.SELECT });
                    if (resultsJRWP.length === 0) {
                        let _to;
                        let _cc;
                        if (port === 8800) {
                            _to = 'mustafa.kayikci@ermetal.com,oner.utucu@ermetal.com';
                            _cc = 'uzeyir.ari@ermetal.com,serkan.ecer@ermetal.com';
                        }
                        if (port === 8900) {
                            _to = 'hasan.bayer@ermetal.com,mustafa.han@ermetal.com,serkan.ceyhan@ermetal.com,kaynak2@ermetal.com,kaynak3@ermetal.com';
                            _cc = 'erkan.cinko@ermetal.com,serkan.ecer@ermetal.com';
                        }
                        let mailOptions = {
                            from: 'verimotrt@gmail.com',
                            to: _to,
                            cc: _cc,
                            subject: 'VerimotRT Vardiya Yoklama Listesi Uyarısı',
                            text: recordJR.jr_start + ' vardiya başlangıcından sonraki ilk yarım saat içerisinde yapılması gereken VARDİYA YOKLAMA LİSTESİ’nin sisteme girişinin yapılmadığı tespit edilmiştir. İlgili listenin acilen sisteme girişinin yapılması gerekmektedir.'
                        };
                        mailTransporter.sendMail(mailOptions, (error, info) => {
                        });
                    }
                }
            }
        }
        if (x.getMinutes() % 2 === 0 && odbcMerga !== '') {
            if (!isExportMerga) {
                isExportMerga = true;
                yield mergaSyncClients();
                yield mergaSyncOperations();
                yield mergaSyncMaterialRoutes();
                yield mergaSyncMaterialOperations();
                yield mergaSyncMaterialOperationsIO();
                yield mergaSyncActualProductions();
                yield mergaSyncTaskLists();
                yield mergaSyncReworks();
                yield run();
            }
        }
        else {
            isExportMerga = false;
        }
        process.nextTick(() => {
            setTimeout(() => {
                return controlExportData();
            }, 10000);
        });
    }
    catch (error) {
        Sentry.captureException(error);
        setTimeout(() => {
            process.exit(315);
        }, 500);
        return;
    }
});
let run = () => __awaiter(void 0, void 0, void 0, function* () {
    while (messages.length > 0) {
        const message = messages.shift();
        yield proc(message);
        yield delay(10);
    }
    return;
});
let proc = (msg) => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise((resolve, reject) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            if (msg.tasks.length > 0) {
                for (const task of msg.tasks) {
                    yield mergaSyncTaskTrans(msg, task);
                    yield delay(10);
                }
            }
            yield delay(50);
            resolve(true);
            return;
        }
        catch (error) {
            Sentry.captureException(error);
            reject(error);
        }
    })).catch((err) => {
        Sentry.captureException(err);
    });
});
let mergaSyncClients = () => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        let rep = null;
        const objLastUpdateDel = yield getLastUpdate('merga_sync_clients_audit');
        sql = 'SELECT * from clients_audit where deleted_at is not null and revtype=\'DEL\' and deleted_at>=:dat order by deleted_at';
        rep = { dat: objLastUpdateDel.lastUpdate };
        const resultsDeleted = yield sequelize.query(sql, { replacements: rep, type: sequelize.QueryTypes.SELECT });
        for (const recordDel of resultsDeleted) {
            yield mergaSyncClientDel(recordDel);
            objLastUpdateDel.lastUpdate = convertFullLocalDateString(recordDel.deleted_at, false);
        }
        yield syncUpdateRecord(objLastUpdateDel);
        const objLastUpdate = yield getLastUpdate('merga_sync_clients');
        sql = 'select * from clients where updated_at>=:lut and coalesce(code,\'\')<>\'\' and uuid is not null order by updated_at limit 100';
        rep = { lut: objLastUpdate.lastUpdate };
        const resultsTable = yield sequelize.query(sql, { replacements: rep, type: sequelize.QueryTypes.SELECT });
        for (const recordTable of resultsTable) {
            yield mergaSyncClient(recordTable);
            objLastUpdate.lastUpdate = convertFullLocalDateString(recordTable.updated_at, false);
        }
        yield syncUpdateRecord(objLastUpdate);
        return;
    }
    catch (error) {
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncClientDel = (row) => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        connMerga = yield odbc.connect(odbcMerga);
        sql = 'update Int_Core_Machine set Status=0 where Code=\'' + row.code + '\' and CompanyUnitCode=\'' + CompanyUnitCode + '\' and Status=1';
        yield connMerga.query(sql);
        yield connMerga.close();
        connMerga = null;
        return;
    }
    catch (error) {
        yield connMerga.close();
        connMerga = null;
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncClient = (row) => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        connMerga = yield odbc.connect(odbcMerga);
        const strWhere = ' where Code=\'' + row.code + '\' and CompanyUnitCode=\'' + CompanyUnitCode + '\' ';
        sql = 'select * from Int_Core_Machine ' + strWhere;
        const result = yield connMerga.query(sql);
        const updatedAt = convertFullLocalDateString(row.updated_at, false);
        if (result) {
            if (result.length !== 0) {
                sql = 'update Int_Core_Machine set CompanyUnitCode=\'' + CompanyUnitCode + '\',EditDate=\''
                    + updatedAt + '\',Status=1 ' + strWhere;
            }
            else {
                sql = 'insert into Int_Core_Machine (Code,Status,CompanyUnitCode,RegisterDate,EditDate) values(\''
                    + row.code + '\',1,\'' + CompanyUnitCode + '\',\'' + updatedAt + '\',\''
                    + updatedAt + '\')';
            }
            yield connMerga.query(sql);
        }
        yield connMerga.close();
        connMerga = null;
        return;
    }
    catch (error) {
        yield connMerga.close();
        connMerga = null;
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncOperations = () => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        let rep = null;
        const objLastUpdateDel = yield getLastUpdate('merga_sync_operations_audit');
        sql = 'SELECT * from product_trees_audit where materialtype=\'O\' and deleted_at is not null and revtype=\'DEL\' and deleted_at>=:dat order by deleted_at';
        rep = { dat: objLastUpdateDel.lastUpdate };
        const resultsDeleted = yield sequelize.query(sql, { replacements: rep, type: sequelize.QueryTypes.SELECT });
        for (const recordDel of resultsDeleted) {
            yield mergaSyncOperationDel(recordDel);
            objLastUpdateDel.lastUpdate = convertFullLocalDateString(recordDel.deleted_at, false);
        }
        yield syncUpdateRecord(objLastUpdateDel);
        const objLastUpdate = yield getLastUpdate('merga_sync_operations');
        sql = 'select name,description,start,finish,updated_at,case finish when null then 0 else 1 end status from product_trees where materialtype=\'O\' and updated_at>=:lut order by updated_at limit 100';
        rep = { lut: objLastUpdate.lastUpdate };
        const resultsTable = yield sequelize.query(sql, { replacements: rep, type: sequelize.QueryTypes.SELECT });
        for (const recordTable of resultsTable) {
            yield mergaSyncOperation(recordTable);
            objLastUpdate.lastUpdate = convertFullLocalDateString(recordTable.updated_at, false);
        }
        yield syncUpdateRecord(objLastUpdate);
        return;
    }
    catch (error) {
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncOperationDel = (row) => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        connMerga = yield odbc.connect(odbcMerga);
        sql = 'update Int_Core_Operation set Status=0 where Code=\'' + row.code + '\' and Status=1';
        yield connMerga.query(sql);
        yield connMerga.close();
        connMerga = null;
        return;
    }
    catch (error) {
        yield connMerga.close();
        connMerga = null;
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncOperation = (row) => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        connMerga = yield odbc.connect(odbcMerga);
        const strWhere = ' where Code=\'' + row.name + '\' ';
        sql = 'select * from Int_Core_Operation ' + strWhere;
        const result = yield connMerga.query(sql);
        const cDescription = (row.description !== '' ? row.description : row.name);
        const updatedAt = convertFullLocalDateString(row.updated_at, false);
        if (result) {
            if (result.length === 0) {
                sql = 'insert into Int_Core_Operation (Code,Status,Name,RegisterDate,EditDate) values(\'' + row.name + '\','
                    + row.status + ',\'\',\'' + updatedAt
                    + '\',\'' + updatedAt + '\')';
            }
            yield connMerga.query(sql);
        }
        yield connMerga.close();
        connMerga = null;
        return;
    }
    catch (error) {
        yield connMerga.close();
        connMerga = null;
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncMaterialRoutes = () => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        let rep = null;
        const objLastUpdateDel = yield getLastUpdate('merga_sync_materialroutes_audit');
        sql = 'SELECT * from product_trees_audit where materialtype in (\'M\') and deleted_at is not null and revtype=\'DEL\' and deleted_at>=:dat order by deleted_at';
        rep = { dat: objLastUpdateDel.lastUpdate };
        const resultsDeleted = yield sequelize.query(sql, { replacements: rep, type: sequelize.QueryTypes.SELECT });
        for (const recordDel of resultsDeleted) {
            yield mergaSyncMaterialRouteDel(recordDel);
            objLastUpdateDel.lastUpdate = convertFullLocalDateString(recordDel.deleted_at, false);
        }
        yield syncUpdateRecord(objLastUpdateDel);
        const objLastUpdate = yield getLastUpdate('merga_sync_materialroutes');
        sql = 'select code,record_id,updated_at,case finish when null then 0 else 1 end status from product_trees where materialtype in (\'M\') and updated_at>=:lut order by updated_at limit 100';
        rep = { lut: objLastUpdate.lastUpdate };
        const resultsTable = yield sequelize.query(sql, { replacements: rep, type: sequelize.QueryTypes.SELECT });
        for (const recordTable of resultsTable) {
            yield mergaSyncMaterialRoute(recordTable);
            objLastUpdate.lastUpdate = convertFullLocalDateString(recordTable.updated_at, false);
        }
        yield syncUpdateRecord(objLastUpdate);
        return;
    }
    catch (error) {
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncMaterialRouteDel = (row) => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        connMerga = yield odbc.connect(odbcMerga);
        sql = 'update Int_Core_MaterialRoute set Status=0 where RecordNo=\'' + row.record_id + '\' ';
        yield connMerga.query(sql);
        sql = 'delete from Int_Core_MaterialOperation where RouteNo=\'' + row.record_id + '\' ';
        yield connMerga.query(sql);
        sql = 'delete from Int_Core_OperationIO where RouteNo=\'' + row.record_id + '\' ';
        yield connMerga.query(sql);
        yield connMerga.close();
        connMerga = null;
        return;
    }
    catch (error) {
        yield connMerga.close();
        connMerga = null;
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncMaterialRoute = (row) => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        connMerga = yield odbc.connect(odbcMerga);
        const strWhere = ' where RecordNo=\'' + row.record_id + '\' ';
        sql = 'select * from Int_Core_MaterialRoute ' + strWhere;
        const result = yield connMerga.query(sql);
        const updatedAt = convertFullLocalDateString(row.updated_at, false);
        if (result) {
            if (result.length !== 0) {
                sql = 'update Int_Core_MaterialRoute set RevisionNo=RevisionNo+1,EditDate=\''
                    + updatedAt + '\',Status=' + row.status + ' ' + strWhere;
            }
            else {
                sql = 'insert into Int_Core_MaterialRoute (RecordNo,Status,RevisionNo,MaterialCode,Amount,RegisterDate,EditDate) values(\''
                    + row.record_id + '\',' + row.status + ',1,\'' + row.code + '\',1,\''
                    + updatedAt + '\',\'' + updatedAt + '\')';
            }
            yield connMerga.query(sql);
        }
        yield connMerga.close();
        connMerga = null;
        return;
    }
    catch (error) {
        yield connMerga.close();
        connMerga = null;
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncMaterialOperations = () => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        let rep = null;
        const objLastUpdate = yield getLastUpdate('merga_sync_materialoperations');
        sql = 'SELECT pt_m.record_id,coalesce(pt.oporder,1) oporder,pt.name,pt.updated_at ' +
            '		,(select name from product_trees where materialtype=\'O\' and code=pt_m.code order by oporder desc,name desc limit 1)lastop ' +
            '	from product_trees pt ' +
            '	join (SELECT code,record_id,updated_at,case finish when null then 0 else 1 end status ' +
            '	from product_trees  ' +
            '	where materialtype=\'M\'  ' +
            ')pt_m on pt.parent=pt_m.code ' +
            'where pt.materialtype=\'O\' and (pt_m.updated_at>=:lut or pt.updated_at>=:lut) ' +
            'order by pt.updated_at ' +
            'limit 100';
        rep = { lut: objLastUpdate.lastUpdate };
        const resultsTable = yield sequelize.query(sql, { replacements: rep, type: sequelize.QueryTypes.SELECT });
        for (const recordTable of resultsTable) {
            yield mergaSyncMaterialOperation(recordTable);
            objLastUpdate.lastUpdate = convertFullLocalDateString(recordTable.updated_at, false);
        }
        yield syncUpdateRecord(objLastUpdate);
        return;
    }
    catch (error) {
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncMaterialOperation = (row) => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        connMerga = yield odbc.connect(odbcMerga);
        const strWhere = ' where RouteNo=\'' + row.record_id + '\' and OperationCode=\'' + row.name + '\' ';
        sql = 'select * from Int_Core_MaterialOperation ' + strWhere;
        const result = yield connMerga.query(sql);
        const updatedAt = convertFullLocalDateString(row.updated_at, false);
        if (result) {
            if (result.length !== 0) {
                sql = 'update Int_Core_MaterialOperation set SortNumber=' + row.oporder + ',EditDate=\''
                    + updatedAt + '\' ' + strWhere;
            }
            else {
                sql = 'insert into Int_Core_MaterialOperation (RouteNo,SortNumber,OperationCode,RegisterDate,EditDate) values(\'' +
                    row.record_id + '\',' + row.oporder + ',\'' + row.name + '\',\'' + updatedAt + '\',\'' + updatedAt + '\')';
            }
            yield connMerga.query(sql);
        }
        yield connMerga.close();
        connMerga = null;
        return;
    }
    catch (error) {
        yield connMerga.close();
        connMerga = null;
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncMaterialOperationsIO = () => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        let rep = null;
        const objLastUpdate = yield getLastUpdate('merga_sync_materialoperationsIO');
        sql = 'SELECT pt_m.record_id,pt_m.code mcode,pt.name,101 iotype,pt.stockcode,pt.quantity,pt.updated_at ' +
            '	from product_trees pt ' +
            '	join (SELECT code,record_id,updated_at,case finish when null then 0 else 1 end status ' +
            '	from product_trees  ' +
            '	where materialtype=\'M\'  ' +
            ')pt_m on pt.parent=pt_m.code ' +
            'where pt.materialtype=\'H\' and (pt_m.updated_at>=:lut or pt.updated_at>=:lut) ' +
            'order by pt.updated_at ' +
            'limit 100';
        rep = { lut: objLastUpdate.lastUpdate };
        const resultsTable = yield sequelize.query(sql, { replacements: rep, type: sequelize.QueryTypes.SELECT });
        for (const recordTable of resultsTable) {
            yield mergaSyncMaterialOperationIO(recordTable);
            objLastUpdate.lastUpdate = convertFullLocalDateString(recordTable.updated_at, false);
        }
        yield syncUpdateRecord(objLastUpdate);
        return;
    }
    catch (error) {
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncMaterialOperationIO = (row) => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        connMerga = yield odbc.connect(odbcMerga);
        const strWhere = ' where RouteNo=\'' + row.record_id + '\' and RootMaterialCode=\'' + row.mcode + '\' and OperationCode=\'' + row.name + '\' and IOType=101 and MaterialCode=\'' + row.stockcode + '\' ';
        sql = 'select * from Int_Core_OperationIO ' + strWhere;
        const result = yield connMerga.query(sql);
        const updatedAt = convertFullLocalDateString(row.updated_at, false);
        if (result) {
            if (result.length !== 0) {
                sql = 'update Int_Core_OperationIO set Amount=' + row.quantity + ',EditDate=\''
                    + updatedAt + '\' ' + strWhere;
            }
            else {
                sql = 'insert into Int_Core_OperationIO (RouteNo,RootMaterialCode,OperationCode,IOType,MaterialCode,Amount,RegisterDate,EditDate) values(\'' + row.record_id + '\',\'' + row.mcode + '\',\'' + row.name + '\',101,\'' + row.stockcode + '\',' + row.quantity + ',\'' + updatedAt + '\',\'' + updatedAt + '\')';
            }
            yield connMerga.query(sql);
        }
        yield connMerga.close();
        connMerga = null;
        return;
    }
    catch (error) {
        yield connMerga.close();
        connMerga = null;
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncActualProductions = () => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        let rep = null;
        const objLastUpdate = yield getLastUpdate('merga_sync_actual_productions');
        sql = 'SELECT cast(CURRENT_TIMESTAMP(0) as varchar(19))zaman,opname,replace(client,\'.\',\'\') client,productcount,productdonecount,erprefnumber,cast(CURRENT_TIMESTAMP(0) as varchar(19))updated_at from task_lists where COALESCE(finish,CURRENT_TIMESTAMP(0))>CURRENT_TIMESTAMP(0)-interval \'300 seconds\' and productdonecount>0 ';
        rep = {};
        const resultsTable = yield sequelize.query(sql, { replacements: rep, type: sequelize.QueryTypes.SELECT });
        for (const recordTable of resultsTable) {
            yield mergaSyncActualProduction(recordTable);
            objLastUpdate.lastUpdate = convertFullLocalDateString(recordTable.zaman, false);
        }
        yield syncUpdateRecord(objLastUpdate);
        return;
    }
    catch (error) {
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncActualProduction = (row) => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        connMerga = yield odbc.connect(odbcMerga);
        const strWhere = ' where MaterialCode=\'' + row.opname + '\' and WorkOrderNo=\'' + row.erprefnumber + '\' and REPLACE(MachineCode,  \'.\', \'\')=\'' + row.client + '\' ';
        sql = 'select * from Int_Core_ActualProduction ' + strWhere;
        const result = yield connMerga.query(sql);
        const updatedAt = convertFullLocalDateString(row.updated_at, false);
        if (result) {
            if (result.length !== 0) {
                sql = 'update Int_Core_ActualProduction set Amount=' + row.productdonecount + ',EditDate=\''
                    + updatedAt + '\',ActualDate=\''
                    + updatedAt + '\' ' + strWhere;
            }
            else {
                sql = 'insert into Int_Core_ActualProduction (MaterialCode,ActualDate,Amount,CompanyUnitCode,WorkOrderNo,MachineCode,RegisterDate,EditDate) values(\'' + row.opname + '\',\'' + updatedAt + '\',' + row.productdonecount + ',\'\',\'' + row.erprefnumber + '\',\'' + row.client + '\',\'' + updatedAt + '\',\'' + updatedAt + '\')';
            }
            yield connMerga.query(sql);
        }
        yield connMerga.close();
        connMerga = null;
        return;
    }
    catch (error) {
        yield connMerga.close();
        connMerga = null;
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncTaskLists = () => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        let rep = null;
        const objLastUpdateDel = yield getLastUpdate('merga_sync_task_lists_audit');
        sql = 'SELECT erprefnumber,opname,record_id,deleted_at from task_lists_audit where deleted_at is not null and revtype=\'DEL\' and deleted_at>=:dat order by deleted_at';
        rep = { dat: objLastUpdateDel.lastUpdate };
        const resultsDeleted = yield sequelize.query(sql, { replacements: rep, type: sequelize.QueryTypes.SELECT });
        for (const recordDel of resultsDeleted) {
            yield mergaSyncTaskListDel(recordDel);
            objLastUpdateDel.lastUpdate = convertFullLocalDateString(recordDel.deleted_at, false);
        }
        yield syncUpdateRecord(objLastUpdateDel);
        const objLastUpdate = yield getLastUpdate('merga_sync_task_lists');
        sql = 'SELECT cast(CURRENT_TIMESTAMP(0) as varchar(19))zaman,erprefnumber,opcode,opname,productcount,productdonecount,replace(client,\'.\',\'\') client,deadline,plannedstart,plannedfinish,cast(extract(epoch from (plannedfinish::timestamp-plannedstart::timestamp)) as integer)sure,start,finish,status,updated_at,case when (productcount-productdonecount)*tpp/60<15 then 1 else 0 end bitiyor from task_lists where finish is null and plannedstart>CURRENT_TIMESTAMP(0)-interval \'10 days\' and updated_at>=:dat ';
        rep = { dat: objLastUpdate.lastUpdate };
        const resultsTable = yield sequelize.query(sql, { replacements: rep, type: sequelize.QueryTypes.SELECT });
        for (const recordTable of resultsTable) {
            yield mergaSyncTaskList(recordTable);
            objLastUpdate.lastUpdate = convertFullLocalDateString(recordTable.zaman, false);
        }
        yield syncUpdateRecord(objLastUpdate);
        return;
    }
    catch (error) {
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncTaskListDel = (row) => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        connMerga = yield odbc.connect(odbcMerga);
        sql = 'delete from Int_PC_WorkOrder where RecordNo=\'' + row.erprefnumber + '-' + row.opname + '\' and OperationCode=\'' + row.opname + '\'';
        yield connMerga.query(sql);
        yield connMerga.close();
        connMerga = null;
        return;
    }
    catch (error) {
        yield connMerga.close();
        connMerga = null;
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncTaskList = (row) => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        connMerga = yield odbc.connect(odbcMerga);
        const strWhere = ' where RecordNo=\'' + row.erprefnumber + '-' + row.opname + '\' and MaterialCode=\'' + row.opcode + '\' and OperationCode=\'' + row.opname + '\' and CompanyUnitCode=\'' + CompanyUnitCode + '\' ';
        sql = 'select * from Int_PC_WorkOrder ' + strWhere;
        const result = yield connMerga.query(sql);
        const updatedAt = convertFullLocalDateString(row.updated_at, false);
        if (result) {
            if (result.length !== 0) {
                sql = 'update Int_PC_WorkOrder set MachineCode=\'' + row.client + '\',Amount=' + row.productcount
                    + ',DueDate=\'' + convertFullLocalDateString(row.deadline, false) + '\',PlannedStartDate=\''
                    + convertFullLocalDateString(row.plannedstart, false) + '\',PlannedEndDate=\''
                    + convertFullLocalDateString(row.plannedfinish, false) + '\',PlannedTime=' + row.sure
                    + ',PlannedEmployess=0,EditDate=\'' + updatedAt + '\'' + strWhere;
            }
            else {
                sql = 'insert into Int_PC_WorkOrder (RecordNo,MaterialCode,OperationCode,MachineCode,Amount,CompanyUnitCode,DueDate,PlannedStartDate,PlannedEndDate,PlannedTime,PlannedEmployess,RegisterDate,EditDate) values(\''
                    + row.erprefnumber + '-' + row.opname + '\',\'' + row.opcode + '\',\'' + row.opname + '\',\'' + row.client + '\',' + row.productcount + ',\''
                    + CompanyUnitCode + '\',\'' + convertFullLocalDateString(row.deadline, false) + '\',\''
                    + convertFullLocalDateString(row.plannedstart, false) + '\',\'' + convertFullLocalDateString(row.plannedfinish, false) + '\',' + row.sure + ',0,\''
                    + updatedAt + '\',\''
                    + updatedAt + '\')';
            }
            yield connMerga.query(sql);
        }
        sql = 'select * from Int_PC_WorkOrderTrans where WorkOrderNo=\'' + row.erprefnumber + '-' + row.opname + '\' and OperationCode=\''
            + row.opname + '\' and CompanyUnitCode=\'' + CompanyUnitCode + '\' ';
        const resultStatus = yield connMerga.query(sql);
        if (resultStatus && resultStatus.length !== 0) {
            sql = 'update Int_PC_WorkOrderTrans set MachineCode=\'' + row.client + '\' where WorkOrderNo=\'' + row.erprefnumber + '-' + row.opname + '\' and OperationCode=\''
                + row.opname + '\' and CompanyUnitCode=\'' + CompanyUnitCode + '\' ';
        }
        else {
            sql = 'insert into Int_PC_WorkOrderTrans (WorkOrderNo,OperationCode,MachineCode,Amount,TransTypeCode,CompanyUnitCode,TransDate,RegisterDate,EditDate) values(\''
                + row.erprefnumber + '-' + row.opname + '\',\'' + row.opname + '\',\'' + row.client + '\',0,0,\''
                + CompanyUnitCode + '\',\'' + updatedAt + '\',\'' + updatedAt + '\',\'' + updatedAt + '\')';
        }
        yield connMerga.query(sql);
        if (row.bitiyor === 1) {
            sql = 'select * from Int_PC_WorkOrderTrans where WorkOrderNo=\'' + row.erprefnumber + '-' + row.opname + '\' and OperationCode=\''
                + row.opname + '\' and CompanyUnitCode=\'' + CompanyUnitCode + '\' and REPLACE(MachineCode,  \'.\', \'\')=\'' + row.client + '\' and TransTypeCode=9 ';
            const resultStatusProdFinish = yield connMerga.query(sql);
            if (resultStatusProdFinish && resultStatusProdFinish.length === 0) {
                sql = 'insert into Int_PC_WorkOrderTrans (WorkOrderNo,OperationCode,MachineCode,Amount,TransTypeCode,CompanyUnitCode,TransDate,RegisterDate,EditDate) values(\''
                    + row.erprefnumber + '-' + row.opname + '\',\'' + row.opname + '\',\'' + row.client + '\',' + row.productdonecount + ',9,\''
                    + CompanyUnitCode + '\',\'' + updatedAt + '\',\'' + updatedAt + '\',\'' + updatedAt + '\')';
                yield connMerga.query(sql);
            }
        }
        yield connMerga.close();
        connMerga = null;
        return;
    }
    catch (error) {
        yield connMerga.close();
        connMerga = null;
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncReworks = () => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        let rep = null;
        const objLastUpdateDel = yield getLastUpdate('merga_sync_reworks_audit');
        sql = 'SELECT record_id erprefnumber,opname,record_id,deleted_at from reworks_audit where opname is not null and deleted_at is not null and revtype=\'DEL\' and deleted_at>=:dat order by deleted_at';
        rep = { dat: objLastUpdateDel.lastUpdate };
        const resultsDeleted = yield sequelize.query(sql, { replacements: rep, type: sequelize.QueryTypes.SELECT });
        for (const recordDel of resultsDeleted) {
            yield mergaSyncReworkDel(recordDel);
            objLastUpdateDel.lastUpdate = convertFullLocalDateString(recordDel.deleted_at, false);
        }
        yield syncUpdateRecord(objLastUpdateDel);
        const objLastUpdate = yield getLastUpdate('merga_sync_reworks');
        sql = 'SELECT cast(CURRENT_TIMESTAMP(0) as varchar(19))zaman,record_id erprefnumber,opcode,opname,0 productcount,0 productdonecount,replace(client,\'.\',\'\') client,CURRENT_TIMESTAMP(0) deadline,CURRENT_TIMESTAMP(0) plannedstart,CURRENT_TIMESTAMP(0) plannedfinish,0 sure,start,finish,updated_at,0 bitiyor,record_id from reworks where opname is not null and islisted=true and coalesce(finish,CURRENT_TIMESTAMP(0))>=CURRENT_TIMESTAMP(0) ';
        rep = {};
        const resultsTable = yield sequelize.query(sql, { replacements: rep, type: sequelize.QueryTypes.SELECT });
        for (const recordTable of resultsTable) {
            yield mergaSyncRework(recordTable);
            objLastUpdate.lastUpdate = convertFullLocalDateString(recordTable.zaman, false);
        }
        yield syncUpdateRecord(objLastUpdate);
        return;
    }
    catch (error) {
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncReworkDel = (row) => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        connMerga = yield odbc.connect(odbcMerga);
        sql = 'delete from Int_PC_WorkOrder where RecordNo=\'' + row.erprefnumber + '-' + row.opname + '\' and OperationCode=\'' + row.opname + '\'';
        yield connMerga.query(sql);
        yield connMerga.close();
        connMerga = null;
        return;
    }
    catch (error) {
        yield connMerga.close();
        connMerga = null;
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncRework = (row) => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        connMerga = yield odbc.connect(odbcMerga);
        const strWhere = ' where RecordNo=\'' + row.erprefnumber + '-' + row.opname + '\' and MaterialCode=\'' + row.opcode + '\' and OperationCode=\'' + row.opname + '\' and CompanyUnitCode=\'' + CompanyUnitCode + '\' ';
        sql = 'select * from Int_PC_WorkOrder ' + strWhere;
        const result = yield connMerga.query(sql);
        const updatedAt = convertFullLocalDateString(row.updated_at, false);
        if (result) {
            if (result.length !== 0) {
                sql = 'update Int_PC_WorkOrder set MachineCode=\'' + row.client + '\',Amount=' + row.productcount
                    + ',DueDate=\'' + convertFullLocalDateString(row.deadline, false) + '\',PlannedStartDate=\''
                    + convertFullLocalDateString(row.plannedstart, false) + '\',PlannedEndDate=\''
                    + convertFullLocalDateString(row.plannedfinish, false) + '\',PlannedTime=' + row.sure
                    + ',PlannedEmployess=0,EditDate=\'' + updatedAt + '\'' + strWhere;
            }
            else {
                sql = 'insert into Int_PC_WorkOrder (RecordNo,MaterialCode,OperationCode,MachineCode,Amount,CompanyUnitCode,DueDate,PlannedStartDate,PlannedEndDate,PlannedTime,PlannedEmployess,RegisterDate,EditDate) values(\''
                    + row.erprefnumber + '-' + row.opname + '\',\'' + row.opcode + '\',\'' + row.opname + '\',\'' + row.client + '\',' + row.productcount + ',\''
                    + CompanyUnitCode + '\',\'' + convertFullLocalDateString(row.deadline, false) + '\',\''
                    + convertFullLocalDateString(row.plannedstart, false) + '\',\'' + convertFullLocalDateString(row.plannedfinish, false) + '\',' + row.sure + ',0,\''
                    + updatedAt + '\',\''
                    + updatedAt + '\')';
            }
            yield connMerga.query(sql);
        }
        sql = 'select * from Int_PC_WorkOrderTrans where WorkOrderNo=\'' + row.erprefnumber + '-' + row.opname + '\' and OperationCode=\''
            + row.opname + '\' and CompanyUnitCode=\'' + CompanyUnitCode + '\' ';
        const resultStatus = yield connMerga.query(sql);
        if (resultStatus && resultStatus.length !== 0) {
            sql = 'update Int_PC_WorkOrderTrans set MachineCode=\'' + row.client + '\' where WorkOrderNo=\'' + row.erprefnumber + '-' + row.opname + '\' and OperationCode=\''
                + row.opname + '\' and CompanyUnitCode=\'' + CompanyUnitCode + '\' ';
        }
        else {
            sql = 'insert into Int_PC_WorkOrderTrans (WorkOrderNo,OperationCode,MachineCode,Amount,TransTypeCode,CompanyUnitCode,TransDate,RegisterDate,EditDate) values(\''
                + row.erprefnumber + '-' + row.opname + '\',\'' + row.opname + '\',\'' + row.client + '\',0,0,\''
                + CompanyUnitCode + '\',\'' + updatedAt + '\',\'' + updatedAt + '\',\'' + updatedAt + '\')';
        }
        yield connMerga.query(sql);
        yield connMerga.close();
        connMerga = null;
        return;
    }
    catch (error) {
        yield connMerga.close();
        connMerga = null;
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let mergaSyncTaskTrans = (obj, task) => __awaiter(void 0, void 0, void 0, function* () {
    let sql = '';
    try {
        connMergaTrans = yield odbc.connect(odbcMerga);
        const updatedAt = convertFullLocalDateString(obj.time, false);
        obj.client = obj.client.split('.').join('');
        let ttCode = 0;
        switch (obj.state) {
            case 'Bekliyor': {
                ttCode = 11;
                break;
            }
            case 'Devam Ediyor': {
                ttCode = 1;
                break;
            }
            case 'Duruş Başladı': {
                ttCode = 3;
                break;
            }
            case 'Duruş Bitti': {
                ttCode = 4;
                break;
            }
            case 'Setup Yapıldı': {
                ttCode = 8;
                break;
            }
            case 'Tamamlandı': {
                ttCode = 2;
                break;
            }
            case 'Yeniden İşlem Başladı': {
                ttCode = 5;
                break;
            }
            case 'Yeniden İşlem Bitti': {
                ttCode = 6;
                break;
            }
            case 'Üretiyor': {
                ttCode = 10;
                break;
            }
        }
        sql = 'insert into Int_PC_WorkOrderTrans (WorkOrderNo,OperationCode,MachineCode,Amount,TransTypeCode,CompanyUnitCode,Explanation,TransDate,RegisterDate,EditDate) values(\''
            + task.erprefnumber + '-' + task.opname + '\',\'' + task.opname + '\',\'' + obj.client + '\',' + (ttCode === 10 ? task.production : task.productdonecount) + ',' + ttCode + ',\''
            + CompanyUnitCode + '\',\'' + obj.losttype + '\',\'' + updatedAt + '\',\'' + updatedAt + '\',\'' + updatedAt + '\')';
        yield connMergaTrans.query(sql);
        if (obj.state === 'Yeniden İşlem Bitti') {
            sql = 'insert into Int_PC_WorkOrderTrans (WorkOrderNo,OperationCode,MachineCode,Amount,TransTypeCode,CompanyUnitCode,TransDate,RegisterDate,EditDate) values(\''
                + task.erprefnumber + '-' + task.opname + '\',\'' + task.opname + '\',\'' + obj.client + '\',' + task.productdonecount + ',10,\''
                + CompanyUnitCode + '\',\'' + updatedAt + '\',\'' + updatedAt + '\',\'' + updatedAt + '\')';
            yield connMergaTrans.query(sql);
        }
        yield connMergaTrans.close();
        connMergaTrans = null;
        return;
    }
    catch (error) {
        yield connMergaTrans.close();
        connMergaTrans = null;
        Sentry.addBreadcrumb({
            category: 'merga_sync',
            message: 'sql:' + sql,
            level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
    }
});
let getLastUpdate = (tableName) => __awaiter(void 0, void 0, void 0, function* () {
    let lastUpdate = (tableName.indexOf('_audit') === -1 ? '2000-01-01' : '2021-11-29');
    let isFirstSync = false;
    const sql = 'select * from _sync_updatelogs where "tableName"=:table';
    const rep = { table: tableName };
    const resultsSUL = yield sequelize.query(sql, { replacements: rep, type: sequelize.QueryTypes.SELECT });
    if (resultsSUL.length !== 0) {
        lastUpdate = resultsSUL[0].strlastupdate;
    }
    else {
        isFirstSync = true;
    }
    return { isFirstSync, lastUpdate, tableName };
});
let syncUpdateRecord = (objLastUpdate) => __awaiter(void 0, void 0, void 0, function* () {
    let rep = { tableName: objLastUpdate.tableName, strlastupdate: objLastUpdate.lastUpdate };
    if (objLastUpdate.isFirstSync) {
        let sql = 'insert into _sync_updatelogs ("tableName","strlastupdate","createdAt","updatedAt") VALUES (:tableName,:strlastupdate,CURRENT_TIMESTAMP(0),CURRENT_TIMESTAMP(0)) ';
        return sequelize.query(sql, { replacements: rep, type: sequelize.QueryTypes.INSERT });
    }
    else {
        let sql = 'update _sync_updatelogs set strlastupdate=:strlastupdate where "tableName"=:tableName';
        return sequelize.query(sql, { replacements: rep, type: sequelize.QueryTypes.UPDATE });
    }
});
let convertFullLocalDateString = (_time, _ms) => {
    const dt = new timezoneJS.Date(_time);
    return dt.toString('yyyy-MM-dd HH:mm:ss' + (_ms === true ? '.SSS' : ''));
};
let getCurrentTime = () => {
    const dt = new timezoneJS.Date();
    return dt.toString('HH:mm:ss');
};
const delay = (ms) => new Promise(res => setTimeout(res, ms));
