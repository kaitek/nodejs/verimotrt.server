const processModule = require('process');
const Sequelize = require('sequelize');
const timezoneJS = require('timezone-js');
const Promise = require('promise');
const Sentry = require('@sentry/node');
Sentry.init({
	dsn: 'https://4e61d0b3664d41c9a849f8edbb2b2255@sentry.kaitek.com.tr/3'
});
let sequelize;
let _pid=false;
let oee_clients=false;
processModule.on('message', function (obj) {
	let event = obj.event;
	let param = obj.param;
	switch (event) {
	case 'init_oee_calculate':{
		_pid=obj.pid;
		db_connect(param);
		break;
	}
	default:{
		//console.log(obj);
		break;
	}
	}
});
processModule.on('uncaughtException', function(err){
	Sentry.captureException(err);
	setTimeout(() => {
		process.exit(100);
	}, 500);
	return;
});
let db_connect = (obj) => {
	sequelize = new Sequelize(obj.database, obj.username, obj.password, {
		host: obj.host,
		dialect: 'postgres',
		logging: false,
		pool: {
			max: 10,
			min: 0,
			acquire: 100000,
			idle: 10000
		},
		timezone:obj.timezone
	});
	return sequelize
		.authenticate()
		.then(() => {
			processModule.send({event:'cp_oee_calculate_inited',ip:obj.ip,pid:_pid});
			return run();
		})
		.catch(err => {
			Sentry.captureException(err);
			setTimeout(() => {
				process.exit(101);
			}, 500);
			return;
		});
};
let run=()=>{
	oee_clients=[];
	let _total=0;
	let sql_jr='SELECT id,code,beginval,endval '+
	'	,cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL \'1 day\' else current_date end as varchar(10)) "day" '+
	'	,concat(cast(case when beginval>endval and cast(current_time as varchar(5))<=endval then current_date - INTERVAL \'1 day\' else current_date end as varchar(10)),\' \',beginval,\':00\')::timestamp jr_start '+
	'	,concat(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL \'1 day\' else current_date end as varchar(10)),\' \',endval,\':59\')::timestamp jr_end '+
	' from job_rotations 	'+
	' where (finish is null or finish<now()) '+
	'	and (	'+
	'	(endval>beginval and cast(current_time as varchar(5)) between beginval and endval)	'+
	'	or (endval<beginval and (cast(current_time as varchar(5))>=beginval or cast(current_time as varchar(5))<=endval) )	'+
	' )';
	return sequelize.query(sql_jr,{ type: sequelize.QueryTypes.SELECT}).then(function(jr_results){
		if(jr_results&&jr_results.length>0){
			let _day=jr_results[0]['day'];
			let _jr=jr_results[0]['code'];
			let time=convertFullLocalDateString(new timezoneJS.Date());
			let _jr_start=convertFullLocalDateString(new timezoneJS.Date(jr_results[0]['jr_start']));
			_total=Math.round((new timezoneJS.Date(time)-new timezoneJS.Date(_jr_start))/1000);
			let sql_oee_lost='select b.client,b.lostgroup,sum(b.suresn)suresn from (	'+
				'	select a.client,a.day,a.jobrotation,a.losttype,a.start,a.finish,round(cast(a.suresn/60 as numeric),0)suredk,round(cast(a.suresn/3600 as numeric),4) suresaat,a.suresn	'+
				'		,case when ((lgd.lostgroup is null and (a.losttype=\'GÖREVDE KİMSE YOK\' or a.losttype=\'İŞ YOK\')) or a.task like \'%DENEME%\') then \'OEE-uretimekapali\' else lgd.lostgroup end lostgroup '+
				'		,case when ((lgd.lostgroup is null and (a.losttype=\'GÖREVDE KİMSE YOK\' or a.losttype=\'İŞ YOK\')) or a.task like \'%DENEME%\') then '+
				'			case when ((date_part(\'dow\',a.finish)=0 and round(cast(a.suresn/60 as numeric),0)>1) or a.task like \'%DENEME%\') then \'Çalışılmayan Süre\' else \'Sipariş Yetersizliği\' end else lgd.lostgroupcode end lostgroupcode '+
				'		,case when ((lgd.lostgroup=\'OEE-kullanilabilirlik\' or lgd.lostgroup=\'OEE-performans\') and a.losttype<>\'VARDİYA DEĞİŞİMİ\' and a.task not like \'%DENEME%\' and round(cast(a.suresn/60 as numeric),0)<=600) then \'KISA DURUS\' else \'\' end info	'+
				'		from (	'+
				'			select z.client,z.day,z.jobrotation	'+
				'				,case when (z.losttype=\'GÖREVDE KİMSE YOK\' or z.losttype=\'\') and z.suresn<=600 and (z.endval is not null or z.beginval is not null) then \'VARDİYA DEĞİŞİMİ\'	'+
				'					when z.losttype=\'\' and z.suresn>600 and (z.endval is not null or z.beginval is not null) then \'GÖREVDE KİMSE YOK\' 	'+
				'					else z.losttype end losttype,z.start,z.finish,z.suresn	'+
				'				,(SELECT string_agg(opname,\',\') opname from client_lost_details where type=\'l_c_t\' and client=z.client and day=z.day and jobrotation=z.jobrotation and start=z.start)as task '+
				'			from (	'+
				'				SELECT cld.client,cld.day,cld.jobrotation,cld.losttype,cld.start,cast(COALESCE(cld.finish,CURRENT_TIMESTAMP)as varchar(19))::timestamp finish	'+
				'					,DATE_PART(\'day\', cast(COALESCE(cld.finish,CURRENT_TIMESTAMP)as varchar(19))::timestamp - cld.start::timestamp) * 24*3600 '+
				'					+ DATE_PART(\'hour\', cast(COALESCE(cld.finish,CURRENT_TIMESTAMP)as varchar(19))::timestamp - cld.start::timestamp) * 3600 	'+
				'					+ DATE_PART(\'minute\', cast(COALESCE(cld.finish,CURRENT_TIMESTAMP)as varchar(19))::timestamp - cld.start::timestamp)*60 	'+
				'					+ DATE_PART(\'second\', cast(COALESCE(cld.finish,CURRENT_TIMESTAMP)as varchar(19))::timestamp - cld.start::timestamp) ' + 
				' 					+(case when jr.endval is not null and jrs.beginval is not null then 1 else 0 end) suresn,jr.endval,jrs.beginval	'+
				'				from client_lost_details cld 	'+
				'				join clients c on c.code=cld.client and c.uuid is not null '+
				'				left join job_rotations jr on substr(cast(cld.finish as varchar),12,5)=jr.endval	'+
				'				left join job_rotations jrs on substr(cast(cld.start as varchar),12,5)=jrs.beginval	'+
				'				where cld.type=\'l_c\' and cld.day=:day and cld.jobrotation=:jobrotation '+
				'			)z	'+
				'	)a	'+
				'	left join (select * from lost_group_details where lostgroup like \'OEE%\') lgd on lgd.losttype=a.losttype	'+
				'	where a.suresn>0  	'+
				' )b	'+
				' group by b.client,b.lostgroup	'+
				' order by b.client,b.lostgroup';
			return sequelize.query(sql_oee_lost, { replacements: { day:_day,jobrotation:_jr},type: sequelize.QueryTypes.SELECT}).then(function(lost_results){
				if(lost_results){
					for(let i=0;i<lost_results.length;i++){
						let item=lost_results[i];
						let idx=getIndex(oee_clients,'client',item.client);
						if(idx===-1){
							oee_clients.push({client:item.client,total:_total,closed:0,planned:0,availablity:0,unclassified:0,productions:0,poorquality:0});
						}
						idx=getIndex(oee_clients,'client',item.client);
						if(idx>-1){
							if(item.lostgroup==='OEE-uretimekapali'){
								oee_clients[idx].closed+=parseInt(item.suresn);
							}else if(item.lostgroup==='OEE-planlidurus'){
								oee_clients[idx].planned+=parseInt(item.suresn);
							}else if(item.lostgroup==='OEE-kullanilabilirlik'){
								oee_clients[idx].availablity+=parseInt(item.suresn);
							}else{
								oee_clients[idx].availablity+=parseInt(item.suresn);
							}
						}
					}
				}
				return null;
			}).then(function(){
				let sql_oee_prod='select a.client,round(sum(a.proctime))proctime,round(sum(a.proctimecurrent))proctimecurrent from ( '+
				'	select cpd.client,cpd.day,cpd.start,cpd.finish,cpd.suresn '+
				' 	,cpd.jobrotation,round(sum(cpd.production)/count(*))production,round(sum(cpd.productioncurrent)/count(*))productioncurrent '+
				'		,round(sum(cpd.proctime)/count(*))proctime,round(sum(cpd.proctimecurrent)/count(*))proctimecurrent  '+
				'	from (	 '+
				'		SELECT cpd.client,cpd.day,cpd.jobrotation,cpd.mould,cpd.mouldgroup,cpd.production,cpd.productioncurrent,cpd.start,cpd.finish,cpd.calculatedtpp,cpd.opname '+
				'			,DATE_PART(\'day\', cpd.finish::timestamp - cpd.start::timestamp) * 24*3600 '+
				'				+ DATE_PART(\'hour\', cpd.finish::timestamp - cpd.start::timestamp) * 3600  '+
				'				+ DATE_PART(\'minute\', cpd.finish::timestamp - cpd.start::timestamp)*60  '+
				'				+ DATE_PART(\'second\', cpd.finish::timestamp - cpd.start::timestamp) suresn '+
				'			,cpd.production*cpd.calculatedtpp proctime,cpd.productioncurrent*cpd.calculatedtpp proctimecurrent '+
				'		from client_production_details cpd  '+
				'		join clients c on c.code=cpd.client and c.uuid is not null '+
				'		where cpd.type in (\'c_p\',\'c_p_confirm\',\'c_p_out\') and cpd.productioncurrent>0  '+
				'			and cpd.day=:day and cpd.jobrotation=:jobrotation '+
				'	)cpd '+
				'	GROUP BY cpd.client,cpd.day,cpd.jobrotation,cpd.mould,cpd.mouldgroup,cpd.start,cpd.finish,cpd.suresn)a '+
				' group by a.client';
				return sequelize.query(sql_oee_prod, { replacements: { day:_day,jobrotation:_jr},type: sequelize.QueryTypes.SELECT}).then(function(prod_results){
					if(prod_results){
						for(let i=0;i<prod_results.length;i++){
							let item=prod_results[i];
							let idx=getIndex(oee_clients,'client',item.client);
							if(idx===-1){
								oee_clients.push({client:item.client,total:_total,closed:0,planned:0,availablity:0,unclassified:0,productions:0,poorquality:0});
							}
							idx=getIndex(oee_clients,'client',item.client);
							if(idx>-1){
								oee_clients[idx].productions+=parseInt(item.proctimecurrent);
							}
						}
					}
					return null;
				});
			}).then(function(){
				let sql_oee_quality='select a.client,round(sum(a.iskartasure))iskartasure from ( '+
				'	SELECT trd.client,trd.day,trd.jobrotation,trd.time,trd.erprefnumber,trd.opname,trd.rejecttype,trd.quantityscrap,pt.tpp,trd.quantityscrap*pt.tpp iskartasure '+
				'	from task_reject_details trd '+
				'	join clients c on c.code=trd.client and c.uuid is not null '+
				'	join product_trees pt on pt.name=trd.opname and pt.materialtype=\'O\' and pt.finish is null '+
				'	where trd.type=\'t_r\' and trd.day=:day and trd.jobrotation=:jobrotation )a '+
				'	group by a.client';
				return sequelize.query(sql_oee_quality, { replacements: { day:_day,jobrotation:_jr},type: sequelize.QueryTypes.SELECT}).then(function(quality_results){
					if(quality_results){
						for(let i=0;i<quality_results.length;i++){
							let item=quality_results[i];
							let idx=getIndex(oee_clients,'client',item.client);
							if(idx===-1){
								oee_clients.push({client:item.client,total:_total,closed:0,planned:0,availablity:0,unclassified:0,productions:0,poorquality:0});
							}
							idx=getIndex(oee_clients,'client',item.client);
							if(idx>-1){
								oee_clients[idx].poorquality+=parseInt(item.iskartasure);
							}
						}
					}
					return null;
				});
			}).then(function(){
				try {
					let _promises=[];
					let oee_write=function(item){
						let sql_oee_details='select * from client_oee_details where client=:client and day=:day and jobrotation=:jobrotation';
						return sequelize.query(sql_oee_details,{replacements:{client:item.client,day:item.day,jobrotation:item.jobrotation},type: sequelize.QueryTypes.SELECT}).then(function(result){
							if(result.length>0){
								let sql_update='update client_oee_details set kul=:kul,per=:per,kal=:kal,oee=:oee where client=:client and day=:day and jobrotation=:jobrotation';
								return sequelize.query(sql_update,{replacements:{client:item.client,day:item.day,jobrotation:item.jobrotation,kul:item.oee_kul_deger,per:item.oee_per_deger,kal:item.oee_kal_deger,oee:item.oee_deger},type: sequelize.QueryTypes.UPDATE})
									.catch(err => {
										Sentry.captureException(err);
										Sentry.addBreadcrumb({
											category: 'oee_write',
											message: 'oee_write update ',
											level: 'error',
											data:item
										});
										setTimeout(() => {
											process.exit(102);
										}, 500);
										return;
									});
							}else{
								let sql_insert='insert into client_oee_details (client,day,jobrotation,kul,per,kal,oee) values (:client,:day,:jobrotation,:kul,:per,:kal,:oee)';
								return sequelize.query(sql_insert,{replacements:{client:item.client,day:item.day,jobrotation:item.jobrotation,kul:item.oee_kul_deger,per:item.oee_per_deger,kal:item.oee_kal_deger,oee:item.oee_deger},type: sequelize.QueryTypes.INSERT})
									.catch(err => {
										Sentry.captureException(err);
										Sentry.addBreadcrumb({
											category: 'oee_write',
											message: 'oee_write insert ',
											level: 'error',
											data:item
										});
										setTimeout(() => {
											process.exit(103);
										}, 500);
										return;
									});
							}
						});
					};
					//oee hesaplaması
					//let newclients=[
					//	'1400T'
					//	,'1500T'
					//	,'ROCHE'
					//	,'P113'
					//	,'P301'
					//];
					for(let i=0;i<oee_clients.length;i++){
						let item=oee_clients[i];
						//if(newclients.indexOf(item.client) === -1){
							item.day=_day;
							item.jobrotation=_jr;
							item.oee_cns=item.total-item.closed-item.planned;
							item.oee_is=item.oee_cns-item.availablity-item.unclassified;
							item.oee_pcs=isNaN(item.productions)?0:item.productions>item.oee_is?item.oee_is:item.productions;
							item.oee_ecs=isNaN(item.poorquality)?0:item.poorquality>item.oee_pcs?0:item.oee_pcs-item.poorquality;
							item.oee_kul_deger=item.oee_cns>0?Math.round(100*(item.oee_is)/(item.oee_cns),2):0;
							item.oee_per_deger=item.oee_is>0?Math.round(100*(item.oee_pcs)/(item.oee_is),2):0;
							item.oee_kal_deger=item.oee_pcs>0?Math.round(100*(item.oee_ecs)/(item.oee_pcs),2):0;
							item.oee_kul_deger=((isNaN(item.oee_kul_deger)||item.oee_kul_deger)<0?0:item.oee_kul_deger);
							item.oee_per_deger=((isNaN(item.oee_per_deger)||item.oee_per_deger)<0?0:item.oee_per_deger);
							item.oee_kal_deger=((isNaN(item.oee_kal_deger)||item.oee_kal_deger)<0?0:item.oee_kal_deger);
							item.oee_deger=Math.round((item.oee_kul_deger*item.oee_per_deger*item.oee_kal_deger)/10000,2);
							processModule.send({event:'calculated_oee',data:item});
							_promises.push(oee_write(item));
						//}
					}
					return Promise.all(_promises).then(function(){
						processModule.send({event:'calculated_oee_all',data:oee_clients});
					});
				} catch (error) {
					Sentry.captureException(error);
				}
			});
		}else{
			Sentry.captureMessage('oee_calculate jr_results.length=0 '+ convertFullLocalDateString(new timezoneJS.Date()));
		}
		return;
	}).then(function(){
		//cld_opsayi,cld_refsayi
		let sql='UPDATE client_lost_details cld '+
		' SET opsayi = z.opsayi '+
		' FROM ( '+
		'	SELECT client,day,jobrotation,losttype,start,finish,count(*) opsayi '+
		'	from client_lost_details '+
		'	where type=\'l_e\' and client<>\'\' and day>current_date - interval \'10 day\' '+
		'	GROUP BY client,day,jobrotation,losttype,start,finish '+
		'	)z '+
		' WHERE '+
		'	z.client=cld.client and z.day=cld.day and z.jobrotation=cld.jobrotation and z.losttype=cld.losttype and z.start=cld.start and z.finish=cld.finish '+
		'	and cld.type in (\'l_c\',\'l_c_t\') and coalesce(cld.opsayi,0)<>z.opsayi';
		return sequelize.query(sql,{ type: sequelize.QueryTypes.UPDATE}).then(function(){
			let sql_1='UPDATE client_lost_details cld '+
			' SET refsayi = z.refsayi '+
			' FROM ( '+
			'	SELECT client,day,jobrotation,losttype,start,finish,count(*) refsayi '+
			'	from client_lost_details '+
			'	where type=\'l_c_t\' and day>current_date - interval \'10 day\' '+
			'	GROUP BY client,day,jobrotation,losttype,start,finish '+
			'	)z '+
			' WHERE '+
			'	z.client=cld.client and z.day=cld.day and z.jobrotation=cld.jobrotation and z.losttype=cld.losttype and z.start=cld.start and z.finish=cld.finish '+
			'	and cld.type in (\'l_c\',\'l_c_t\') and coalesce(cld.refsayi,0)<>z.refsayi';
			return sequelize.query(sql_1,{type: sequelize.QueryTypes.UPDATE});
		}).then(function(){
			//cpd_opsayi
			let sql='SELECT * from client_production_details where type=\'c_p\' and productioncurrent>0 and day>current_date - interval \'30 day\' and finish is not null and opsayi is null order by start limit 100';
			return sequelize.query(sql,{ type: sequelize.QueryTypes.SELECT}).then(function(results_cpd){
				if(results_cpd){
					let cpd_update=function(item){
						let _id=item.id;
						let sql_1='select count(a.*) opsayi from (select employee from client_production_details where type=\'e_p\' and start>=:start and finish<=:finish and client=:client and day=:day and jobrotation=:jobrotation GROUP BY employee)a';
						return sequelize.query(sql_1,{replacements:{start:item.start,finish:item.finish,client:item.client,day:item.day,jobrotation:item.jobrotation},type: sequelize.QueryTypes.SELECT}).then(function(result){
							if(result.length>0){
								let sql_update='update client_production_details set opsayi=:opsayi where id=:id';
								return sequelize.query(sql_update,{replacements:{opsayi:result[0].opsayi,id:_id},type: sequelize.QueryTypes.UPDATE});
							}
							return null;
						});
					};
					let _promises=[];
					for(let i=0;i<results_cpd.length;i++){
						_promises.push(cpd_update(results_cpd[i]));
					}
					return Promise.all(_promises);
				}
				return null;
			});
		});
	}).then(function(){
		process.nextTick(function(){
			setTimeout(function() {
				run();
			}, 30*1000);
		});
	}).catch(err => {
		Sentry.addBreadcrumb({
			category: 'oee_calculate',
			message: 'err 100 ',
			level: 'error',
			data:''
		});
		Sentry.captureException(err);
		setTimeout(() => {
			process.exit(105);
		}, 500);
		return;
	});
};
let getIndex=(array,key,keyValue)=>{
	if(typeof array.length!='undefined')
		for(let i = 0, len = array.length; i < len; i++){
			if(array[i][key]==keyValue)	return i;
		}
	else
		for(let i in array)
			if(typeof array[i]!='function')
				if(array[i][key]==keyValue)	return i;
	return -1;
};
let convertFullLocalDateString = (_time,_ms) => {
	var dt = new timezoneJS.Date(_time);
	return dt.toString('yyyy-MM-dd HH:mm:ss'+(_ms==true?'.SSS':''));
};