const cp = require('child_process');
const fs=require('fs');
//const fsE=require('fs-extra');
const https=require('https');
const path = require('path');
const processModule = require('process');
const Sentry = require('@sentry/node');
Sentry.init({
	dsn: 'https://4e61d0b3664d41c9a849f8edbb2b2255@sentry.kaitek.com.tr/3'
});
let _pid=false;
let _importer_app=false;
//let _mh_importpath=false;
let _waitImportResponse=false;
processModule.on('message', function (obj) {
	let event = obj.event;
	switch (event) {
	case 'start_data_importer':{
		_pid=obj.pid;
		_importer_app=obj.app;
		//_mh_importpath=obj.mh_importpath;
		processModule.send({event:'data_importer_started',pid:_pid});
		setTimeout(() => {
			run();
		}, 5000);
		break;
	}
	}
});
processModule.on('uncaughtException', function(err){
	Sentry.captureException(err);
	setTimeout(() => {
		process.exit(200);
	}, 500);
	return;
});
let run=()=>{
	setTimeout(() => {
		controlImportPath();
	}, 500);
	//if(_mh_importpath){
	//	setTimeout(() => {
	//		controlMHImportPath();
	//	}, 100);
	//}
};
let move=(oldPath, newPath, callback)=>{
	fs.readFile(oldPath, 'utf8', function (err, data) {
		if (err){
			return;
		}
		try {
			let obj = JSON.parse(data);
			if(obj.material_prepare === true || obj.material_prepare === 'true'){
				fs.rename(oldPath, newPath, function (err) {
					if (err) {
						if (err.code === 'EXDEV') {
							copy();
						} else {
							callback(err);
						}
						return;
					}
					callback();
				});
			}
		} catch (error) {
			//Sentry.captureException(error);
			callback(error);
		}
	});
	let copy=()=> {
		var readStream = fs.createReadStream(oldPath);
		var writeStream = fs.createWriteStream(newPath);
		readStream.on('error', callback);
		writeStream.on('error', callback);
		readStream.on('close', function () {
			let isError=false;
			try {
				//delFileBatch(oldPath);
				fs.unlink(oldPath, callback);
			} catch (error) {
				isError=true;
			} finally{
				if(isError){
					isError=false;
					try {
						fs.rm(oldPath, callback);
					} catch (error) {
						isError=true;
					}finally{
						if(isError){
							isError=false;
							try {
								delFileBatch(oldPath);
								//fs.unlink(oldPath, callback);
							} catch (error) {
								isError=true;
								callback(error);
							}
						}
					}
				}
				
			}
		});
		readStream.pipe(writeStream);
	};
	let delFileBatch=(oldPath)=>{
		var ls = cp.spawn(path.join(__dirname, '../fDel.bat'), [oldPath.split(';').join('!')]);
		ls.stdout.on('data', function (data) {
			// console.log('stdout: ' + data);
		});
		ls.stderr.on('data', function (data) {
			callback(data);
			// console.log('stderr: ' + data);
		});
		ls.on('exit', function (code) {
			// console.log('child process exited with code ' + code);
			callback();
		});
	};
};

//let controlMHImportPath=()=>{
//	fs.readdir(_mh_importpath, (err, files) => {
//		if(files&&files.length>0){
//			files.forEach(file => {
//				move(_mh_importpath+file,__dirname+'/../_import/'+file,function(err){
//					if(err){
//						// console.log('err-controlMHImportPath',_mh_importpath+file,__dirname+'/../_import/'+file);
//						// console.error(err);
//						// Sentry.captureException(err);
//					}
//				});
//				//fsE.move(_mh_importpath+file,__dirname+'/../_import/'+file, (err) => {
//				//	if (err){
//				//		console.log('err-controlMHImportPath');
//				//		console.log(err);
//				//		Sentry.captureException(err);
//				//	} 
//				//});
//			});
//		}
//	});
//};
let controlImportPath=()=>{
	if(_waitImportResponse==false){
		let path_import=__dirname+'/../_import/';
		fs.readdir(path_import, (err, files) => {
			if(_waitImportResponse==false){
				if(files.length>0){
					files.forEach(file => {
						if(_waitImportResponse==false){
							_waitImportResponse=path_import+file;
							readFileImport(path_import+file);
						}else{
							return;
						}
					});
				}else{
					run();
				}
			}else{
				run();
			}
		});
	}else{
		run();
	}
};
let readFileImport=(file)=>{
	let obj;
	//console.log('readFileImport',file);
	try {
		let _ext=path.extname(file);
		if(_ext==='.json'){
			fs.readFile(file, 'utf8', function (err, data) {
				if (err){
					// console.log('Error:readFileImport-1');
					// console.log(err);
					if(err.code!=='EBUSY'){
						Sentry.captureException(err);
					}
					setTimeout(() => {
						process.exit(201);
					}, 500);
					return;
				}
				try {					
					//bom remove
					if (data.charCodeAt(0) === 0xFEFF) {
						data.splice(0,1);
					}
					if(data.length==0){
						fs.unlinkSync(file);
						throw 'Dosya boş';
					}
					try {
						obj = JSON.parse(data);
						//console.log('readfile',obj);
						if(obj.material_prepare === true || obj.material_prepare === 'true'){
							processModule.send({event:'data_imported_material_prepare',data:obj,path:file});
							_waitImportResponse=false;
							run();
						}else if(obj.api){
							processModule.send({event:'imported_api_call',data:obj,path:file});
							_waitImportResponse=false;
							run();
						}else{
							sendRequestImport(obj);
						}
					} catch (error) {
						Sentry.captureException(error);
						let path_import=__dirname+'/../_import/';
						let path_import_err=__dirname+'/../_import_err/';
						let new_path=file.split(path_import).join(path_import_err);
						fs.unlinkSync(file);
						fs.writeFileSync(new_path, data);
						setTimeout(() => {
							process.exit(203);
						}, 500);
						return;
					}
				} catch (error) {
					Sentry.captureMessage('[readFileImport] ' + error);
					if(error!='Dosya boş'){
						Sentry.captureException(error);
					}
					setTimeout(() => {
						process.exit(202);
					}, 500);
					return;
				}
			});
		}
		//if(_ext==='.xlsx'){
		//	//dosya adı sistemdeki tablonun adı olmak zorunda
		//	let _basename=path.basename(file);
		//	let _filename=_basename.replace(_ext,'');
		//	let _dest_filename=file.replace('xlsx','json');
		//	const convertExcel = require('excel-as-json').processFile;
		//	convertExcel(file, _dest_filename, null, function(err, data){
		//		if (err){
		//			console.log('Error:readFileImport-2');
		//			console.log(err);
		//			_waitImportResponse=false;
		//			run();
		//			return;
		//		} 
		//		try {
		//			fs.unlinkSync(file);
		//			obj = {customer:_customer,app:_app,file:_dest_filename,table:_filename,data:data};
		//			sendRequestImport(obj);
		//		} catch (error) {
		//			_waitImportResponse=false;
		//			run();
		//		}
		//	});
		//}
	} catch (error) {
		// _waitImportResponse=false;
		// run();
		Sentry.captureException(error);
		setTimeout(() => {
			process.exit(204);
		}, 500);
	}
};
let copyFileImportErr=(file,msg)=>{
	let path_import=__dirname+'/../_import/';
	let path_import_err=__dirname+'/../_import_err/';
	let obj;
	let new_path=file.split(path_import).join(path_import_err);
	try {
		fs.readFile(file, 'utf8', function (err, data) {
			if (err){
				//console.log('copyFileImportErr-1');
				//console.log(err);
				_waitImportResponse=false;
				run();
				return;
			} 
			try {
				obj = JSON.parse(data);
				obj.msg=msg;
				fs.unlinkSync(file);
				fs.writeFile(new_path, JSON.stringify(obj), function (err) {
					if (err){
						//console.log('copyFileImportErr-2');
						//console.log(err);
						_waitImportResponse=false;
						run();
						return;
					} 
					//console.log('Dosya taşındı. '+new_path);
					//console.log('Import Hata:'+obj.msg);
					_waitImportResponse=false;
					run();
				});
			} catch (error) {
				Sentry.captureException(error);
				//setTimeout(() => {
				//	process.exit(205);
				//}, 500);
				// _waitImportResponse=false;
				// run();
			}
		});
	} catch (error) {
		Sentry.captureException(error);
		setTimeout(() => {
			process.exit(206);
		}, 500);
		// _waitImportResponse=false;
		// run();
	}
};
let sendRequestImport=(params)=>{
	let options = {
		hostname: '127.0.0.1',
		path: '/'+_importer_app+'/tr/m2m/ImportData',
		method: 'POST',
		headers: {
			'X-Requested-With':'XMLHttpRequest'
			,'Content-Type':'application/x-www-form-urlencoded'
			,'X-HTTP-Method-Override': 'PUT' 
		},
		rejectUnauthorized: false,
		requestCert: true,
		agent: false
	};

	let req = https.request(options, function(res) {
		res.setEncoding('utf8');
		res.on('data', function (chunk) {
			try {
				let obj_resp=JSON.parse(chunk);
				if(obj_resp.success==true&&obj_resp.message=='OK'){
					fs.unlinkSync(_waitImportResponse);
					_waitImportResponse=false;
					run();
				}else{
					processModule.send({event:'import_info_message',data:chunk,fn:'req.on.data'});
					copyFileImportErr(_waitImportResponse,obj_resp.message);
				}
			} catch (error) {
				Sentry.captureException(error);
				processModule.send({event:'import_info_message',data:chunk,fn:'req.on.data'});
				//console.log('Error:sendRequestImport-2');
				copyFileImportErr(_waitImportResponse,error.message);
			}
			return;
		});
	});

	req.on('error', function(e) {
		//console.log('ERROR:sendRequestImport-1 ' + e.message);
		_waitImportResponse=false;
		run();
	});

	req.write(JSON.stringify(params));
	req.end();
};