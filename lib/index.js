'use strict';
/*
event tanımları
jse_ javascript node event
cn__sn client node to server node event
sn_cn server node to client node event
sse_ server service event
sre_ server route event
*/
var timezoneJS = require('timezone-js');
var tzdata = require('tzdata');
var dcs_server=require('./dcs_server');

function init(){
	var _tz = timezoneJS.timezone;
	_tz.loadingScheme = _tz.loadingSchemes.MANUAL_LOAD;
	_tz.loadZoneDataFromObject(tzdata);
	new dcs_server({});
}
exports.init = init;
