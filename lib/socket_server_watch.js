const formidable = require('formidable');
const fs=require('fs');
const https=require('https');
const path = require('path');
const processModule = require('process');
const url=require('url');
const util = require('util');
const Sentry = require('@sentry/node');
Sentry.init({
	dsn: 'https://4e61d0b3664d41c9a849f8edbb2b2255@sentry.kaitek.com.tr/3'
});
let __clients=[];//watch ekranları
let _io=false;
let _pid=false;
let _pin=false;
let _ip=false;
let _port=false;
let _server=false;
let _oeedata=false;
processModule.on('message', function (obj) {
	let event = obj.event;
	let param = obj.param;
	switch (event) {
	case 'start_server':{
		_pid=obj.pid;
		_pin=obj.pin;
		_ip=obj.ip;
		_port=obj.port;
		start_server(param);
		break;
	}
	case 'device_connect':{
		_io.sockets.emit('cn_js_watch',{event:'device_connect',ip:obj.ip});
		break;
	}
	case 'device_disconnect':{
		_io.sockets.emit('cn_js_watch',{event:'device_disconnect',ip:obj.ip});
		break;
	}
	case 'res_getDataAllClients':{
		let _client=false;
		for(let i=0;i<__clients.length;i++){
			let _tclient=__clients[i];
			if(_tclient.ip==obj.ip){
				_client=_tclient;
				_client.emit('cn_js_watch',{event:obj.event.replace('res_',''),data:obj.data});
			}
		}
		if(_oeedata){
			_io.sockets.emit('cn_js_watch',{event:'calculated_oee',data:_oeedata});
		}
		break;
	}
	case 'watch_calc_tempo':{
		_io.sockets.emit('cn_js_watch',{event:obj.event,data:obj.data});
		break;
	}
	case 'watch_info':{
		_io.sockets.emit('cn_js_watch',{event:obj.event,data:obj.data});
		break;
	}
	case 'device_io_info':{
		_io.sockets.emit('cn_js_watch',{event:obj.event,data:obj.data,ip:obj.ip});
		break;
	}
	case 'watch_input_signal':{
		_io.sockets.emit('cn_js_watch',{event:obj.event,data:obj.data});
		break;
	}
	case 'res_lockpc':{
		for(let i=0;i<__clients.length;i++){
			if(__clients[i].ip==obj.ip){
				__clients[i].emit('cn_js_watch',{event:obj.event.replace('res_',''),data:obj.msg,ip:obj.ip});
			}
		}
		break;
	}
	case 'res_unlockpc':{
		for(let i=0;i<__clients.length;i++){
			if(__clients[i].ip==obj.ip){
				__clients[i].emit('cn_js_watch',{event:obj.event.replace('res_',''),data:obj.msg,ip:obj.ip});
			}
		}
		break;
	}
	case 'res_updateApp':{
		for(let i=0;i<__clients.length;i++){
			if(__clients[i].ip==obj.ip){
				__clients[i].emit('cn_js_watch',{event:obj.event.replace('res_',''),data:obj.msg,ip:obj.ip});
			}
		}
		break;
	}
	case 'calculated_oee':{
		_oeedata=obj.data;
		_io.sockets.emit('cn_js_watch',{event:obj.event,data:_oeedata});
		break;
	}
	default:{
		//console.log(obj);
		break;
	}
	}
});
processModule.on('uncaughtException', function(err){
	Sentry.captureException(err);
	setTimeout(() => {
		process.exit(30);
	}, 500);
	return;
});
let start_server=()=>{
	function onRequest(req,res){
		try {
			let uri = url.parse(req.url).pathname;
			let x = uri.replace('/','').split('&');
			if(x.length>1){
				let obj={};
				for(let i=0;i<x.length;i++){
					let citem=x[i].split('=');
					obj[citem[0]]=citem[1];
				}
			}else{
				if (uri == '/'){
					uri = '/index.html';
				}
				if(req.url =='/crashreport/'&& req.method.toLowerCase() == 'post'){
					let form = new formidable.IncomingForm();
					form.encoding = 'utf-8';
					form.uploadDir = __dirname+'/../log';
					form.keepExtensions = true;
					form.parse(req, function(err, fields, files) {
						res.writeHead(200, {'content-type': 'text/plain'});
						res.write('received upload:\n\n');
						res.end(util.inspect({fields: fields, files: files}));
					});
					return;
				}else{
					let filename = __dirname+'/..'+unescape(uri);
					let stats;
					let extname = path.extname(filename);
					let contentType = 'text/html';
					switch (extname) {
					case '.asp':
					case '.aspx':
					case '.cc':
					case '.cfm':
					case '.cgi':
					case '.exe':
					case '.gdb':
					case '.htm':
					case '.ini':
					case '.jsp':
					case '.mdb':
					case '.php':
					case '.php3':
					case '.php4':
					case '.pl':
					case '.py':
					case '.rb':
					case '.sh':
					case '.tml':
					case '.wdm':
					case '.wdm.':
						res.writeHead(404, {'Content-Type': 'text/plain'});
						res.write('404 Not Found\n');
						res.end();
						return;
					case '.zip':
						contentType = 'application/zip';
						break;
					case '.js':
						contentType = 'text/javascript';
						break;
					case '.css':
						contentType = 'text/css';
						break;
					case '.txt':
						contentType = 'text/plain';
						break;
					}
					try {
						stats = fs.lstatSync(filename);
					} catch (e) {
						res.writeHead(404, {'Content-Type': 'text/plain'});
						res.write('404 Not Found\n');
						res.end();
						return;
					}
					if (stats.isFile()) {
						let fileStream = fs.createReadStream(filename);
						fileStream.on('error', function () {
							res.writeHead(404, { 'Content-Type': 'text/plain'});
							res.end('file not found');
						});
						fileStream.on('open', function() {
							res.writeHead(200, {'Content-Type': contentType});
							if(extname=='.zip'){
								res.writeHead(200,{'Content-Disposition': 'attachment; filename='+path.basename(uri)});
							}
						});
						fileStream.on('end', function() {
							//
						});
						fileStream.pipe(res);
					} else if (stats.isDirectory()) {
						res.writeHead(200, {'Content-Type': 'text/plain'});
						res.write('Index of '+uri+'\n');
						res.write('TODO, show index?\n');
						res.end();
					} else {
						res.writeHead(200, {'Content-Type': 'text/plain'});
						res.write('200 Internal server error\n');
						res.end();
					}
				}
			}
		} catch(e) {
			res.writeHead(200);
			res.end();     
			Sentry.captureException(e);
		}	
	}
	let options = {
		key: fs.readFileSync(__dirname+'/../'+path.join('ssl', 'kaitek2.key'))
		//, ca: [ fs.readFileSync(path.join('certs', 'my-root-ca.crt.pem'))]
		, cert: fs.readFileSync(__dirname+'/../'+path.join('ssl', 'kaitek2.crt'))
		, requestCert: false
		, rejectUnauthorized: false
	};
	_server=https.createServer(options,onRequest);
	_io = require('socket.io')(_server,{
		cors: {
			origin: "https://"+_ip,
			methods: ["GET", "POST"]
		}
	});
	_io.on('connection', function (client){
		let clientIp = client.request.connection.remoteAddress.replace('::ffff','').replace(':','');
		client.ip=clientIp;
		let idx=getIndex(__clients,'ip',client.ip);
		if(idx===-1){
			__clients.push(client);
		}
		client.on('error', function(error){
			Sentry.captureException(error);
			setTimeout(() => {
				process.exit(31);
			}, 500);
		});
		client.on('disconnect', function(){
			for(let i=0;i<__clients.length;i++){
				let _client=__clients[i];
				if(_client.ip==client.ip){
					__clients.splice(i,1);
				}
			}
		});
		client.on('js_cn_watch',function(obj){
			let event=obj.event;
			switch(event) {
			case 'getDataAllClients':{
				processModule.send({event:'req_getDataAllClients',ip:client.ip});
				break;
			}
			case 'lockpc':{
				obj.watchIP=client.ip;
				processModule.send(obj);
				break;
			}
			case 'unlockpc':{
				obj.watchIP=client.ip;
				processModule.send(obj);
				break;
			}
			case 'updateApp':{
				obj.watchIP=client.ip;
				processModule.send(obj);
				break;
			}
			default:{
				//console.log('js_cn_watch');
				//console.log(obj);
				break;
			}
			}
		});
	});
	_server.timeout = 20000;
	_server.listen(_port);
	processModule.send({event:'started_socket_server_watch',pid:_pid,port:_port});
};
let getIndex=(array,key,keyValue)=>{
	if(typeof array.length!='undefined')
		for(let i = 0, len = array.length; i < len; i++){
			if(array[i][key]==keyValue)	return i;
		}
	else
		for(let i in array)
			if(typeof array[i]!='function')
				if(array[i][key]==keyValue)	return i;
	return -1;
};