const processModule = require('process');
const fs=require('fs');
const path = require('path');
const util = require('util');
const fs_writeFile = util.promisify(fs.writeFile);
const nodemailer = require('nodemailer');
const amqp = require('amqplib/callback_api');
const Promise = require('promise');
const Sequelize = require('sequelize');
const timezoneJS = require('timezone-js');
const Sentry = require('@sentry/node');
Sentry.init({
	dsn: 'https://4e61d0b3664d41c9a849f8edbb2b2255@sentry.kaitek.com.tr/3'
});
let sequelize;
let conn=null;
let pubChannel = null;
let _pid=false;
let _ip=false;
let _port=false;
let _customer=false;
let _dbParams=false;
let arr_queue_assert=[];
let arr_tmp_queue_assert=[];
let _flag_data_is_finished=false;
let _url_amqp=false;
let _material_prepare=false;
let mailTransporter=false;
let _gun=false;
let _saat=false;
let _obj_sv=false;
let path_export=__dirname+'/../_export/';
processModule.on('message', function (obj) {
	let event = obj.event;
	switch (event) {
	case 'init_server':{
		_pid=obj.pid;
		_ip=obj.ip;
		_port=obj.port;
		_customer=obj.customer;
		_dbParams=obj.param;
		_material_prepare=obj.material_prepare;
		_url_amqp='amqp://admin:q32wxcx@'+obj.ip;
		connect_amqp();
		break;
	}
	default:{
		//console.log(obj);
		break;
	}
	}
});
processModule.on('uncaughtException', function(err){
	//console.log(err);
	Sentry.captureException(err);
	setTimeout(() => {
		process.exit(500);
	}, 500);
	return;
});
let connect_amqp = ()=>{
	amqp.connect(_url_amqp,function(_err,_conn) {
		if (_err) {
			Sentry.captureMessage('[AMQP-S] connect ' + _err);
			//console.error('[AMQP-S]', _err.message);
			setTimeout(() => {
				process.exit(501);
			}, 500);
			return;
		}
		if(conn==null){
			conn=_conn;
			conn.on('error', function(err) {
				Sentry.captureMessage('[AMQP-S] conn error ' + err);
				//if (err.message && err.message !== 'Connection closing') {
					//console.error('[AMQP-S] conn error', err.message);
				//}
				Sentry.captureException(err);
				setTimeout(function(){
					process.exit(502);
				}, 1000);
				return;
			});
			conn.on('close', function() {
				Sentry.captureMessage('[AMQP-S] close');
				setTimeout(function(){
					process.exit(503);
				}, 1000);
				return;
			});
		}
		createChannel();
	});
};
let createChannel=()=> {
	conn.createChannel(function(err, ch) {
		if (closeOnErr(err)) return;
		if(pubChannel==null){
			pubChannel = ch;
			pubChannel.on('error', function(err) {
				Sentry.captureMessage('[AMQP-S] channel err ' + err);
				//console.error('[AMQP-S] channel error', err.message);
				Sentry.captureException(err);
				setTimeout(() => {
					process.exit(504);
				}, 500);
				return;
			});
			pubChannel.on('close', function() {
				Sentry.captureMessage('[AMQP-S] channel close ');
				//console.log('[AMQP-S] channel closed');
				setTimeout(() => {
					process.exit(505);
				}, 500);
				return;
			});
			processModule.send({event:'cp_server_inited',pid:_pid});
			db_connect(_dbParams);
		}
	});
};
let closeOnErr=(err)=> {
	if (!err) return false;
	Sentry.captureMessage('[AMQP-S] error' + err);
	//console.error('[AMQP-S] error', err);
	Sentry.captureException(err);
	setTimeout(function(){
		process.exit(506);
	}, 1000);
	return true;
};
let db_connect = (obj) => {
	sequelize = new Sequelize(obj.database, obj.username, obj.password, {
		host: obj.host,
		dialect: 'postgres',
		logging: false,
		pool: {
			max: 10,
			min: 0,
			acquire: 100000,
			idle: 10000
		},
		timezone:obj.timezone
	});
	return sequelize
		.authenticate()
		.then(() => {
			mailTransporter = nodemailer.createTransport({
				service: 'gmail',
				auth: {
					user: 'verimotrt@gmail.com',
					pass: 'mkktjptwruhmczmg'
				}
			});
			return Promise.all([queue_asserter()]).then(()=>{
				process.nextTick(function(){
					setTimeout(function() {
						return run();
					}, 10);
				});
			});
		})
		.catch(err => {
			Sentry.captureMessage('[AMQP-S-db] ' + err);
			Sentry.captureException(err);
			setTimeout(() => {
				process.exit(507);
			}, 500);
			return;
		});
};
let control_arr_tmp_queue_assert=()=>{
	if(arr_tmp_queue_assert.length>0){
		process.nextTick(function(){
			setTimeout(function() {
				return Promise.all([control_arr_tmp_queue_assert()]);
			}, 5);
		}); 
	}else{
		return true;
	}
};
let queue_asserter=()=>{
	const _w_=(_ip==='192.168.1.40'||_ip==='192.168.1.10')?' and code<>\'03.06.0003\' ':'';
	return sequelize.query('SELECT id,code,uuid,ip,connected,status,statustime,workflow,pincode,versionapp,versionshell,computername,lastprodstarttime,lastprodtime '+
	' from clients where uuid is not null and isactive=true and ip<>\'\' and ip is not null '+_w_+' order by code'
	, { type: sequelize.QueryTypes.SELECT}).then(function(results) {
		try {
			processModule.send({event:'set_clients',data:results});
		} catch (error) {
			Sentry.captureException(error);
		}
		for(let i=0;i<results.length;i++){
			let item=results[i];
			if(item.code&&item.code!==null&&item.code!==''&&item.code!=='undefined'){
				if(arr_queue_assert.indexOf(item.code+'_server')===-1){
					if(pubChannel!==null){
						arr_tmp_queue_assert.push(item.code+'_server');
						pubChannel.assertQueue(item.code+'_server',{durable: true},function(err, ok){
							if(err){
								Sentry.captureMessage('[AMQP-S-assert] ' + err);
								//console.error('[AMQP-S] error-assertQueue', err);
								setTimeout(function(){
									process.exit(531);
								}, 1000);
							}
							arr_tmp_queue_assert.splice(arr_tmp_queue_assert.indexOf(ok.queue),1);
						});
						arr_queue_assert.push(item.code+'_server');
					}
				}
			}else{
				if(item.ip&&item.ip!==''&&item.ip!=='undefined'){
					if(arr_queue_assert.indexOf(item.ip+'_server')===-1){
						if(pubChannel!==null){
							arr_tmp_queue_assert.push(item.ip+'_server');
							pubChannel.assertQueue(item.ip+'_server',{durable: true},function(err, ok){
								if(err){
									Sentry.captureMessage('[AMQP-S-assert] ' + err);
									//console.error('[AMQP-S] error-assertQueue', err);
									setTimeout(function(){
										process.exit(532);
									}, 1000);
								}
								arr_tmp_queue_assert.splice(arr_tmp_queue_assert.indexOf(ok.queue),1);
							});
							arr_queue_assert.push(item.ip+'_server');
						}
					}
				}
			}
		}
		return Promise.all([control_arr_tmp_queue_assert()]);
	}).then(function(){
		process.nextTick(function(){
			setTimeout(function() {
				queue_asserter();
			}, 10000);
		});
	});
};
let run=()=>{
	process.nextTick(function(){
		setTimeout(function() {
			syncMessage();
		}, 500);
	});
};
let insert_sync_message=(client,type,data)=>{
	let sql_sm='insert into _sync_messages (ip,uuid,type,data,queued,"createdAt","updatedAt") '+
	'values (:ip,:uuid,:type,:data,0,CURRENT_TIMESTAMP(0),CURRENT_TIMESTAMP(0))';
	return sequelize.query(sql_sm, { replacements:{ip:client+'_server',uuid:null,type:type,data:JSON.stringify(data)}, type: sequelize.QueryTypes.INSERT});
};
let sendMesageToQueue = (channel, queue, content)=>{
	if(channel!==null){
		//@@todo:queue var mı diye kontrol edilmeli
		channel.sendToQueue(queue, new Buffer.from(content), {persistent: true});
	}
	return null;
};
let syncMessage = ()=>{
	let x=new Date();
	//return sequelize.query("SELECT * from \"_sync_messages\" where queued=0 order by case when type in ('remote_quality_confirm','remote_quality_rejection') then 0 else case when data->>'tableName'='employees' then 0 else 1 end end,id limit 100 "
	return sequelize.query('SELECT * from "_sync_messages" where queued=0 order by id limit 1000 '
		, { type: sequelize.QueryTypes.SELECT}).then(function(results) {
		let _promises=[];
		//if(results.length>0){
		//	console.log('syncMessage',x.toLocaleTimeString(),results.length);
		//}
		for(let i=0;i<results.length;i++){
			let result=results[i];
			if(pubChannel!==null){
				//@@todo:queue var mı diye kontrol edilmeli
				// pubChannel.sendToQueue(result.ip, new Buffer.from(JSON.stringify(result)), {persistent: true});
				_promises.push(sendMesageToQueue(pubChannel, result.ip, JSON.stringify(result)));
				_promises.push(recordDelete(result.id));
			}
		}
		return Promise.all(_promises);
	}).then(()=>{
		if(_material_prepare){
			let urecord=(record_id)=>{
				let sql='update case_labels set synced=2 where record_id=:record_id';
				return sequelize.query(sql,{replacements:{record_id:record_id},type: sequelize.QueryTypes.UPDATE});
			};
			return sequelize.query('select code from clients where uuid is not null and isactive=true and ip<>\'\' and ip is not null',{type: sequelize.QueryTypes.SELECT}).then((clients)=>{
				let _c=clients;
				let sql='SELECT erprefnumber,time,status,record_id,synced,canceldescription,canceltime,cancelusername from case_labels where COALESCE(synced,0)<2';
				return sequelize.query(sql,{type: sequelize.QueryTypes.SELECT}).then((results)=>{
					if(results&&results.length>0){
						let _promises=[];
						for(let i=0;i<results.length;i++){
							let _record=results[i];
							for(let j=0;j<_c.length;j++){
								let data={
									tableName:'case_labels'
									,erprefnumber:_record.erprefnumber
									,status:_record.status
									,record_id:_record.record_id
									,canceldescription:_record.canceldescription
									,canceltime:convertFullLocalDateString(_record.canceltime)
									,cancelusername:_record.cancelusername
								};
								_promises.push(insert_sync_message(_c[j].code,(parseInt(_record.synced)==0?'mh_label_inserted':'mh_label_updated'),data));
							}
							_promises.push(urecord(_record.record_id));
						}
						return Promise.all(_promises);
					}
					return null;
				});
			});
		}else{
			return null;
		}
	}).then(function(){
		if((x.getSeconds()==0)&&parseInt(x.getMinutes())%5==0){
			if(_flag_data_is_finished===false){
				_flag_data_is_finished=true;
				return sub_process();
			}
		}else{
			_flag_data_is_finished=false;
		}
		return null;
	}).then(function(){
		process.nextTick(function(){
			setTimeout(function() {
				run();
			}, 100);
		});
	}).catch(err => {
		Sentry.captureException(err);
		Sentry.addBreadcrumb({
			category: 'syncMessage',
			message: 'syncMessage ',
			level: 'error'
		});
		setTimeout(() => {
			process.exit(507);
		}, 500);
		return;
	});
};
let sub_process = ()=>{
	let x=new Date();
	//console.log('sub_process-0',x.toLocaleTimeString());
	return Promise.all([gunlukislemler()])
		.catch(err => {
			Sentry.captureException(err);
			Sentry.addBreadcrumb({
				category: 'syncMessage',
				message: 'syncMessage ',
				level: 'error'
			});
			setTimeout(() => {
				process.exit(500);
			}, 500);
			return;
		});
};
let recordDelete = (_id,_messageId) => {
	let rep={
		id:_id
	};
	let _w='';
	if(_messageId){
		_w=' or "messageId"=:messageId';
		rep.messageId=_messageId;
	}
	let sql='DELETE from "_sync_messages" where ("id"=:id '+_w+')';
	return sequelize.query(sql, { replacements:rep, type: sequelize.QueryTypes.DELETE});
};

let gunlukislemler=()=>{
	let time=new timezoneJS.Date();
	let gun=time.toString('yyyy-MM-dd');
	let saat=time.toString('HH');
	let x=new Date();
	//console.log('gunlukislemler-0',x.toLocaleTimeString());
	if(_gun!==gun){
		_gun=gun;
		return;
	}else{
		if(_saat!==saat){
			_saat=saat;
		}
	}
	x=new Date();
};
let convertFullLocalDateString=(_time,_ms)=>{
	var dt = new timezoneJS.Date(_time);
	return dt.toString('yyyy-MM-dd HH:mm:ss'+(_ms==true?'.SSS':''));
};