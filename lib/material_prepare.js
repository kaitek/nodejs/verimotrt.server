const processModule = require('process');
const Sequelize = require('sequelize');
const timezoneJS = require('timezone-js');
const formidable = require('formidable');
const Promise = require('promise');
const fs=require('fs');
const http=require('http');
const path = require('path');
const url=require('url');
const util = require('util');
const { v4: uuidv4 } = require('uuid');
const Sentry = require('@sentry/node');
Sentry.init({
	dsn: 'https://4e61d0b3664d41c9a849f8edbb2b2255@sentry.kaitek.com.tr/3'
});
let sequelize;
let _io=false;
let _pid=false;
let _port=false;
let _ip=false;
let carrriers=[];
let _server=false;
// let _timerKill=false;
processModule.on('message', function (obj) {
	let event = obj.event;
	let param = obj.param;
	switch (event) {
	case 'init_material_prepare':{
		_pid=obj.pid;
		_port=obj.port;
		_ip=obj.ip;
		Sentry.captureMessage({
			message: 'init_material_prepare'
		});
		db_connect(param);
		break;
	}
	case 'data_imported_material_prepare':{
		try {
			if(fs.existsSync(obj.path)){
				fs.unlinkSync(obj.path);
			}
		} catch (error) {
			Sentry.captureException(error);
			log_error('err-data_imported_material_prepare',JSON.stringify(obj.data),'data_imported_material_prepare-1');
			setTimeout(() => {
				process.exit(625);
			}, 1000);
		}
		data_imported_material_prepare(obj.data);
		break;
	}
	default:{
		//console.log(obj);
		break;
	}
	}
});
processModule.on('uncaughtException', function(err){
	Sentry.captureException(err);
	setTimeout(() => {
		process.exit(600);
	}, 500);
	return;
});
let db_connect = (obj) => {
	sequelize = new Sequelize(obj.database, obj.username, obj.password, {
		host: obj.host,
		dialect: 'postgres',
		logging: false,
		pool: {
			max: 10,
			min: 0,
			acquire: 100000,
			idle: 10000
		},
		timezone:obj.timezone
	});
	return sequelize
		.authenticate()
		.then(() => {
			processModule.send({event:'cp_material_prepare_inited',ip:obj.ip,pid:_pid});
			setTimeout(() => {
				connectedDeviceControl();
			},60000);
			return run();
		})
		.catch(err => {
			Sentry.captureException(err);
			setTimeout(() => {
				process.exit(601);
			}, 500);
			return;
		});
};
let connectedDeviceControl=()=>{
	if(_ip==="172.16.1.143"||_ip==="172.16.1.144"){
		if(carrriers.length===0){
			// if(_timerKill===false){
			//	_timerKill=setTimeout(() => {
				// setTimeout(() => {
					//if(carrriers.length===0){
						Sentry.captureMessage({
							message: 'material_prepare_kill'
						});
						setTimeout(()=>{
							process.exit(630);
						},500);
					//}
				// }, 10000);
			// }
		}else{
			setTimeout(() => {
				connectedDeviceControl();
			},60000);
		}
	}else{
		return true;
	}
}
let run=()=>{
	function onRequest(req,res){
		try {
			let uri = url.parse(req.url).pathname;
			let x = uri.replace('/','').split('&');
			if(x.length>1){
				let obj={};
				for(let i=0;i<x.length;i++){
					let citem=x[i].split('=');
					obj[citem[0]]=citem[1];
				}
			}else{
				if (uri == '/'){
					uri = '/index.html';
				}
				if(req.url =='/crashreport/'&& req.method.toLowerCase() == 'post'){
					let form = new formidable.IncomingForm();
					form.encoding = 'utf-8';
					form.uploadDir = __dirname+'/../log';
					form.keepExtensions = true;
					form.parse(req, function(err, fields, files) {
						res.writeHead(200, {'content-type': 'text/plain'});
						res.write('received upload:\n\n');
						res.end(util.inspect({fields: fields, files: files}));
					});
					return;
				}else{
					let filename = __dirname+'/..'+unescape(uri);
					let stats;
					let extname = path.extname(filename);
					let contentType = 'text/html';
					switch (extname) {
					case '.asp':
					case '.aspx':
					case '.cc':
					case '.cfm':
					case '.cgi':
					case '.exe':
					case '.gdb':
					case '.htm':
					case '.ini':
					case '.jsp':
					case '.mdb':
					case '.php':
					case '.php3':
					case '.php4':
					case '.pl':
					case '.py':
					case '.rb':
					case '.sh':
					case '.tml':
					case '.wdm':
					case '.wdm.':
						res.writeHead(404, {'Content-Type': 'text/plain'});
						res.write('404 Not Found\n');
						res.end();
						return;
					case '.zip':
						contentType = 'application/zip';
						break;
					case '.js':
						contentType = 'text/javascript';
						break;
					case '.css':
						contentType = 'text/css';
						break;
					case '.txt':
						contentType = 'text/plain';
						break;
					}
					try {
						stats = fs.lstatSync(filename);
					} catch (e) {
						res.writeHead(404, {'Content-Type': 'text/plain'});
						res.write('404 Not Found\n');
						res.end();
						return;
					}
					if (stats.isFile()) {
						let fileStream = fs.createReadStream(filename);
						fileStream.on('error', function () {
							res.writeHead(404, { 'Content-Type': 'text/plain'});
							res.end('file not found');
						});
						fileStream.on('open', function() {
							res.writeHead(200, {'Content-Type': contentType});
							if(extname=='.zip'){
								res.writeHead(200,{'Content-Disposition': 'attachment; filename='+path.basename(uri)});
							}
						});
						fileStream.on('end', function() {
							//
						});
						fileStream.pipe(res);
					} else if (stats.isDirectory()) {
						res.writeHead(200, {'Content-Type': 'text/plain'});
						res.write('Index of '+uri+'\n');
						res.write('TODO, show index?\n');
						res.end();
					} else {
						res.writeHead(200, {'Content-Type': 'text/plain'});
						res.write('200 Internal server error\n');
						res.end();
					}
				}
			}
		} catch(e) {
			res.writeHead(200);
			res.end();     
			Sentry.captureException(e);
		}	
	}
	_server=http.createServer(onRequest);
	_io = require('socket.io')(_server,{
		cors: {
			origin: "http://"+_ip,
			methods: ["GET", "POST"]
		}
	});
	_io.on('connection', function (client){
		let clientIp = client.request.connection.remoteAddress.replace('::ffff','').replace(':','');
		client.versionApp=0;
		client.buildNumber=0;
		client.dcsClientId=0;
		client.ip=clientIp;
		//console.log('connection');
		let idx=getIndex(carrriers,'id',client.id);
		if(idx==-1){
			carrriers.push(client);
			//console.log(carrriers);
		}
		client.on('setUser',function(obj){
			//console.log('setUser');
			idx=getIndex(carrriers,'id',client.id);
			if(idx>-1){
				carrriers[idx].dcsClientId=obj.id;
				carrriers[idx].code=obj.carrier;
			}
			//console.log(carrriers);
			return sequelize.query('UPDATE carriers set connected=:connected,ip=:ip where id=:id', { replacements:{connected:true,ip:client.ip,id:obj.id}, type: sequelize.QueryTypes.UPDATE});
		});
		client.on('error', function(error){
			Sentry.captureException(error);
			setTimeout(() => {
				process.exit(622);
			}, 500);
		});
		client.on('disconnect', function(){
			//console.log('disconnect');
			let idx=getIndex(carrriers,'id',client.id);
			if(idx>-1){
				carrriers.splice(idx,1);
				//console.log(carrriers);
			}
			return sequelize.query('UPDATE carriers set connected=:connected where ip=:ip', { replacements:{connected:false,ip:client.ip}, type: sequelize.QueryTypes.UPDATE});
		});
		client.on('cn_sn',function(obj){
			let event=obj.event;
			let _client;
			switch(event){
			case 'connect':
				//console.log('connect');
				return sequelize.query('select * from carriers where code=:code',{replacements:{code:obj.carrier}, type: sequelize.QueryTypes.SELECT}).then((result)=>{
					if(result&&result.length>0){
						idx=getIndex(carrriers,'id',client.id);
						if(idx==-1){
							client.dcsClientId=result[0].id;
							client.code=obj.carrier;
							carrriers.push(client);
						}else{
							carrriers[idx].dcsClientId=result[0].id;
							carrriers[idx].code=obj.carrier;
						}
						//console.log(carrriers);
						// if(_timerKill!==false){
						// 	if(carrriers.length!==0){
						// 		clearTimeout(_timerKill);
						// 		_timerKill=false;
						// 	}
						// }
					}else{
						client.emit('sn_cn',{event:'hata',status:'err',message:'Cihaz tanımlı değil.'});
						return;
					}
					if(obj.ip){
						return sequelize.query('UPDATE carriers set connected=:connected,ip=:ip where code=:code', { replacements:{connected:true,ip:obj.ip,code:obj.carrier}, type: sequelize.QueryTypes.UPDATE}).then(function(){
							client.emit('sn_cn',{event:'connect',id:client.dcsClientId,serverIp:_ip,rtc:getFullLocalDateString()});
							return null;
						});
					}else{
						client.emit('sn_cn',{event:'connect',id:client.dcsClientId,serverIp:_ip,rtc:getFullLocalDateString()});
						return null;
					}
				}).catch(err => {
					Sentry.captureException(err);
					return;
				});
			case 'islemzamanasimi':{
				islemzamanasimi2(client,obj.carrier);
				break;
			}
			case 'oturumkapandi':{
				client.emit('sn_cn',{event:'oturumkapandi',status:'ok',message:'oturumkapandi'});
				break;
			}
			case 'oturumkontrol':{
				oturumkontrol2(client,obj.carrier);
				break;
			}
			case 'tasimagorevilistele0':{
				tasimagorevilistele0(client,obj.id);
				break;
			}
			case 'tasimagorevilistele':{
				tasimagorevilistele2(client,obj.carrier);
				break;
			}
			case 'ping':{
				client.emit('forklift_ping',{status:'ok',message:''});
				for(let i=0;i<carrriers.length;i++){
					_client=carrriers[i];
					if(_client.ip==client.ip){
						_client.emit('sn_cn',{event:'pong',serverIp:_ip,rtc:getFullLocalDateString()});
					}
				}
				break;
			}
			case 'tasimagorevikapat':{
				data_imported_material_prepare({material_prepare:true,methodName:'tasimagorevikapat',id:obj.id,sicil:obj.sicil,forklift:obj.forklift,lokasyonvaris:obj.lokasyonvaris,bilesen:obj.bilesen,miktar:obj.miktar});
				break;
			}
			case 'time_sync':{
				for(let i=0;i<carrriers.length;i++){
					_client=carrriers[i];
					if(_client.ip==client.ip){
						const _d_=getFullLocalDateString();
						const _arr_t_=_d_.split('.');
						const _t_=_arr_t_[0];
						_client.emit('sn_cn',{event:'response|time_sync',serverIp:_ip,rtc:_d_,status:'ok',time:_t_,timems:_d_});
					}
				}
				break;
			}
			}
		});
	});
	_server.timeout = 20000;
	_server.listen(_port);
	processModule.send({event:'started_material_prepare',pid:_pid,port:_port});
};
let islemzamanasimi=(client,id)=>{
	let sql='UPDATE carrier_sessions cs SET finish = CURRENT_TIMESTAMP FROM carriers cr WHERE cr.code=cs.carrier AND cs.finish is null and cr.id=:id';
	return sequelize.query(sql, { replacements:{id:id}, type: sequelize.QueryTypes.UPDATE}).then(()=>{
		client.emit('forklift_islemzamanasimi',{status:'ok',message:'islemzamanasimi'});
	});
};
let islemzamanasimi2=(client,carrier)=>{
	let sql='UPDATE carrier_sessions cs SET finish = CURRENT_TIMESTAMP FROM carriers cr WHERE cr.code=cs.carrier AND cs.finish is null and cr.code=:carrier';
	return sequelize.query(sql, { replacements:{carrier:carrier}, type: sequelize.QueryTypes.UPDATE}).then(()=>{
		client.emit('sn_cn',{event:'islemzamanasimi',status:'ok',message:'islemzamanasimi'});
	});
};
let oturumkontrol=(client,id)=>{
	let sql='SELECT cs.* from carriers c join carrier_sessions cs on cs.carrier=c.code where cs.finish is null and c.id=:id';
	return sequelize.query(sql, { replacements:{id:id}, type: sequelize.QueryTypes.SELECT}).then((result)=>{
		if(result&&result.length>0){
			client.emit('forklift_oturumkontrol',{status:'ok',message:'oturumvar',sicil:result[0].empcode,'isim':result[0].empname});
		}else{
			client.emit('forklift_oturumkontrol',{status:'err',message:'oturumyok',sicil:0,'isim':''});
		}
	});
};
let oturumkontrol2=(client,carrier)=>{
	let sql='SELECT cs.* from carriers c join carrier_sessions cs on cs.carrier=c.code where cs.finish is null and c.code=:carrier';
	return sequelize.query(sql, { replacements:{carrier:carrier}, type: sequelize.QueryTypes.SELECT}).then((result)=>{
		if(result&&result.length>0){
			client.emit('sn_cn',{event:'oturumkontrol',status:'ok',message:'oturumvar',sicil:result[0].empcode,'isim':result[0].empname});
		}else{
			client.emit('sn_cn',{event:'oturumkontrol',status:'err',message:'oturumyok',sicil:0,'isim':''});
		}
	});
};
let tasimagorevilistele0=(client,id)=>{
	let sql='SELECT cm.*,to_char(cm.starttime, \'HH24:MI\') as cagrizamani '+
	'	,COALESCE(mtt.label,cm.tasktype)islemtip,case when cm.tasktype in (\'GRV001\',\'GRV011\') then cm.casename else cm.stockcode end bilesen2  '+
	'	,TO_CHAR(( DATE_PART(\'day\', cm.targettime::timestamp - current_timestamp::timestamp) * 24*3600 '+
	'				+ DATE_PART(\'hour\', cm.targettime::timestamp - current_timestamp::timestamp) * 3600  '+
	'				+ DATE_PART(\'minute\', cm.targettime::timestamp - current_timestamp::timestamp)*60  '+
	'				+ DATE_PART(\'second\', cm.targettime::timestamp - current_timestamp::timestamp)|| \' second\')::interval, \'HH24:MI:SS\')kalansure '+
	' from case_movements cm '+
	' left join carriers cr on cr.code=cm.carrier '+
	' left join movement_task_types mtt on mtt.tasktype=cm.tasktype '+
	' where cr.id= ? and cm.actualfinishtime is null and status=\'WAITFORDELIVERY\' ' +
	' order by cm.targettime';
	return sequelize.query(sql, { replacements:[id], type: sequelize.QueryTypes.SELECT}).then((result)=>{
		client.emit('forklift_tasimagorevilistele',{length:result.length,records:result});
		return null;
	}).catch(err => {
		Sentry.captureException(err);
		Sentry.addBreadcrumb({
			category: 'material_prepare',
			message: 'tasimagorevilistele client:'+client+' id:'+id,
			level: 'error',
			data:sql
		});
		//setTimeout(() => {
		//	process.exit(620);
		//}, 500);
		return;
	});
};
let tasimagorevilistele2=(client,carrier)=>{
	let sql='SELECT cm.*,to_char(cm.starttime, \'HH24:MI\') as cagrizamani '+
	'	,COALESCE(mtt.label,cm.tasktype)islemtip,case when cm.tasktype in (\'GRV001\',\'GRV011\') then cm.casename else cm.stockcode end bilesen2  '+
	'	,TO_CHAR(( DATE_PART(\'day\', cm.targettime::timestamp - current_timestamp::timestamp) * 24*3600 '+
	'				+ DATE_PART(\'hour\', cm.targettime::timestamp - current_timestamp::timestamp) * 3600  '+
	'				+ DATE_PART(\'minute\', cm.targettime::timestamp - current_timestamp::timestamp)*60  '+
	'				+ DATE_PART(\'second\', cm.targettime::timestamp - current_timestamp::timestamp)|| \' second\')::interval, \'HH24:MI:SS\')kalansure '+
	'	,cast(extract(epoch from (cm.targettime::timestamp-CURRENT_TIMESTAMP(0))) as integer)kalansuresn,cm.id _id_ '+
	' from case_movements cm '+
	' left join carriers cr on cr.code=cm.carrier '+
	' left join movement_task_types mtt on mtt.tasktype=cm.tasktype '+
	' where cr.code= ? and cm.actualfinishtime is null and status=\'WAITFORDELIVERY\' ' +
	' order by case when cm.tasktype=\'GRV001\' then 1 else 0 end,cm.targettime';
	return sequelize.query(sql, { replacements:[carrier], type: sequelize.QueryTypes.SELECT}).then((result)=>{
		client.emit('sn_cn',{event:'tasimagorevilistele',length:result.length,records:result});
		return null;
	}).catch(err => {
		Sentry.captureException(err);
		Sentry.addBreadcrumb({
			category: 'material_prepare',
			message: 'tasimagorevilistele2 client:'+client+' carrier:'+carrier,
			level: 'error',
			data:sql
		});
		//setTimeout(() => {
		//	process.exit(620);
		//}, 500);
		return;
	});
};
let simpleJSONstringify=(obj)=>{
	var prop, str, val,
		isArray = obj instanceof Array;

	if (typeof obj !== "object") return false;

	str = isArray ? "[" : "{";

	function quote(str) {
		if (typeof str !== "string") str = str.toString();
		return str.match(/^\".*\"$/) ? str : '"' + str.replace(/"/g, '\\"') + '"'
	}

	for (prop in obj) {
		if (!isArray) {
			// quote property
			str += quote(prop) + ": ";
		}

		// quote value
		val = obj[prop];
		str += typeof val === "object" ? simpleJSONstringify(val) : quote(val);
		str += ", ";
	}

	// Remove last colon, close bracket
	str = str.substr(0, str.length - 2)  + ( isArray ? "]" : "}" );

	return str;
};
let getAllClients=()=>{
	let _x_={};
	for(let i=0;i<carrriers.length;i++){
		_x_[carrriers[i].ip]={ip:carrriers[i].ip,code:carrriers[i].code};
	}
	return JSON.stringify(_x_);
};
let insert_sync_message=(client,type,data)=>{
	let sql_sm='insert into _sync_messages (ip,uuid,type,data,queued,"createdAt","updatedAt") '+
	'values (:ip,:uuid,:type,:data,0,CURRENT_TIMESTAMP(0),CURRENT_TIMESTAMP(0))';
	return sequelize.query(sql_sm, { replacements:{ip:client+'_server',uuid:null,type:type,data:JSON.stringify(data)}, type: sequelize.QueryTypes.INSERT});
};
let data_imported_material_prepare=(data)=>{
	try {
		log_error('data_import',JSON.stringify(data),'import');
		switch (data.methodName) {
			case 'cihazdegistir':{
				let idx_e=getIndex(carrriers,'dcsClientId',data.eski);
				let sql='SELECT * from carriers where id=:eski';
				return sequelize.query(sql, { replacements:{eski:data.eski}, type: sequelize.QueryTypes.SELECT}).then((result_c_e)=>{
					if(result_c_e&&result_c_e.length>0){
						sql='SELECT * from carriers where id=:yeni and (code=\'\' or code is null)';
						return sequelize.query(sql, { replacements:{yeni:data.yeni}, type: sequelize.QueryTypes.SELECT}).then((result_c_y)=>{
							if(result_c_y&&result_c_y.length>0){
								sql='update carriers set code=:code where id=:yeni';
								return sequelize.query(sql, { replacements:{code:result_c_e[0].code,yeni:data.yeni}, type: sequelize.QueryTypes.UPDATE}).then(()=>{
									sql='update carriers set code=\'\' where id=:eski';
									return sequelize.query(sql, { replacements:{eski:data.eski}, type: sequelize.QueryTypes.UPDATE}).then(()=>{
										if(idx_e>-1){
											for(let i=0;i<carrriers.length;i++){
												if(carrriers[i]&&carrriers[i].dcsClientId==data.eski){
													carrriers[i].emit('dcs_changestation',{status:'ok',message:'Cihaz değiştirildi.'});
												}
											}
											carrriers[idx_e].dcsClientId=data.yeni;
										}
										let idx_y=getIndex(carrriers,'dcsClientId',data.yeni);
										if(idx_y>-1){
											for(let i=0;i<carrriers.length;i++){
												if(carrriers[i]&&carrriers[i].dcsClientId==data.yeni){
													carrriers[i].emit('dcs_pagerefresh',{status:'ok',message:'Cihaz değiştirildi.'});
												}
											}
										}
										return null;
									});
								});
							}else{
								if(idx_e>-1){
									for(let i=0;i<carrriers.length;i++){
										if(carrriers[i]&&carrriers[i].dcsClientId==data.eski){
											carrriers[i].emit('forklift_hata',{status:'err',message:'Yeni cihaz boş değil.'});
											carrriers[i].emit('sn_cn',{event:'hata',status:'err',message:'Yeni cihaz boş değil.'});
										}
									}
								}
								return null;
							}
						});
					}else{
						if(idx_e>-1){
							for(let i=0;i<carrriers.length;i++){
								if(carrriers[i]&&carrriers[i].dcsClientId==data.eski){
									carrriers[i].emit('forklift_hata',{status:'err',message:'Eski cihaz bulunamadı.'});
									carrriers[i].emit('sn_cn',{event:'hata',status:'err',message:'Eski cihaz bulunamadı.'});
								}
							}
						}
						return null;
					}
				});
			}
			case 'etiketyaz':{
				let sql='SELECT * from case_labels where status=\'OPEN\' and erprefnumber=:erprefnumber';
				return sequelize.query(sql, { replacements:{erprefnumber:data.erprefnumber}, type: sequelize.QueryTypes.SELECT}).then((result_c_l)=>{
					if(result_c_l&&result_c_l.length==0){
						sql='insert into case_labels (erprefnumber,time,status,synced,record_id,create_user_id,created_at) values (:erprefnumber,CURRENT_TIMESTAMP,\'OPEN\',0,:record_id,0,CURRENT_TIMESTAMP)';
						return sequelize.query(sql, { replacements:{erprefnumber:data.erprefnumber,record_id:uuidv4()}, type: sequelize.QueryTypes.INSERT});
					}
				});
			}
			case 'etiketsil':{
				let _erpref=data.erprefnumber;
				let sql='SELECT * from case_labels where erprefnumber=:erprefnumber';
				return sequelize.query(sql, { replacements:{erprefnumber:_erpref}, type: sequelize.QueryTypes.SELECT}).then((result_c_l)=>{
					if(result_c_l&&result_c_l.length>0){
						sql='SELECT * from case_movements where erprefnumber=:erprefnumber';
						return sequelize.query(sql, { replacements:{erprefnumber:_erpref}, type: sequelize.QueryTypes.SELECT}).then((result_c_m)=>{
							if(result_c_m&&result_c_m.length>0){
								//taskcase tablosundan input kasadan eksilme veya output kasa kasa içi miktar değişmişse iptal edemesin
								sql='SELECT * from task_cases where movementdetail is not null and erprefnumber=:erprefnumber and ((casetype=\'O\' and quantityremaining>0) or (casetype=\'I\' and packcapacity<>quantityremaining))';
								return sequelize.query(sql, { replacements:{erprefnumber:_erpref}, type: sequelize.QueryTypes.SELECT}).then((result_t_c)=>{
									if(result_t_c&&result_t_c.length>0){
										//hareket yapılmış,işlem yapılamaz
										return null;
									}else{
										log_error('etiketsil','','100');
										sql='select id,filepath from case_movements where status in (\'WAITFORASSIGNMENT\',\'WAITFORDELIVERY\') and actualfinishtime is null and erprefnumber=:erprefnumber';
										return sequelize.query(sql, { replacements:{erprefnumber:_erpref}, type: sequelize.QueryTypes.SELECT}).then((result_c_m_1)=>{
											if(result_c_m_1&&result_c_m_1.length>0){
												for(let i=0;i<result_c_m_1.length;i++){
													if(fs.existsSync(result_c_m_1[i].filepath)){
														fs.unlinkSync(result_c_m_1[i].filepath);
														log_error('file_deleted',result_c_m_1[i].filepath,'etiketsil-1');
													}else{
														log_error('file_not_deleted',result_c_m_1[i].filepath,'etiketsil-2');
													}
												}
											}
											return;
										}).then(()=>{
											log_error('etiketsil','','101');
											//hareket işlemi tamamlanmamış,atama yapılma durumu kontrol edilecek
											sql='SELECT c.id,cm.carrier from case_movements cm left join carriers c on c.code=cm.carrier where cm.erprefnumber=:erprefnumber and cm.actualfinishtime is null and cm.status in (\'WAITFORASSIGNMENT\',\'WAITFORDELIVERY\') and cm.assignmenttime is not null GROUP BY c.id,cm.carrier';
											return sequelize.query(sql, { replacements:{erprefnumber:_erpref}, type: sequelize.QueryTypes.SELECT}).then((result_c_m_2)=>{
												if(result_c_m_2&&result_c_m_2.length>0){
													log_error('etiketsil','','102');
													for(let i=0;i<result_c_m_2.length;i++){
														log_error('etiketsil',result_c_m_2[i].carrier,'104');
														let idx=getIndex(carrriers,'code',result_c_m_2[i].carrier);
														if(idx>-1){
															let _promises=[];
															for(let j=0;j<carrriers.length;j++){
																if(carrriers[j]&&carrriers[j].code==result_c_m_2[i].carrier){
																	_promises.push(tasimagorevilistele2(carrriers[j],result_c_m_2[i].carrier));
																}
															}
															return Promise.all(_promises).catch(err=>{
																Sentry.addBreadcrumb({
																	category: 'material_prepare',
																	message: 'etiketsil 104-2 ',
																	level: 'error',
																	data:JSON.stringify(data)
																});
																Sentry.captureException(err);
															});
														}
													}
												}
												return null;
											}).then(()=>{
												log_error('etiketsil','','109');
												sql='update case_movement_details cmd set canceltime=CURRENT_TIMESTAMP,description=\'Etiket Silinmesi\' from case_movements cm where cmd.casemovementid=cm.id and cm.erprefnumber=:erprefnumber and cm.actualfinishtime is null and cm.assignmenttime is not null';
												return sequelize.query(sql, { replacements:{erprefnumber:_erpref}, type: sequelize.QueryTypes.UPDATE})
												.catch(err=>{
													Sentry.addBreadcrumb({
														category: 'material_prepare',
														message: 'etiketsil 109 ',
														level: 'error',
														data:JSON.stringify(data)
													});
													Sentry.captureException(err);
												});
											}).then(()=>{
												log_error('etiketsil','','110');
												sql='update case_movements set actualfinishtime=CURRENT_TIMESTAMP,status=\'LABELDELETED\',carrier=null,empcode=null,empname=null where erprefnumber=:erprefnumber and actualfinishtime is null';
												return sequelize.query(sql, { replacements:{erprefnumber:_erpref}, type: sequelize.QueryTypes.UPDATE})
												.catch(err=>{
													Sentry.addBreadcrumb({
														category: 'material_prepare',
														message: 'etiketsil 110 ',
														level: 'error',
														data:JSON.stringify(data)
													});
													Sentry.captureException(err);
												});
											}).then(()=>{
												log_error('etiketsil','','120');
												sql='update task_cases set status=\'LABELDELETED\' where movementdetail is not null and erprefnumber=:erprefnumber and status in (\'WAIT\',\'WORK\')';
												return sequelize.query(sql, { replacements:{erprefnumber:_erpref}, type: sequelize.QueryTypes.UPDATE}).then(()=>{
													log_error('etiketsil','','121');
													sql='delete from case_labels where erprefnumber=:erprefnumber';
													return sequelize.query(sql, { replacements:{erprefnumber:_erpref}, type: sequelize.QueryTypes.DELETE}).catch(err=>{
														Sentry.addBreadcrumb({
															category: 'material_prepare',
															message: 'etiketsil 121 ',
															level: 'error',
															data:JSON.stringify(data)
														});
														Sentry.captureException(err);
													});
												}).catch(err=>{
													Sentry.addBreadcrumb({
														category: 'material_prepare',
														message: 'etiketsil 120 ',
														level: 'error',
														data:JSON.stringify(data)
													});
													Sentry.captureException(err);
												});
											}).catch(err=>{
												Sentry.addBreadcrumb({
													category: 'material_prepare',
													message: 'etiketsil 101- ',
													level: 'error',
													data:JSON.stringify(data)
												});
												Sentry.captureException(err);
											});
										}).catch(err=>{
											Sentry.addBreadcrumb({
												category: 'material_prepare',
												message: 'etiketsil 101 ',
												level: 'error',
												data:JSON.stringify(data)
											});
											Sentry.captureException(err);
										});
									}
								});
							}else{
								//kasahareket kaydı yok, etiket kaydını kapat
								log_error('etiketsil','','130');
								sql='delete from case_labels where erprefnumber=:erprefnumber';
								return sequelize.query(sql, { replacements:{erprefnumber:_erpref}, type: sequelize.QueryTypes.DELETE}).catch(err=>{
									Sentry.addBreadcrumb({
										category: 'material_prepare',
										message: 'etiketsil 130 ',
										level: 'error',
										data:JSON.stringify(data)
									});
									Sentry.captureException(err);
								});
							}
						}).then(()=>{
							log_error('etiketsil','','140');
							return sequelize.query('select code from clients where uuid is not null and isactive=true and ip<>\'\' and ip is not null',{type: sequelize.QueryTypes.SELECT}).then((clients)=>{
								let _c=clients;
								let _promises=[];
								log_error('etiketsil','','141');
								for(let j=0;j<_c.length;j++){
									let data={
										tableName:'case_labels'
										,erprefnumber:_erpref
										,status:'LABELDELETED'
									};
									_promises.push(insert_sync_message(_c[j].code,'mh_label_deleted',data));
								}
								return Promise.all(_promises).catch(err=>{
									Sentry.addBreadcrumb({
										category: 'material_prepare',
										message: 'etiketsil 141 ',
										level: 'error',
										data:JSON.stringify(data)
									});
									Sentry.captureException(err);
								});
							}).catch(err=>{
								Sentry.addBreadcrumb({
									category: 'material_prepare',
									message: 'etiketsil 140 ',
									level: 'error',
									data:JSON.stringify(data)
								});
								Sentry.captureException(err);
							});
						}).catch(err=>{
							Sentry.addBreadcrumb({
								category: 'material_prepare',
								message: 'etiketsil 100 ',
								level: 'error',
								data:JSON.stringify(data)
							});
							Sentry.captureException(err);
						});
					}
				}).catch(err=>{
					Sentry.addBreadcrumb({
						category: 'material_prepare',
						message: 'etiketsil ',
						level: 'error',
						data:JSON.stringify(data)
					});
					Sentry.captureException(err);
				});
			}
			case 'oturumac':{
				let _carrrier='';
				let idx=getIndex(carrriers,'dcsClientId',data.id);
				if(idx>-1){
					let close_case_movements=(id,client,assignmenttime)=>{
						let breaktimeseconds=0;
						let sql_breaktime='select sum(COALESCE(DATE_PART(\'day\', cld_1.finish::timestamp - cld_1.start::timestamp) * 24*3600 '+
						'			+ DATE_PART(\'hour\', cld_1.finish::timestamp - cld_1.start::timestamp) * 3600  '+
						'			+ DATE_PART(\'minute\', cld_1.finish::timestamp - cld_1.start::timestamp)*60  '+
						'			+ DATE_PART(\'second\', cld_1.finish::timestamp - cld_1.start::timestamp),0) ) breaktimeseconds '+
						' from ( '+
						'	SELECT case when cld.start>:assignmenttime then cld.start else :assignmenttime end as start '+
						'		,coalesce(cld.finish,CURRENT_TIMESTAMP(0)) finish '+
						'	from client_lost_details cld '+
						' join ( SELECT id,code,beginval,endval ,current_date,current_time '+
						'  		,cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL \'1 day\' else current_date end as varchar(10)) "day"  '+
						'  		,concat(cast(case when beginval>endval and cast(current_time as varchar(5))<=endval then current_date - INTERVAL \'1 day\' else current_date end as varchar(10)),\' \',beginval,\':00\')::timestamp jr_start  '+
						'  		,concat(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL \'1 day\' else current_date end as varchar(10)),\' \',endval,\':59\')::timestamp +interval \'1 second\' jr_end  '+
						'		from job_rotations 	 '+
						'		where (finish is null or finish<now())	 '+
						'			and (	 '+
						'				(endval>beginval and cast(current_time as varchar(5)) between beginval and endval)	 '+
						'				or  '+
						'				(endval<beginval and (cast(current_time as varchar(5))>=beginval or cast(current_time as varchar(5))<=endval) )	 '+
						'			)) sv on cast(sv.day as date)=cld.day '+
						'	where cld.type=\'l_c\' and cld.losttype in (\'CUMA PAYDOSU\',\'ÇAY MOLASI\',\'YEMEK MOLASI\') and cld.client=:client '+
						'	and (CURRENT_TIMESTAMP(0) between cld.start and coalesce(cld.finish,CURRENT_TIMESTAMP(0)) '+
						'		or :assignmenttime between cld.start and coalesce(cld.finish,CURRENT_TIMESTAMP(0)) '+
						'		or (:assignmenttime<=cld.start and CURRENT_TIMESTAMP(0)>=coalesce(cld.finish,CURRENT_TIMESTAMP(0))) '+
						'	) '+
						')cld_1';
						return sequelize.query(sql_breaktime, { replacements:{assignmenttime:assignmenttime,client:client}, type: sequelize.QueryTypes.SELECT}).then(function(result_breaktime){
							if(result_breaktime&&result_breaktime.length>0){
								breaktimeseconds=result_breaktime[0]['breaktimeseconds'];
							}
							let sql='update case_movements set actualfinishtime=CURRENT_TIMESTAMP(0),status=\'LOGOUT\',breaktimeseconds=:breaktimeseconds where id=:id';
							return sequelize.query(sql, { replacements:{breaktimeseconds:breaktimeseconds,id:id}, type: sequelize.QueryTypes.UPDATE}).catch(err => {
								Sentry.captureException(err);
								//console.log(err);
								setTimeout(() => {
									process.exit(603);
								}, 500);
								return;
							});
						}).catch(err => {
							Sentry.captureException(err);
							//console.log(err);
							setTimeout(() => {
								process.exit(604);
							}, 500);
							return;
						});
					};
					let close_case_movement_details=(id,description)=>{
						let sql='update case_movement_details set canceltime=CURRENT_TIMESTAMP(0),description=:description where id=:id and donetime is null and canceltime is null';
						return sequelize.query(sql, { replacements:{id:id,description:description}, type: sequelize.QueryTypes.UPDATE}).catch(err => {
							Sentry.captureException(err);
							//console.log(err);
							setTimeout(() => {
								process.exit(605);
							}, 500);
							return;
						});
					};
					let copy_case_movements=(copydata,empcode,empname)=>{
						let sql='insert into case_movements ( '+
						'	carrier,code,stockcode,number,casename '+
						'	,locationsource,locationdestination,tasktype '+
						'	,starttime,targettime,assignmenttime,status '+
						'	,client,erprefnumber,quantitynecessary,quantityready '+
						'	,locationsourceinfo,locationdestinationinfo,filepath '+
						'	,position,carrierfeeder,carrierdrainer '+
						'	,empcode,empname) '+
						' values ( '+
						'	:carrier,:code,:stockcode,:number,:casename '+
						'	,:locationsource,:locationdestination,:tasktype '+
						'	,:starttime,:targettime,CURRENT_TIMESTAMP(0),:status '+
						'	,:client,:erprefnumber,:quantitynecessary,:quantityready '+
						'	,:locationsourceinfo,:locationdestinationinfo,:filepath '+
						'	,:position,:carrierfeeder,:carrierdrainer '+
						'	,:empcode,:empname)';
						return sequelize.query(sql, { replacements:{
							carrier:copydata.carrier
							,code:copydata.code
							,stockcode:copydata.stockcode
							,number:copydata.number
							,casename:copydata.casename
							,locationsource:copydata.locationsource
							,locationdestination:copydata.locationdestination
							,tasktype:copydata.tasktype
							,starttime:copydata.starttime
							,targettime:copydata.targettime
							,status:copydata.status
							,client:copydata.client
							,erprefnumber:copydata.erprefnumber
							,quantitynecessary:copydata.quantitynecessary
							,quantityready:copydata.quantityready
							,locationsourceinfo:copydata.locationsourceinfo
							,locationdestinationinfo:copydata.locationdestinationinfo
							,filepath:copydata.filepath
							,position:copydata.position
							,carrierfeeder:copydata.carrierfeeder
							,carrierdrainer:copydata.carrierdrainer
							,empcode:empcode
							,empname:empname
						}, type: sequelize.QueryTypes.INSERT})
							.then(()=>{
								return sequelize.query('select * from case_movements '+
								' where carrier=:carrier and code=:code and stockcode=:stockcode and number=:number '+
								' and casename=:casename and locationdestination=:locationdestination and tasktype=:tasktype '+
								' and starttime=:starttime and status=:status and empcode=:empcode'
								, {
									replacements:{
										carrier:copydata.carrier
										,code:copydata.code
										,stockcode:copydata.stockcode
										,number:copydata.number
										,casename:copydata.casename
										,locationdestination:copydata.locationdestination
										,tasktype:copydata.tasktype
										,starttime:copydata.starttime
										,status:copydata.status
										,empcode:empcode
									}, type: sequelize.QueryTypes.SELECT}
								).then((res_cm_copy_1)=>{
									if(res_cm_copy_1&&res_cm_copy_1.length>0){
										let sql='insert into case_movement_details (casemovementid,carrier,assignmenttime,empcode,empname) '+
										' values (:casemovementid,:carrier,:assignmenttime,:empcode,:empname)';
										return sequelize.query(sql, { replacements:{
											casemovementid:res_cm_copy_1[0].id
											,carrier:res_cm_copy_1[0].carrier
											,assignmenttime:res_cm_copy_1[0].assignmenttime
											,empcode:res_cm_copy_1[0].empcode
											,empname:res_cm_copy_1[0].empname
										}, type: sequelize.QueryTypes.INSERT}).catch(err => {
											Sentry.captureException(err);
											Sentry.addBreadcrumb({
												category: 'material_prepare',
												message: 'oturumac 7 ',
												level: 'error',
												data:sql
											});
											setTimeout(() => {
												process.exit(606);
											}, 500);
											return;
										});
									}else{
										return null;
									}
								}).catch(err => {
									Sentry.captureException(err);
									Sentry.addBreadcrumb({
										category: 'material_prepare',
										message: 'oturumac 6 ',
										level: 'error',
										data:sql
									});
									setTimeout(() => {
										process.exit(607);
									}, 500);
									return;
								});
							}).catch(err => {
								Sentry.captureException(err);
								Sentry.addBreadcrumb({
									category: 'material_prepare',
									message: 'oturumac 5 ',
									level: 'error',
									data:copydata
								});
								setTimeout(() => {
									process.exit(608);
								}, 500);
								return;
							});
					};
					let sql='SELECT * from carriers c where c.id=:id and code is not null and code<>\'\'';
					return sequelize.query(sql, { replacements:{id:data.id}, type: sequelize.QueryTypes.SELECT}).then((result_c)=>{
						if(result_c&&result_c.length>0){
							_carrrier=result_c[0].code;
							sql='SELECT * from carrier_sessions where finish is null and carrier=:carrier and empcode=:empcode';
							return sequelize.query(sql, { replacements:{carrier:_carrrier,empcode:data.sicil}, type: sequelize.QueryTypes.SELECT}).then((result_cs1)=>{
								if(result_cs1&&result_cs1.length>0){
									//açık oturum var
									//kendi oturumu 
									sql='UPDATE case_movements set empcode=:empcode,empname=:empname where carrier=:carrier and actualfinishtime is null and status=:status and empcode<>:empcode';
									return sequelize.query(sql, { replacements:{empcode:data.sicil,empname:data.isim,carrier:_carrrier,status:'WAITFORDELIVERY'}, type: sequelize.QueryTypes.UPDATE}).then(()=>{
										for(let i=0;i<carrriers.length;i++){
											if(carrriers[i]&&carrriers[i].code==_carrrier){
												carrriers[i].emit('forklift_oturumacildi',{status:'err',message:'Oturum açık.'});
												carrriers[i].emit('sn_cn',{event:'oturumacildi',status:'err',message:'Oturum açık.'});
											}
										}
										return null;
									}).catch(err => {
										Sentry.captureException(err);
										Sentry.addBreadcrumb({
											category: 'material_prepare',
											message: 'oturumac 2 ',
											level: 'error',
											data:sql
										});
										setTimeout(() => {
											process.exit(602);
										}, 500);
										return;
									});
								}else{
									//sicilin başka cihazda açık oturumu var mı?
									sql='SELECT * from carrier_sessions where finish is null and carrier<>:carrier and empcode=:empcode';
									return sequelize.query(sql, { replacements:{carrier:_carrrier,empcode:data.sicil}, type: sequelize.QueryTypes.SELECT}).then((result_cs2)=>{
										if(result_cs2&&result_cs2.length>0){
											//sicilin başka cihazda oturumu var
											for(let i=0;i<carrriers.length;i++){
												if(carrriers[i]&&carrriers[i].code==_carrrier){
													carrriers[i].emit('forklift_hata',{status:'err',message:'Sicilin açık oturumu var.'});
													carrriers[i].emit('sn_cn',{event:'hata',status:'err',message:'Sicilin açık oturumu var.'});
												}
											}
											return null;
										}else{
											//taşıyıcıda başka açık oturum var mı?
											sql='SELECT * from carrier_sessions where finish is null and carrier=:carrier and empcode<>:empcode';
											return sequelize.query(sql, { replacements:{carrier:_carrrier,empcode:data.sicil}, type: sequelize.QueryTypes.SELECT})
											.then((result_cs3)=>{
												sql='SELECT id,code,beginval,endval '+
												' 	,cast(case when beginval>endval and cast(current_time+interval \'3 minutes\' as varchar(5))>endval then current_date + INTERVAL \'1 day\' else current_date end as varchar(10)) "day"  '+
												' 	,concat(cast(case when beginval>endval and cast(current_time+interval \'3 minutes\' as varchar(5))<=endval then current_date - INTERVAL \'1 day\' else current_date end as varchar(10)),\' \',beginval,\':00\')::timestamp jr_start '+
												' 	,concat(cast(case when beginval>endval and cast(current_time+interval \'3 minutes\' as varchar(5))>endval then current_date + INTERVAL \'1 day\' else current_date end as varchar(10)),\' \',endval,\':59\')::timestamp jr_end '+
												' from job_rotations  '+
												' where (finish is null or finish<now()) '+
												'	and ( '+
												'	(endval>beginval and cast(current_time+interval \'3 minutes\' as varchar(5)) between beginval and endval) '+
												'	or (endval<beginval and (cast(current_time+interval \'3 minutes\' as varchar(5))>=beginval or cast(current_time+interval \'3 minutes\' as varchar(5))<=endval) ) '+
												' )';
												return sequelize.query(sql, { type: sequelize.QueryTypes.SELECT}).then((result_sv_1)=>{
													let _day=null;
													let _jr=null;
													if(result_sv_1&&result_sv_1.length>0){
														_day=result_sv_1[0].day;
														_jr=result_sv_1[0].code;
													}
													sql='insert into carrier_sessions (carrier,empcode,empname,start,day,jobrotation) values (:carrier,:empcode,:empname,CURRENT_TIMESTAMP,:day,:jobrotation)';
													return sequelize.query(sql, { replacements:{carrier:_carrrier,empcode:data.sicil,empname:data.isim,day:_day,jobrotation:_jr}, type: sequelize.QueryTypes.INSERT})
														.then(()=>{
															sql='select cm.*,cmd.id cmd_id '+
															' from case_movements cm  '+
															' join case_movement_details cmd on cmd.casemovementid=cm.id and cmd.carrier=cm.carrier and cmd.canceltime is null and cmd.donetime is null '+
															' where cm.actualfinishtime is null and cm.status=:status and cmd.carrier=:carrier';
															return sequelize.query(sql, { replacements:{carrier:_carrrier,status:'WAITFORDELIVERY'}, type: sequelize.QueryTypes.SELECT})
																.then((result_cm_3)=>{
																	if(result_cm_3&&result_cm_3.length>0){
																		let _promises=[];
																		for(let i=0;i<result_cm_3.length;i++){
																			let row=result_cm_3[i];
																			_promises.push(close_case_movements(row.id,row.client,row.assignmenttime));
																			_promises.push(close_case_movement_details(row.cmd_id,(result_cs3&&result_cs3.length>0?'oturum değişimi':'oturum açma')));
																			_promises.push(copy_case_movements(row,data.sicil,data.isim));
																		}
																		return Promise.all(_promises).catch(err => {
																			Sentry.captureException(err);
																			//console.log(err);
																			setTimeout(() => {
																				process.exit(609);
																			}, 500);
																			return;
																		});
																	}else{
																		return null;
																	}
																}).then(()=>{
																	if(result_cs3&&result_cs3.length>0){
																		sql='UPDATE carrier_sessions set finish=CURRENT_TIMESTAMP where carrier=:carrier and finish is null and empcode<>:empcode';
																		return sequelize.query(sql, { replacements:{carrier:_carrrier,empcode:data.sicil}, type: sequelize.QueryTypes.UPDATE}).catch(err => {
																			Sentry.captureException(err);
																			Sentry.addBreadcrumb({
																				category: 'material_prepare',
																				message: 'oturumac 10 ',
																				level: 'error',
																				data:sql
																			});
																			setTimeout(() => {
																				process.exit(610);
																			}, 500);
																			return;
																		});
																	}else{
																		return null;
																	}
																}).then(()=>{
																	for(let i=0;i<carrriers.length;i++){
																		if(carrriers[i]&&carrriers[i].code==_carrrier){
																			carrriers[i].emit('forklift_oturumacildi',{status:'ok',message:'Oturum açılıyor'});
																			carrriers[i].emit('sn_cn',{event:'oturumacildi',status:'ok',message:'Oturum açılıyor'});
																		}
																	}
																}).catch(err => {
																	Sentry.captureException(err);
																	Sentry.addBreadcrumb({
																		category: 'material_prepare',
																		message: 'oturumac 9 ',
																		level: 'error',
																		data:sql
																	});
																	setTimeout(() => {
																		process.exit(611);
																	}, 500);
																	return;
																});
														}).catch(err => {
															Sentry.captureException(err);
															Sentry.addBreadcrumb({
																category: 'material_prepare',
																message: 'oturumac 8 ',
																level: 'error',
																data:sql
															});
															setTimeout(() => {
																process.exit(612);
															}, 500);
															return;
														});
												});
											}).catch(err => {
												Sentry.captureException(err);
												Sentry.addBreadcrumb({
													category: 'material_prepare',
													message: 'oturumac 4 ',
													level: 'error',
													data:sql
												});
												setTimeout(() => {
													process.exit(613);
												}, 500);
												return;
											});
										}
									}).catch(err => {
										Sentry.captureException(err);
										Sentry.addBreadcrumb({
											category: 'material_prepare',
											message: 'oturumac 3 ',
											level: 'error',
											data:sql
										});
										setTimeout(() => {
											process.exit(614);
										}, 500);
										return;
									});
								}
							}).catch(err => {
								Sentry.captureException(err);
								Sentry.addBreadcrumb({
									category: 'material_prepare',
									message: 'oturumac 1 ',
									level: 'error',
									data:sql
								});
								setTimeout(() => {
									process.exit(615);
								}, 500);
								return;
							});
						}else{
							//Cihaz aktif değil
							for(let i=0;i<carrriers.length;i++){
								if(carrriers[i]&&carrriers[i].dcsClientId==data.id){
									carrriers[i].emit('forklift_oturumac',{status:'err',message:'Cihaz aktif değil'});
								}
							}
						}
						return null;
					});
				}else{
					//cihaz bağlı değil
				}
				break;
			}
			case 'oturumkapat':{
				let _carrrier='';
				let idx=getIndex(carrriers,'dcsClientId',data.id);
				if(idx>-1){
					let sql='SELECT c.* from carriers c join carrier_sessions cs on cs.carrier=c.code where cs.finish is null and c.id=:id and cs.empcode=:empcode ';
					return sequelize.query(sql, { replacements:{id:data.id,empcode:data.sicil}, type: sequelize.QueryTypes.SELECT}).then((result_c)=>{
						if(result_c&&result_c.length>0){
							_carrrier=result_c[0].code;
							sql='UPDATE carrier_sessions set finish=CURRENT_TIMESTAMP where carrier=:carrier and finish is null and empcode=:empcode';
							return sequelize.query(sql, { replacements:{carrier:_carrrier,empcode:data.sicil}, type: sequelize.QueryTypes.UPDATE}).then(()=>{
								for(let i=0;i<carrriers.length;i++){
									if(carrriers[i]&&carrriers[i].code==_carrrier){
										carrriers[i].emit('forklift_oturumkapandi',{status:'ok',message:'Oturum kapatıldı'});
										carrriers[i].emit('sn_cn',{event:'oturumkapandi',status:'ok',message:'Oturum kapatıldı'});
									}
								}
								return null;
							}).catch(err => {
								Sentry.captureException(err);
								Sentry.addBreadcrumb({
									category: 'material_prepare',
									message: 'oturumkapat 11 ',
									level: 'error',
									data:sql
								});
								setTimeout(() => {
									process.exit(616);
								}, 500);
								return;
							});
						}else{
							for(let i=0;i<carrriers.length;i++){
								if(carrriers[i]&&carrriers[i].dcsClientId==data.id){
									carrriers[i].emit('forklift_hata',{status:'err',message:'Oturum kaydı bulunamadı.'});
									carrriers[i].emit('sn_cn',{event:'hata',status:'err',message:'Oturum kaydı bulunamadı.'});
								}
							}
						}
						return null;
					});
				}else{
					//cihaz bağlı değil
				}
				break;
			}
			case 'tasimagorevikapat':{
				//hedef lokasyonun istasyon olmaması durumunda taskcase tablosuna veri girişi olmayacak
				//lokasyonvaris değerinin istasyon olup olmadığını belirle
				let kasano='';
				if(typeof data.kasano==='string'){
					kasano=data.kasano;
				}else{
					data.kasano='';
				}
				let sicil=data.sicil;
				let forklift=data.forklift;
				let bilesen=data.bilesen.replace('@', '/');
				let miktar=parseFloat(data.miktar);
				if(miktar==0&&_ip==='10.10.0.10'){
					miktar=100;
				}
				let file=data.file;
				let lokasyonvaris=data.lokasyonvaris;
				let id=0;
				let isim='';
				let verilenmiktar=miktar;
				let close_case_movements=(id,quantitydelivered,client,assignmenttime,casenumberdestination)=>{
					let breaktimeseconds=0;
					let cm_id=id;
					//let sql_breaktime='select sum(COALESCE(DATE_PART(\'day\', cld_1.finish::timestamp - cld_1.start::timestamp) * 24*3600 '+
					//'			+ DATE_PART(\'hour\', cld_1.finish::timestamp - cld_1.start::timestamp) * 3600  '+
					//'			+ DATE_PART(\'minute\', cld_1.finish::timestamp - cld_1.start::timestamp)*60  '+
					//'			+ DATE_PART(\'second\', cld_1.finish::timestamp - cld_1.start::timestamp),0)) breaktimeseconds '+
					//' from ( '+
					//'	SELECT case when cld.start>:assignmenttime then cld.start else :assignmenttime end as start '+
					//'		,coalesce(cld.finish,CURRENT_TIMESTAMP(0)) finish '+
					//'	from client_lost_details cld '+
					//'	where cld.type=\'l_c\' and cld.losttype in (\'CUMA PAYDOSU\',\'ÇAY MOLASI\',\'YEMEK MOLASI\') and cld.client=:client '+
					//'	and (CURRENT_TIMESTAMP(0) between cld.start and coalesce(cld.finish,CURRENT_TIMESTAMP(0)) '+
					//'		or :assignmenttime between cld.start and coalesce(cld.finish,CURRENT_TIMESTAMP(0)) '+
					//'		or (:assignmenttime<=cld.start and CURRENT_TIMESTAMP(0)>=coalesce(cld.finish,CURRENT_TIMESTAMP(0))) '+
					//'	) '+
					//')cld_1';
					let sql_breaktime=`
					select sum(COALESCE(DATE_PART('day', cld_1.finish::timestamp - cld_1.start::timestamp) * 24*3600 
						+ DATE_PART('hour', cld_1.finish::timestamp - cld_1.start::timestamp) * 3600  
						+ DATE_PART('minute', cld_1.finish::timestamp - cld_1.start::timestamp)*60  
						+ DATE_PART('second', cld_1.finish::timestamp - cld_1.start::timestamp),0)) breaktimeseconds 
					from ( 
						SELECT case when cld.start>:assignmenttime then cld.start else :assignmenttime end as start 
							,coalesce(cld.finish,CURRENT_TIMESTAMP(0)) finish 
						from client_lost_details cld 
						join (SELECT id,code,beginval,endval,concat(current_date,' ',cast(current_time as varchar(8))) zaman,CURRENT_TIMESTAMP(0) ts
							,cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as date) "day" 
							,concat(cast(case when beginval>endval and cast(current_time as varchar(5))<=endval then current_date - INTERVAL '1 day' else current_date end as varchar(10)),' ',beginval,':00')::timestamp jr_start
							,concat(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)),' ',endval,':59')::timestamp jr_end
						from job_rotations 
						where (finish is null or finish<now())
							and (
							(endval>beginval and cast(current_time as varchar(5)) between beginval and endval)
							or (endval<beginval and (cast(current_time as varchar(5))>=beginval or cast(current_time as varchar(5))<=endval) )
						)) sv on sv.day=cld.day and sv.code=cld.jobrotation
						where cld.type='l_c' and cld.losttype in ('CUMA PAYDOSU','ÇAY MOLASI','YEMEK MOLASI') and cld.client=:client 
						and (CURRENT_TIMESTAMP(0) between cld.start and coalesce(cld.finish,CURRENT_TIMESTAMP(0)) 
							or :assignmenttime between cld.start and coalesce(cld.finish,CURRENT_TIMESTAMP(0)) 
							or (:assignmenttime<=cld.start and CURRENT_TIMESTAMP(0)>=coalesce(cld.finish,CURRENT_TIMESTAMP(0))) 
						) 
					)cld_1
					`;
					//console.log("assignmenttime:",assignmenttime,"client:",client);
					return sequelize.query(sql_breaktime, { replacements:{assignmenttime:assignmenttime,client:client}, type: sequelize.QueryTypes.SELECT}).then(function(result_breaktime){
						if(result_breaktime&&result_breaktime.length>0){
							breaktimeseconds=result_breaktime[0]['breaktimeseconds'];
						}
						let sql='update case_movements '+
						' set actualfinishtime=CURRENT_TIMESTAMP(0) '+
						' ,status=\'CLOSED\' '+
						' ,quantitydelivered=:quantitydelivered '+
						' ,breaktimeseconds=:breaktimeseconds '+
						' ,casenumberdestination=:casenumberdestination '+
						' where id=:id';
						return sequelize.query(sql, { replacements:{quantitydelivered:quantitydelivered,breaktimeseconds:breaktimeseconds,id:id,casenumberdestination:casenumberdestination}, type: sequelize.QueryTypes.UPDATE}).then(function(){
							let sql_cc_u=`update case_calls set actualfinishtime=CURRENT_TIMESTAMP(0),state=:state where casemovementid=:casemovementid and actualfinishtime is null`;
							return sequelize.query(sql_cc_u, { replacements:{casemovementid:cm_id,state:'TAŞINDI'}, type: sequelize.QueryTypes.UPDATE}).catch(err=>{
								Sentry.addBreadcrumb({
									category: 'material_prepare',
									message: 'tasimagorevikapat 1010-2 ',
									level: 'error',
									data:JSON.stringify(data)
								});
								Sentry.captureException(err);
							});
						}).catch(err=>{
							Sentry.addBreadcrumb({
								category: 'material_prepare',
								message: 'tasimagorevikapat 1010-1 ',
								level: 'error',
								data:JSON.stringify(data)
							});
							Sentry.captureException(err);
						});
					}).catch(err=>{
						Sentry.addBreadcrumb({
							category: 'material_prepare',
							message: 'tasimagorevikapat 1010 ',
							level: 'error',
							data:JSON.stringify(data)
						});
						Sentry.captureException(err);
					});
				};
				let close_case_movement_details=(casemovementid)=>{
					let sql='update case_movement_details set donetime=CURRENT_TIMESTAMP(0) where casemovementid=:casemovementid and donetime is null and canceltime is null';
					return sequelize.query(sql, { replacements:{casemovementid:casemovementid}, type: sequelize.QueryTypes.UPDATE}).catch(err=>{
						Sentry.addBreadcrumb({
							category: 'material_prepare',
							message: 'tasimagorevikapat 1011 ',
							level: 'error',
							data:JSON.stringify(data)
						});
						Sentry.captureException(err);
					});
				};
				let insert_production_storages=(obj/*,uuid,queue*/)=>{
					let dt = new timezoneJS.Date();
					obj.start=dt.toString('yyyy-MM-dd HH:mm:ss');
					obj.finish=null;
					let sql_ps_1='insert into production_storages '+
					' (casemovementidin,casemovementidout,code,number,stockcode'+
					' ,casename,start,finish,erprefnumber,quantity,create_user_id,created_at,update_user_id,updated_at) '+
					' values '+
					' (:casemovementidin,:casemovementidout,:code,:number,:stockcode'+
					' ,:casename,:start,:finish,:erprefnumber,:quantity,1,:createdat,1,:updatedat) ';
					let rep={
						casemovementidin:obj.casemovementidin
						,casemovementidout:obj.casemovementidout
						,code:obj.code
						,number:obj.number
						,stockcode:obj.stockcode
						,casename:obj.casename
						,start:obj.start
						,finish:obj.finish
						,erprefnumber:obj.erprefnumber
						,quantity:parseFloat(obj.quantity)
						,createdat:obj.start
						,updatedat:obj.start
					};
					return sequelize.query(sql_ps_1, { replacements:rep, type: sequelize.QueryTypes.INSERT}).catch(err=>{
						Sentry.addBreadcrumb({
							category: 'material_prepare',
							message: 'tasimagorevikapat 1012 '+sql+' '+JSON.stringify(rep)+' '+JSON.stringify(data),
							level: 'error',
							data:JSON.stringify(data)
						});
						Sentry.captureException(err);
					});
				};
				let close_production_storages=(id,code,number,stockcode,erprefnumber)=>{
					let sql_ps='select id from production_storages where finish is null and code=:code and stockcode=:stockcode and erprefnumber=:erprefnumber order by id limit 1';
					return sequelize.query(sql_ps, { replacements:{code:code,number:number,stockcode:stockcode,erprefnumber:erprefnumber}, type: sequelize.QueryTypes.SELECT}).then((result_ps)=>{
						if(result_ps&&result_ps.length>0){
							let row=result_ps[0];
							let sql='update production_storages set finish=CURRENT_TIMESTAMP(0),casemovementidout=:casemovementidout,update_user_id=1,updated_at=current_timestamp() where id=:id and finish is null';
							return sequelize.query(sql, { replacements:{casemovementidout:id,id:row.id}, type: sequelize.QueryTypes.UPDATE}); 
						}
						return null;
					}).catch(err=>{
						Sentry.addBreadcrumb({
							category: 'material_prepare',
							message: 'tasimagorevikapat 1013 ',
							level: 'error',
							data:JSON.stringify(data)
						});
						Sentry.captureException(err);
					});
				};
				let insert_task_cases=(obj,uuid,queue)=>{
					let sql_pt='SELECT * from product_trees where finish is null and code=:code and number=:number ';
					if(obj.casetype=='O'){
						sql_pt+=' and stockname=:stockcode limit 1';
					}else{
						sql_pt+=' and stockcode=:stockcode limit 1';
					}
					let dt = new timezoneJS.Date();
					return sequelize.query(sql_pt, { replacements:{code:obj.opcode,number:obj.opnumber,stockcode:obj.stockcode}, type: sequelize.QueryTypes.SELECT}).then((result_pt)=>{
						if(result_pt&&result_pt.length>0){
							let row=result_pt[0];
							obj.opname=row.name;
							obj.stockname=row.stockname;
							obj.quantityused=obj.casetype=='O'?1:parseFloat(row.quantity);
							obj.feeder=row.feeder;
							obj.preptime=row.preptime;
							obj.goodsplanner=row.goodsplanner;
							obj.start=dt.toString('yyyy-MM-dd HH:mm:ss');
							obj.finish=null;
							//sunucu tarafında task_cases tablosuna kayıt oluştur
							let sql_tc_1='insert into task_cases '+
							' (movementdetail,erprefnumber,casetype,opcode,opnumber,opname,stockcode,stockname,quantityused,casename'+
							' ,casenumber,packcapacity,feeder,preptime,quantityremaining,quantitydeliver,conveyor,goodsplanner'+
							' ,record_id,create_user_id,created_at,update_user_id,updated_at,version,status,client,start,finish) '+
							' values '+
							' (:movementdetail,:erprefnumber,:casetype,:opcode,:opnumber,:opname,:stockcode,:stockname,:quantityused,:casename'+
							' ,:casenumber,:packcapacity,:feeder,:preptime,:quantityremaining,:quantitydeliver,:conveyor,:goodsplanner'+
							' ,:record_id,0,CURRENT_TIMESTAMP(0),0,CURRENT_TIMESTAMP(0),1,:status,:client,:start,:finish) ';
							return sequelize.query(sql_tc_1, { replacements:{
								movementdetail:obj.movementdetail
								,erprefnumber:obj.erprefnumber
								,casetype:obj.casetype
								,opcode:obj.opcode
								,opnumber:obj.opnumber
								,opname:obj.opname
								,stockcode:obj.stockcode
								,stockname:obj.stockname
								,quantityused:obj.quantityused
								,casename:obj.casename
								,casenumber:(obj.casenumber?obj.casenumber:null)
								,packcapacity:obj.packcapacity
								,feeder:obj.feeder
								,preptime:obj.preptime
								,quantityremaining:parseFloat(obj.quantityremaining)
								,quantitydeliver:parseFloat(obj.quantitydeliver)
								,conveyor:obj.conveyor
								,goodsplanner:obj.goodsplanner
								,record_id:obj.record_id
								,status:obj.status
								,client:obj.client
								,start:obj.start
								,finish:obj.finish
							}, type: sequelize.QueryTypes.INSERT}).then(()=>{
								log_error('tasimagorevikapat-i-tc',JSON.stringify(obj),'insert_task_cases');
								//cihaza gönderilmek üzere mesaj kaydı oluştur
								let sql_sm='insert into _sync_messages (ip,uuid,type,data,queued,"createdAt","updatedAt") '+
								'values (:ip,:uuid,\'sync_message\',:data,0,CURRENT_TIMESTAMP(0),CURRENT_TIMESTAMP(0))';
								obj.createdAt=dt.toString('yyyy-MM-dd HH:mm:ss');
								obj.updatedAt=dt.toString('yyyy-MM-dd HH:mm:ss');
								let data={
									tableName:'task_cases'
									,procType:'insert'
									,data:obj
								};
								return sequelize.query(sql_sm, { replacements:{ip:queue+'_server',uuid:uuid,data:JSON.stringify(data)}, type: sequelize.QueryTypes.INSERT}).then(()=>{
									log_error('tasimagorevikapat-i-sm',JSON.stringify(obj),'insert__sync_messages');
								}).catch(err => {
									Sentry.captureException(err);
								});
							}).catch(err => {
								Sentry.captureException(err);
							});
						}else{
							log_error('tasimagorevikapat-product_trees-not_found',JSON.stringify(data),'insert_task_cases');
							return null;
						}
					}).catch(err=>{
						Sentry.addBreadcrumb({
							category: 'material_prepare',
							message: 'tasimagorevikapat 1014 ',
							level: 'error',
							data:JSON.stringify(data)
						});
						Sentry.captureException(err);
					});
				};
				let update_task_cases=(code,number,casename,locationsource,erprefnumber,casetype)=>{
					let sql_s_tc='SELECT * from task_cases where movementdetail is not null and finish is not null and status=\'WAITFORDELIVERY\' '+
					' and opcode=:opcode and opnumber=:opnumber and casename=:casename and client=:client and erprefnumber=:erprefnumber and casetype=:casetype '+
					' order by finish limit 1';
					return sequelize.query(sql_s_tc, { replacements:{opcode:code,opnumber:number,casename:casename,client:locationsource,erprefnumber:erprefnumber,casetype:casetype}, type: sequelize.QueryTypes.SELECT})
						.then((res_tc)=>{
							if(res_tc&&res_tc.length>0){
								let row=res_tc[0];
								let sql='update task_cases set status=\'DELIVERED\' where id=:id';
								return sequelize.query(sql, { replacements:{id:row.id}, type: sequelize.QueryTypes.UPDATE}); 
							}else{
								return null;
							}
						}).catch(err=>{
							Sentry.addBreadcrumb({
								category: 'material_prepare',
								message: 'tasimagorevikapat 1015 ',
								level: 'error',
								data:JSON.stringify(data)
							});
							Sentry.captureException(err);
						});
				};
				log_error('tasimagorevikapat',JSON.stringify(data),'tasimagorevikapat-1');
				let sql='SELECT * from carriers c where c.code=:code and code is not null and code<>\'\'';
				return sequelize.query(sql, { replacements:{code:forklift}, type: sequelize.QueryTypes.SELECT}).then((result_c)=>{
					if(result_c&&result_c.length>0){
						id=result_c[0].id;
						let idx=getIndex(carrriers,'code',forklift);
						if(idx===-1){
							idx=getIndex(carrriers,'dcsClientId',data.id);
						}
						if(/*sicil>0&&*/miktar>0){
							//20161025 toplantı kararı sonrasında Forklift - Tablet sicil uyuşmazlığı(oturum kontrolü) kapatıldı-eski kod içinde var kapatılan kısım
							//let sql_1='SELECT cm.*,now()::timestamp(0) zamangiris,case when c.id is null then 0 else 1 end is_client,c.uuid,c.ip '+
							//' 	,(SELECT case when cp.value=\'true\' then 1 else 0 end is_client_delivery from client_params cp where delete_user_id is null and client=cm.locationdestination and name=\'task_delivery\' limit 1) is_client_delivery '+
							//'		,(SELECT case when delivery=true then 1 else 0 end is_op_delivery from product_trees where finish is null and materialtype=\'O\' and code=cm.code and number=cm.number limit 1) is_op_delivery '+
							//' 	,case when exists (select id from task_cases where movementdetail=cm.id) then 1 else 0 end is_tc_exists '+
							//' from case_movements cm '+
							//' left join clients c on c.code=cm.locationdestination '+
							//' where cm.actualfinishtime is null and cm.status=\'WAITFORDELIVERY\' and cm.carrier=:carrier and replace(locationdestination,\'.\',\'\')=:destination  '+
							//' and (case when cm.tasktype in (\'GRV001\',\'GRV011\') then cm.casename else cm.stockcode end)=:bilesen '+
							//' limit 1';
							let sql_1=`
								select * from (	
									SELECT cm.*,now()::timestamp(0) zamangiris,case when c.id is null then 0 else 1 end is_client,c.uuid,c.ip 
										,(SELECT case when cp.value='true' then 1 else 0 end is_client_delivery from client_params cp where delete_user_id is null and client=cm.locationdestination and name='task_delivery' limit 1) is_client_delivery 
										,(SELECT case when delivery=true then 1 else 0 end is_op_delivery from product_trees where finish is null and materialtype='O' and code=cm.code and number=cm.number limit 1) is_op_delivery 
										,case when exists (select id from task_cases where movementdetail=cm.id) then 1 else 0 end is_tc_exists 
									from case_movements cm 
									left join clients c on c.code=cm.locationdestination and c.uuid is not null
									where cm.actualfinishtime is null and cm.status='WAITFORDELIVERY' and cm.carrier=:carrier and replace(locationdestination,'.','')=:destination  
										and (case when cm.tasktype in ('GRV001','GRV011') then cm.casename else cm.stockcode end)=:bilesen)cm1
								where cm1.is_tc_exists=0
								limit 1
							`;
							return sequelize.query(sql_1, { replacements:{carrier:forklift,destination:lokasyonvaris,bilesen:bilesen}, type: sequelize.QueryTypes.SELECT}).then((result_cm)=>{
								if(result_cm&&result_cm.length>0){
									let _promises=[];
									let res_cm=result_cm[0];
									file=(_ip=='192.168.1.40'?'':res_cm['filepath']);
									if(sicil!=res_cm['empcode']){
										data.res_cm_empcode=res_cm['empcode'];
										log_error('tasimagorevikapat_Kapatan sicil hatalı',JSON.stringify(data),'tasimagorevikapat-2');
										if(idx>-1){
											for(let i=0;i<carrriers.length;i++){
												if(carrriers[i]&&carrriers[i].code==forklift){
													carrriers[i].emit('forklift_hata',{status:'err',message:'Kapatan sicil hatalı\nGörevli Sicil:'+res_cm['empcode']+' Terminal Sicil:'+sicil});
													carrriers[i].emit('sn_cn',{event:'hata',status:'err',message:'Kapatan sicil hatalı\nGörevli Sicil:'+res_cm['empcode']+' Terminal Sicil:'+sicil});
												}
											}
										}
										return null;
									}
									if(res_cm['is_tc_exists']===1){
										log_error('tasimagorevikapat_hareket daha önce kapanmış',JSON.stringify(data),'tasimagorevikapat-12');
										if(idx>-1){
											for(let i=0;i<carrriers.length;i++){
												if(carrriers[i]&&carrriers[i].code==forklift){
													carrriers[i].emit('forklift_hata',{status:'err',message:'Görev zaten kapatılmış'});
													carrriers[i].emit('sn_cn',{event:'hata',status:'err',message:'Görev zaten kapatılmış'});
												}
											}
										}
										return null;
									}
									let kasahareketId=res_cm.id;
									switch (res_cm.tasktype) {
									case 'GRV001':{//Boş taşıyıcı sahasından üretime boş taşıyıcı taşınması
										verilenmiktar=miktar*parseFloat(res_cm.quantitynecessary);
										_promises.push(close_case_movements(kasahareketId,verilenmiktar,res_cm.client,res_cm.assignmenttime,''));
										_promises.push(close_case_movement_details(kasahareketId));
										//görev kapatılacak, task_cases kaydı oluşturulacak
										let obj_tc={
											movementdetail:res_cm.id
											,erprefnumber:res_cm.erprefnumber
											,casetype:'O'
											,opcode:res_cm.code
											,opnumber:res_cm.number
											,stockcode:res_cm.stockcode
											,casename:res_cm.casename
											,packcapacity:verilenmiktar
											,quantityremaining:0
											,quantitydeliver:0
											,conveyor:0
											,status:'WAIT'
											,client:res_cm.locationdestination
											,record_id:uuidv4()
										};
										_promises.push(insert_task_cases(obj_tc,res_cm.uuid,res_cm.client));
										break;
									}
									case 'GRV002':{//Yarımamül Ambardan üretime alt bileşen malzeme taşınması
										_promises.push(close_case_movements(kasahareketId,verilenmiktar,res_cm.client,res_cm.assignmenttime,kasano));
										_promises.push(close_case_movement_details(kasahareketId));
										let obj_tc={
											movementdetail:res_cm.id
											,erprefnumber:res_cm.erprefnumber
											,casetype:'I'
											,opcode:res_cm.code
											,opnumber:res_cm.number
											,stockcode:res_cm.stockcode
											,casename:res_cm.casename
											,casenumber:kasano
											,packcapacity:verilenmiktar
											,quantityremaining:verilenmiktar
											,quantitydeliver:0
											,conveyor:0
											,status:'WAIT'
											,client:res_cm.locationdestination
											,record_id:uuidv4()
										};
										_promises.push(insert_task_cases(obj_tc,res_cm.uuid,res_cm.client));
										break;
									}
									case 'GRV003':{//Yarımamül ambarından malzeme hazırlık alanına yarımamül taşınması*-hareketi yok
										_promises.push(close_case_movements(kasahareketId,verilenmiktar,res_cm.client,res_cm.assignmenttime,''));
										_promises.push(close_case_movement_details(kasahareketId));
										let obj_tc={
											movementdetail:res_cm.id
											,erprefnumber:res_cm.erprefnumber
											,casetype:'I'
											,opcode:res_cm.code
											,opnumber:res_cm.number
											,stockcode:res_cm.stockcode
											,casename:res_cm.casename
											,packcapacity:verilenmiktar
											,quantityremaining:verilenmiktar
											,quantitydeliver:0
											,conveyor:0
											,status:'WAIT'
											,client:res_cm.locationdestination
											,record_id:uuidv4()
										};
										_promises.push(insert_task_cases(obj_tc,res_cm.uuid,res_cm.client));
										break;
									}
									case 'GRV004':{//Malzeme hazırlık alanından üretime alt bileşen malzeme taşınması
										_promises.push(close_case_movements(kasahareketId,verilenmiktar,res_cm.client,res_cm.assignmenttime,kasano));
										_promises.push(close_case_movement_details(kasahareketId));
										let obj_tc={
											movementdetail:res_cm.id
											,erprefnumber:res_cm.erprefnumber
											,casetype:'I'
											,opcode:res_cm.code
											,opnumber:res_cm.number
											,stockcode:res_cm.stockcode
											,casename:res_cm.casename
											,casenumber:kasano
											,packcapacity:verilenmiktar
											,quantityremaining:verilenmiktar
											,quantitydeliver:0
											,conveyor:0
											,status:'WAIT'
											,client:res_cm.locationdestination
											,record_id:uuidv4()
										};
										_promises.push(insert_task_cases(obj_tc,res_cm.uuid,res_cm.client));
										break;
									}
									case 'GRV005':{//Üretimden muayene sahasına mamüllerin taşınması
										_promises.push(close_case_movements(kasahareketId,verilenmiktar,res_cm.client,res_cm.assignmenttime,''));
										_promises.push(close_case_movement_details(kasahareketId));
										_promises.push(update_task_cases(res_cm.code,res_cm.number,res_cm.casename,res_cm.locationsource,res_cm.erprefnumber,'O'));
										break;
									}
									case 'GRV006':{//Muayene sahasından yarımamül ambarına yarımamüllerin taşınması*
										
										break;
									}
									case 'GRV007':{//Üretimden yarımamül ambarına yarımamüllerin taşınması
										_promises.push(close_case_movements(kasahareketId,verilenmiktar,res_cm.client,res_cm.assignmenttime,''));
										_promises.push(close_case_movement_details(kasahareketId));
										_promises.push(update_task_cases(res_cm.code,res_cm.number,res_cm.casename,res_cm.locationsource,res_cm.erprefnumber,'O'));
										break;
									}
									case 'GRV008':{//Üretimden mamül ambarına mamüllerin taşınması
										_promises.push(close_case_movements(kasahareketId,verilenmiktar,res_cm.client,res_cm.assignmenttime,''));
										_promises.push(close_case_movement_details(kasahareketId));
										_promises.push(update_task_cases(res_cm.code,res_cm.number,res_cm.casename,res_cm.locationsource,res_cm.erprefnumber,'O'));
										break;
									}
									case 'GRV009':{//Üretim esnasında oluşan ara komplelerin bir tezgahtan production_storages'a taşınması 
										_promises.push(close_case_movements(kasahareketId,verilenmiktar,res_cm.client,res_cm.assignmenttime,''));
										_promises.push(close_case_movement_details(kasahareketId));
										_promises.push(update_task_cases(res_cm.code,res_cm.number,res_cm.casename,res_cm.locationsource,res_cm.erprefnumber,'O'));
										let obj_ps={
											casemovementidin:res_cm.id
											,casemovementidout:null
											,code:res_cm.code
											,number:res_cm.number
											,stockcode:res_cm.stockcode
											,casename:res_cm.casename
											,erprefnumber:res_cm.erprefnumber
											,quantity:verilenmiktar
										};
										_promises.push(insert_production_storages(obj_ps/*,res_cm.uuid,res_cm.client*/));
										break;
									}
									case 'GRV010':{//Giriş muayene sahasından yarımamül ambarına yarımamüllerin taşınması*-hareketi yok
										
										break;
									}
									case 'GRV011':{//Üretimden boş taşıyıcı sahasına boş hammadde kasası taşınması
										_promises.push(close_case_movements(kasahareketId,verilenmiktar,res_cm.client,res_cm.assignmenttime,''));
										_promises.push(close_case_movement_details(kasahareketId));
										_promises.push(update_task_cases(res_cm.code,res_cm.number,res_cm.casename,res_cm.locationsource,res_cm.erprefnumber,'I'));
										break;
									}
									case 'GRV012':{//Üretim esnasında oluşan ara komplelerin production_storages'dan bir tezgaha taşınması 
										_promises.push(close_case_movements(kasahareketId,verilenmiktar,res_cm.client,res_cm.assignmenttime,''));
										_promises.push(close_case_movement_details(kasahareketId));
										let obj_tc={
											movementdetail:res_cm.id
											,erprefnumber:res_cm.erprefnumber
											,casetype:'I'
											,opcode:res_cm.code
											,opnumber:res_cm.number
											,stockcode:res_cm.stockcode
											,casename:res_cm.casename
											,packcapacity:verilenmiktar
											,quantityremaining:verilenmiktar
											,quantitydeliver:0
											,conveyor:0
											,status:'WAIT'
											,client:res_cm.locationdestination
											,record_id:uuidv4()
										};
										_promises.push(insert_task_cases(obj_tc,res_cm.uuid,res_cm.client));
										_promises.push(close_production_storages(res_cm.id,res_cm.code,res_cm.number,res_cm.stockcode,res_cm.erprefnumber));
										break;
									}
									default:
										break;
									}
									return Promise.all(_promises).then(()=>{
										if(file!==''){
											try {
												if (fs.existsSync(file)) {
													fs.unlinkSync(file);
												}
											} catch (error) {
												Sentry.addBreadcrumb({
													category: 'material_prepare',
													message: 'tasimagorevikapat 1001 ',
													level: 'error',
													data:JSON.stringify(data)
												});
												Sentry.captureException(error);
												log_error('err-gorevkapatdosyasil',JSON.stringify(data),'tasimagorevikapat-3');
											} finally {
												return null;
											}
										}
										return null;
									}).then(()=>{
										let sql_empname='SELECT empname from carrier_sessions where finish is null and carrier=:carrier';
										return sequelize.query(sql_empname, { replacements:{carrier:forklift}, type: sequelize.QueryTypes.SELECT}).then((res_empname)=>{
											if(res_empname&&res_empname.length>0){
												isim=res_empname[0].empname;
											}
											let sql_cm='SELECT count(*)sayi from case_movements where actualfinishtime is null and status=\'WAITFORDELIVERY\' and carrier=:carrier';
											return sequelize.query(sql_cm, { replacements:{carrier:forklift}, type: sequelize.QueryTypes.SELECT}).then((res_cm)=>{
												if(res_cm[0].sayi==0){
													sql='SELECT id,code,beginval,endval '+
												' 	,cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL \'1 day\' else current_date end as varchar(10)) "day"  '+
												' 	,concat(cast(case when beginval>endval and cast(current_time as varchar(5))<=endval then current_date - INTERVAL \'1 day\' else current_date end as varchar(10)),\' \',beginval,\':00\')::timestamp jr_start '+
												' 	,concat(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL \'1 day\' else current_date end as varchar(10)),\' \',endval,\':59\')::timestamp jr_end '+
												' from job_rotations  '+
												' where (finish is null or finish<now()) '+
												'	and ( '+
												'	(endval>beginval and cast(current_time as varchar(5)) between beginval and endval) '+
												'	or (endval<beginval and (cast(current_time as varchar(5))>=beginval or cast(current_time as varchar(5))<=endval) ) '+
												' )';
													return sequelize.query(sql, { type: sequelize.QueryTypes.SELECT}).then((result_sv_2)=>{
														let _day=null;
														let _jr=null;
														if(result_sv_2&&result_sv_2.length>0){
															_day=result_sv_2[0].day;
															_jr=result_sv_2[0].code;
														}
														let sql_ces='insert into carrier_empty_sessions (carrier,empcode,empname,start,day,jobrotation) '+
														' values (:carrier,:empcode,:empname,CURRENT_TIMESTAMP(0),:day,:jobrotation)';
														return sequelize.query(sql_ces, { replacements:{carrier:forklift,empcode:sicil,empname:isim,day:_day,jobrotation:_jr}, type: sequelize.QueryTypes.INSERT}).catch(err => {
															Sentry.captureException(err);
															Sentry.addBreadcrumb({
																category: 'material_prepare',
																message: 'tasimagorevikapat 1002 ',
																level: 'error',
																data:JSON.stringify(data)
															});
															Sentry.addBreadcrumb({
																category: 'material_prepare',
																message: 'tasimagorevikapat 15 ',
																level: 'error',
																data:''
															});
															setTimeout(() => {
																process.exit(617);
															}, 500);
															return;
														});
													}).catch(err=>{
														Sentry.addBreadcrumb({
															category: 'material_prepare',
															message: 'tasimagorevikapat 1003 ',
															level: 'error',
															data:JSON.stringify(data)
														});
														Sentry.captureException(err);
														setTimeout(() => {
															process.exit(628);
														}, 500);
														return;
													});
												}
											}).catch(err => {
												Sentry.addBreadcrumb({
													category: 'material_prepare',
													message: 'tasimagorevikapat 1004 ',
													level: 'error',
													data:JSON.stringify(data)
												});
												Sentry.captureException(err);
												Sentry.addBreadcrumb({
													category: 'material_prepare',
													message: 'tasimagorevikapat 14 ',
													level: 'error',
													data:''
												});
												setTimeout(() => {
													process.exit(618);
												}, 500);
												return;
											});
										}).catch(err => {
											Sentry.addBreadcrumb({
												category: 'material_prepare',
												message: 'tasimagorevikapat 1005 ',
												level: 'error',
												data:JSON.stringify(data)
											});
											Sentry.captureException(err);
											Sentry.addBreadcrumb({
												category: 'material_prepare',
												message: 'tasimagorevikapat 13 ',
												level: 'error',
												data:''
											});
											setTimeout(() => {
												process.exit(619);
											}, 500);
											return;
										});
									}).then(()=>{
										log_error('gorevkapatildi',JSON.stringify(data),'tasimagorevikapat-4');
										if(idx>-1){
											let _promises=[];
											for(let i=0;i<carrriers.length;i++){
												if(carrriers[i]&&carrriers[i].code==forklift){
													_promises.push(tasimagorevilistele2(carrriers[i],carrriers[i].code));
												}
												if(carrriers[i]&&carrriers[i].dcsClientId==id){
													_promises.push(tasimagorevilistele0(carrriers[i],id));
												}
											}
											return Promise.all(_promises).catch(err=>{
												Sentry.addBreadcrumb({
													category: 'material_prepare',
													message: 'tasimagorevikapat 1006 ',
													level: 'error',
													data:JSON.stringify(data)
												});
												Sentry.captureException(err);
											});
										}
										return null;
									}).catch(err => {
										Sentry.addBreadcrumb({
											category: 'material_prepare',
											message: 'tasimagorevikapat 100 ',
											level: 'error',
											data:''
										});
										Sentry.captureException(err);
										setTimeout(() => {
											process.exit(620);
										}, 500);
										return;
									});
								}else{
									log_error('tasimagorevikapat_Hareket Kaydı Bulunamadı-2',JSON.stringify(data),'tasimagorevikapat-5');
									if(idx>-1){
										for(let i=0;i<carrriers.length;i++){
											if(carrriers[i]&&carrriers[i].code==forklift){
												carrriers[i].emit('forklift_hata',{status:'err',message:'Hareket Kaydı Bulunamadı-2'});
												carrriers[i].emit('sn_cn',{event:'hata',status:'err',message:'Hareket Kaydı Bulunamadı-2'});
											}
										}
									}
								}
							}).catch(err => {
								Sentry.captureException(err);
								Sentry.addBreadcrumb({
									category: 'material_prepare',
									message: 'tasimagorevikapat 12 ',
									level: 'error',
									data:''
								});
								setTimeout(() => {
									process.exit(621);
								}, 500);
								return;
							});
						}else{
							log_error('tasimagorevikapat_Sicil-Miktar Bulunamadı',JSON.stringify(data),'tasimagorevikapat-6');
							if(idx>-1){
								for(let i=0;i<carrriers.length;i++){
									if(carrriers[i]&&carrriers[i].code==forklift){
										carrriers[i].emit('forklift_hata',{status:'err',message:'Sicil-Miktar Bulunamadı'});
										carrriers[i].emit('sn_cn',{event:'hata',status:'err',message:'Sicil-Miktar Bulunamadı'});
									}
								}
							}
						}
					}else{
						log_error('tasimagorevikapat_carrier_not_found',JSON.stringify(data),'tasimagorevikapat-7');
						return null;
					}
				});
			}
			case 'tasimagorevilistele':{
				let _promises=[];
				if(data.id){
					for(let i=0;i<carrriers.length;i++){
						if(carrriers[i].dcsClientId==data.id){
							_promises.push(tasimagorevilistele2(carrriers[i],carrriers[i].code));
						}
					}
				}
				if(data.carrier){
					let idx=getIndex(carrriers,'code',data.carrier);
					if(idx>-1){
						for(let i=0;i<carrriers.length;i++){
							if(carrriers[i].code==data.carrier){
								_promises.push(tasimagorevilistele2(carrriers[i],data.carrier));
							}
						}
					}else{
						Sentry.captureMessage({
							message: 'tgl_car_not_fnd',
							data : getAllClients(carrriers),
							id:data.id,
							carrier:data.carrier
						});
					}
				}
				return Promise.all(_promises);
			}
			default:
				break;
		}
	} catch (error) {
		Sentry.captureException(err);
		setTimeout(() => {
			process.exit(625);
		}, 500);
	}
};
let getIndex=(array,key,keyValue)=>{
	if(typeof array.length!='undefined')
		for(let i = 0, len = array.length; i < len; i++){
			if(array[i][key]==keyValue)	return i;
		}
	else
		for(let i in array)
			if(typeof array[i]!='function')
				if(array[i][key]==keyValue)	return i;
	return -1;
};
let log_error=(title,msg,pos)=>{
	return sequelize.query('insert into _log_exception (code,file,line,message,method,path,sessionid,time) values (:code,:file,:line,:message,:method,:path,:sessionid,CURRENT_TIMESTAMP)'
		, { replacements: { code:100,file:title,line:0,message:msg,method:'NODE',path:pos,sessionid:0 },type: sequelize.QueryTypes.INSERT});
};
let getFullLocalDateString=()=>{
	let time=new timezoneJS.Date();
	return time.toString('yyyy-MM-dd HH:mm:ss.SSS');
};