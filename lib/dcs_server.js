let classBase=require('./base');
const dcs_message=require('./dcs_message');
const Promise = require('promise');
const crypto = require('crypto');
const dir = require('node-dir');
const fs = require('fs');
const util = require('util');
const fs_writeFile = util.promisify(fs.writeFile);
const path = require('path');
const { v4: uuidv4 } = require('uuid');
const models = require(__dirname+'/../models');
const cp = require('child_process');
const timezoneJS = require('timezone-js');
const Sentry = require('@sentry/node');
const path_export=__dirname+'/../_export/';

Sentry.init({
	dsn: 'https://4e61d0b3664d41c9a849f8edbb2b2255@sentry.kaitek.com.tr/3'
});
class dcs_server extends classBase.MyBase{
	constructor(config) {
		super(config); 
	}
	init(){
		let t = this;
		t.componentClass='dcs_server';
		t.version='0.1';
		t.isDevMachine=false;
		t.started=false;
		t.pin=false;
		t.timer=false;
		t.updatePrefix='verimotRT-Client_';
		t.updates=false;
		t.cfg_mh_importpath=false;
		t.cfg_material_prepare=false;
		t.port=8800;
		Object.assign(this, this.config);
		t._arr_tables=['client_details','client_lost_details','client_mould_details','client_mould_groups','client_params','client_production_details','kanban_operations','reworks','task_cases','task_reject_details','task_imports','task_lists'];
		t._arr_tables_finish_isnull=['client_details','client_lost_details','client_mould_details','client_mould_groups','client_production_details'];
		t._arr_tables_not_day=['client_details','client_mould_details','client_mould_groups','client_params','kanban_operations','product_trees','reject_types','reworks','task_cases','task_imports','task_lists'];
		t.proc={
			socket_server_device:false
			,socket_server_watch:false
			,data_importer:false
			,data_exporter:false
			,oee_calculate:false
			,material_prepare:false
			,rabbitmq_receive:false
			,rabbitmq_send:false
			,sync_clients:[]
		};
		
		t.clients=[];
		t.oee_clients=[];
		t.arr_sync_update_records=[];
		t.__basepath=__dirname.split('\\').slice(0, -1).join('\\');
		t.pin=t.randomIntInc(1000,9000);
		t.ip=t.getIp();
		t.isDevMachine=t.ip=='192.168.1.40'||t.ip=='192.168.1.10'||t.ip==='172.16.1.146';
		t.err=false;
		try {
			fs.accessSync(__dirname+'/../config.json');
		} catch (error) {
			fs.writeFileSync(__dirname+'/../config.json', JSON.stringify({'ip':t.ip,'port':t.port}));
		}
		try {
			fs.accessSync(__dirname+'/../version.txt');
		} catch (error) {
			fs.writeFileSync(__dirname+'/../version.txt', '1');
		}
		try {
			fs.accessSync(__dirname+'/../update');
		} catch (error) {
			fs.mkdirSync(__dirname+'/../update');
		}
		try{
			let config = JSON.parse(fs.readFileSync(__dirname+'/../config.json', 'utf-8'));
			let content = fs.readFileSync(__dirname+'/../version.txt');
			t.version=parseInt(content);
			if(typeof config!='undefined'){
				t.cfg_import_customer=config.import_customer;
				t.cfg_import_app=config.import_app.trim();
				t.cfg_material_prepare=config.malzeme_hazirlik&&parseInt(config.malzeme_hazirlik)==1?true:false;
				t.cfg_mh_importpath=config.mh_importpath?config.mh_importpath:false;
				t.cfg_dcs_server_IP=(typeof config.ip!='undefined'?config.ip:t.ip);
				t.cfg_dcs_server_PORT=(typeof config.port!='undefined'?config.port:t.port);
				t.cfg_export_fault=(typeof config.export_fault!='undefined'?config.export_fault:false);
				t.cfg_export_data=(typeof config.export_data!='undefined'?config.export_data:false);
				t.cfg_odbc_merga=false;
				t.cfg_odbc_boys=false;
				t.cfg_odbc_boys_prod=false;
				if(t.cfg_export_fault=='only_start_odbc_boys'||t.cfg_export_fault=='full_odbc_boys'||t.cfg_export_fault=='only_start_odbc_boys_ermetal'||t.cfg_export_fault=='full_odbc_boys_ermetal'){
					t.cfg_odbc_boys=config.odbc_boys;
					t.cfg_odbc_boys_prod=config.odbc_boys_prod;
				}
				if(config.odbc_merga){
					t.cfg_odbc_merga=config.odbc_merga;
				}
				t.conERP=false;
				if(typeof config.delivery!='undefined'&&config.delivery=='1'){
					var db = require('odbc')
						, cn = config.deliveryDbCon;
					db.connect(cn, function (err,connection) {
						if (err){
							return;//console.log(err);
						}
						t.conERP=connection;					
					});
				}
			}else{
				t.err='config read error';
			}
			if(t.err==false){
				t.db=models;
				t.db.sequelize.sync().then(function() {
					return t.db.sequelize.query('SELECT NOW() AS "theTime"', { type: t.db.sequelize.QueryTypes.SELECT}).then(function(result) {
						let _time=result[0].theTime;
						t.mjd(t.convertFullLocalDateString(_time)+' '+t.getFullLocalDateString());
						//if(t.cfg_material_prepare){
						//	t.p_material_prepare();
						//}
						t.run();
					});
				});
			}else{
				t.mjd(t.err);
			}
		}catch(err){
			t.mjd('start-err:'+err.message);
			//console.log(err);
			Sentry.captureException(err);
		}
	}
	run(){
		let t=this;
		Sentry.configureScope(scope => {
			scope.setTag('ip',t.cfg_dcs_server_IP);
		});
		Sentry.addBreadcrumb({
			category: 'program_start',
			message: 'Device IP: '+t.ip+' Port:'+t.cfg_dcs_server_PORT,
			level: Sentry.Severity.Info,
		});
		Sentry.captureMessage('Device Start');
		//Sentry.captureMessage('Server Start ip:'+t.ip+' port:'+t.cfg_dcs_server_PORT);
		t.updateControl();
		t.control_sync_update_records();
		t.p_socket_server_device();
		t.p_socket_server_watch();
		t.p_data_importer();
		//t.p_data_exporter();
		t.p_rabbitmq_send();
		t.p_rabbitmq_receive();
		//t.p_oee_calculate();
		t.started=true;
		t.mjd('start server v:'+t.version+' PID:'+process.pid);
		// if(t.ip==='10.10.0.10'){
		// 	let sql=`delete from "_user_right" where menu_id in (SELECT id from "_menu" where menuid in ('documentmodule','Document'))`;
		// 	return t.db.sequelize.query(sql,{type: t.db.sequelize.QueryTypes.DELETE}).then(function (){
		// 		let sqlm=`delete from "_menu" where menuid in ('documentmodule','Document')`;
		// 		return t.db.sequelize.query(sqlm,{type: t.db.sequelize.QueryTypes.DELETE});
		// 	});
		// }
		/*
		let sequelize=t.db.sequelize;
		let getNextTask=(obj)=>{
			const erprefnumber=obj.tasks[0].erprefnumber;
			const opname=obj.tasks[0].opname;
			let sql='select concat(SUBSTRING(cast(DATE_PART('year',coalesce(start,CURRENT_TIMESTAMP)) as varchar),3,2), right(concat('000',DATE_PART('doy',coalesce(start,CURRENT_TIMESTAMP))),3) ) bstring from task_lists where finish is null and erprefnumber=:erprefnumber and opname=:opname';
			return sequelize.query(sql,{replacements:{erprefnumber:erprefnumber,opname:opname}, type: sequelize.QueryTypes.SELECT}).then((result)=>{
				if(result.length>0){
					const bString=result[0].bstring;
					let sqlf='select * from task_lists where finish is null and taskinfo is not null and erprefnumber=:erprefnumber and opname>:opname';
					return sequelize.query(sqlf,{replacements:{erprefnumber:erprefnumber,opname:opname}, type: sequelize.QueryTypes.SELECT}).then((resultf)=>{
						if(resultf.length>0){
							let clients=resultf[0].client;
							let record_id=resultf[0].record_id;
							let taskinfo=resultf[0].taskinfo;
							let opnamenext=resultf[0].opname;
							if(typeof taskinfo!=='object'){
								taskinfo=JSON.parse(resultf[0].taskinfo);
							}
							if(typeof taskinfo.printer2!=='undefined'&&typeof taskinfo.printer2.veri2!=='undefined'){
								let veri2=taskinfo.printer2.veri2;
								let arr2=veri2.split(' ');
								arr2[arr2.length-1]=bString;
								taskinfo.printer2.veri2=arr2.join(' ');
								return {record_id:record_id,erprefnumber:erprefnumber,opname:opnamenext,taskinfo:taskinfo,clients:clients};
							}
						}else{
							return null;
						}
					}).then((obj)=>{
						if(obj!==null){
							return Promise.all([updateTaskInfo(obj)]);
						}
					})
				}else{
					return null;
				}
			});
		};
		
		let updateTaskInfo=(obj)=>{
			console.log('updateTaskInfo',obj);
			let sql="update task_lists set taskinfo=:taskinfo where erprefnumber=:erprefnumber and opname=:opname and record_id=:record_id";
			return sequelize.query(sql, { replacements:{taskinfo:JSON.stringify(obj.taskinfo),erprefnumber:obj.erprefnumber,opname:obj.opname,record_id:obj.record_id}, type: sequelize.QueryTypes.UPDATE})
			.catch(function(err){
				console.log(err);
				Sentry.captureException(err);
				return null;
			});
		};
		const tobj={
			tasks:[{
				erprefnumber:'101',
				opname:'16.14085-40'
			}]
		};
		return Promise.all([getNextTask(tobj)]);
		*/
	}
	log_error(msg){
		let t=this;
		let time=new timezoneJS.Date();
		return t.db.sequelize.query('insert into _log_exception (code,file,line,message,method,path,sessionid,time) values (:code,:file,:line,:message,:method,:path,:sessionid,:time)'
			, { replacements: { code:100,file:'',line:0,message:msg,method:'NODE',path:'',sessionid:0,time:time.toString('yyyy-MM-dd HH:mm:ss.SSS') },type: t.db.sequelize.QueryTypes.INSERT})
			.catch(function(err){
				//console.log('catch-log_error-'+err);
				Sentry.captureException(err);
			});
	}
	p_socket_server_device(){
		let t=this;
		let onMessage = function(obj) {
			try {
				switch (obj.event) {
				case 'started_socket_server_device':{
					t.mjd('socket_server_device '+obj.port+' Process PID:'+obj.pid);				
					break;
				}
				case 'socket_client_connect':{
					let _time= t.getFullLocalDateString();
					let _data=obj.data;
					return t.db.sequelize.query('SELECT * from clients where uuid=:uuid'
						, { replacements: { uuid: _data.dcsClientId },type: t.db.sequelize.QueryTypes.SELECT})
						.then((results)=>{
							if(results.length>0){
								return t.db.sequelize.query('update clients set connected=true, versionapp=:versionapp, versionshell=:versionshell, computername=:computername where uuid=:uuid'
									, { replacements: { uuid: _data.dcsClientId, versionapp:_data.versionApp, versionshell:_data.versionShell, computername:_data.computerName },type: t.db.sequelize.QueryTypes.UPDATE})
									.then(function(){
										try {
											t.proc.socket_server_watch.send({
												event: 'device_connect',
												ip: obj.ip
											});
											t.proc.socket_server_device.send({
												event: 'device_connect',
												ip: obj.ip
											});
										} catch (error) {
											Sentry.captureException(error);
										}
										return;
									})
									.catch(function(err){
										//console.log('catch-p_socket_server_device-'+err);
										Sentry.captureException(err);
									});
							}else{
								return t.db.sequelize.query('SELECT * from clients where ip=:ip'
									, { replacements: { ip: obj.ip },type: t.db.sequelize.QueryTypes.SELECT})
									.then((result_1)=>{
										if(result_1.length==0){
											//cihaz ilk kez bağlanıyor client kaydı oluştur
											//console.log('------------------client kaydı oluştur');
											//console.log(_data);
											let _obj_insert_client={
												code: ''
												,uuid: _data.dcsClientId
												,ip: obj.ip
												,isactive:true
												,connected:true
												,materialpreparation:false
												,pincode:t.randomIntInc(1000,9999)
												,versionapp:_data.versionApp
												,versionshell:_data.versionShell
												,computername:_data.computerName
												,record_id:uuidv4()
											};
											return t.db.sequelize.query('INSERT INTO '+
											' "clients" (code,uuid,ip,isactive,connected,materialpreparation,pincode,versionapp,versionshell,computername,record_id,create_user_id,created_at,update_user_id,updated_at) '+
											' VALUES (:code,:uuid,:ip,:isactive,:connected,:materialpreparation,:pincode,:versionapp,:versionshell,:computername,:record_id,0,\''+_time+'\',0,\''+_time+'\')'
											, { replacements: _obj_insert_client,type: t.db.sequelize.QueryTypes.INSERT})
												.catch(function(err){
													//console.log('catch-socket_client_connect-insert-'+err);
													Sentry.captureException(err);
												});
										}else{
											//console.log('!!!!-client kaydı oluşturma');
											//console.log(_data);
										}
									});
							}
						}).then(()=>{
							return t.db.sequelize.query('SELECT * from client_disconnects where finish is null and client=:client', 
								{ replacements: { client: _data.client_name },type: t.db.sequelize.QueryTypes.SELECT})
								.then((results_c_d)=>{
									if(results_c_d.length>0){
										return t.db.sequelize.query('update client_disconnects set finish=CURRENT_TIMESTAMP(0) where finish is null and client=:client', 
											{ replacements: { client: _data.client_name },type: t.db.sequelize.QueryTypes.UPDATE});
									}
								});
						});
				}
				case 'socket_client_ping':{
					//console.log('socket_client_ping:'+obj.ip);
					return t.db.sequelize.query('update clients set connected=true where ip=:ip and connected=false '
						, { replacements: { ip: obj.ip },type: t.db.sequelize.QueryTypes.UPDATE})
						.then(function(){
							try {
								t.proc.socket_server_watch.send({
									event: 'device_connect',
									ip: obj.ip
								});
								t.proc.socket_server_device.send({
									event: 'device_connect',
									ip: obj.ip
								});
							} catch (error) {
								Sentry.captureException(error);
							}
							return;
						})
						.catch(function(err){
							//console.log('catch-p_socket_server_device-'+err);
							Sentry.captureException(err);
						});
				}
				case 'socket_client_setuser':{
					let _code='';
					let _ip=obj.ip;
					//if(obj.ip==='172.16.41.101'){
					//	console.log('socket_client_setuser');
					//	console.log(obj.uuid);
					//	Sentry.addBreadcrumb({
					//		category: 'socket_client_setuser',
					//		message: 'sql:' + JSON.stringify(obj),
					//		level: Sentry.Severity.Error
					//	});
					//	Sentry.captureMessage('[socket_client_setuser] ' + obj);
					//}
					return t.db.sequelize.query('SELECT * from clients where uuid=:uuid'
						, { replacements: { uuid: obj.uuid },type: t.db.sequelize.QueryTypes.SELECT})
						.then((results)=>{
							if(results.length>0){
								_code=results[0].code;
								try {
									t.proc.socket_server_device.send({
										event: 'socket_client_setuser_response',
										data: results[0]
									});
								} catch (error) {
									Sentry.captureException(error);
								}
							}
							return;
						}).then(()=>{
							for(let i=0;i<(_code===''?6:obj.sync_data.length);i++){
								let item_sync=obj.sync_data[i];
								let _tablename=item_sync.tableName;
								let arr_exclude=['client_lost_details','client_params','client_production_details','control_answers','task_cases','task_imports','task_reject_details'];
								if(_tablename!='undefined'&&arr_exclude.indexOf(_tablename)===-1){
									let _updated=item_sync.strlastupdate;
									let _w='';
									if(t._arr_tables.indexOf(_tablename)>-1){
										if(_tablename=='reworks'){
											_w=' and ("client"=\''+_code+'\' or "client" is null) ';
										}else if(_tablename==='task_lists'){
											_w=' and client ilike \'%'+_code+'%\' and "finish" is null ';
										}else{
											_w=' and "client"=\''+_code+'\' ';
											if(t._arr_tables_not_day.indexOf(_tablename)==-1){
												if(t._arr_tables_finish_isnull.indexOf(_tablename)>-1){
													_w+=' and ("day">(CURRENT_DATE-INTERVAL \'1 DAY\')::date or "finish" is null) ';
												}else{
													_w+=' and "day">(CURRENT_DATE-INTERVAL \'1 DAY\')::date ';
												}
											}else{
												if(t._arr_tables_finish_isnull.indexOf(_tablename)>-1){
													_w+=' and "finish" is null ';
												}
											}
										}
									}
									if(_tablename==='client_production_details'){
										_w=_w+' and "type" not like \'c_s%\' ';
									}
									//if(_tablename==='task_imports'){
									//	_w=_w+' and "deadline">(CURRENT_DATE-INTERVAL \'2 DAY\')::date ';
									//}
									if(_tablename==='task_lists'){
										_w=_w+' and ("deadline">(CURRENT_DATE-INTERVAL \'2 DAY\')::date or "finish" is null) ';
									}
									t.arr_sync_update_records.push({uuid:obj.uuid,ip:obj.ip,tablename:_tablename,updated:_updated,w:_w,queue:((_code!==''&&_code!==null?_code:_ip)+'_server')});
								}
							}
							return null;
						});
				}
				case 'socket_client_disconnect':{
					let client_name='';
					return t.db.sequelize.query('update clients set connected=false where uuid=:uuid'
						, { replacements: { uuid: obj.uuid },type: t.db.sequelize.QueryTypes.UPDATE})
						.then(function(){
							try {
								t.proc.socket_server_watch.send({
									event: 'device_disconnect',
									ip: obj.ip
								});
								t.proc.socket_server_device.send({
									event: 'device_disconnect',
									ip: obj.ip
								});
							} catch (error) {
								Sentry.captureException(error);
							}
							return;
						})
						.then(()=>{
							return t.db.sequelize.query('SELECT * from clients where uuid=:uuid', 
								{ replacements: { uuid: obj.uuid },type: t.db.sequelize.QueryTypes.SELECT})
								.then((results_c)=>{
									if(results_c.length>0){
										let row=results_c[0];
										client_name=row.code;
									}
								});
						})
						.then(()=>{
							return t.db.sequelize.query('SELECT * from client_disconnects where finish is null and client=:client', 
								{ replacements: { client: client_name },type: t.db.sequelize.QueryTypes.SELECT})
								.then((results_c_d)=>{
									if(results_c_d.length>0){
										return t.db.sequelize.query('update client_disconnects set finish=CURRENT_TIMESTAMP(0) where finish is null and client=:client', 
											{ replacements: { client: client_name },type: t.db.sequelize.QueryTypes.UPDATE});
									}
								});
						})
						.then(()=>{
							return t.db.sequelize.query('INSERT INTO client_disconnects (client,start,create_user_id,created_at) values(:client,CURRENT_TIMESTAMP(0),0,CURRENT_TIMESTAMP(0))', 
								{ replacements: { client: client_name },type: t.db.sequelize.QueryTypes.INSERT});
						})
						.catch(function(err){
							//console.log('catch-socket_client_disconnect-'+err);
							Sentry.captureException(err);
						});
				}
				case 'res_device_is_online':{
					if(obj.connected){
						if(obj.parent.event==='lockpc'||obj.parent.event==='unlockpc'){
							try {
								t.proc.socket_server_watch.send({
									event: 'res_'+obj.parent.event,
									msg:{success:'true',message:'Cihaza bilgi gönderildi.IP:'+obj.parent.ip},
									ip:obj.parent.watchIP
								});
								t.proc.socket_server_device.send({
									event: 'res_'+obj.parent.event,
									routeparams:(obj.parent.event==='lockpc'?'screen_lockpc':'screen_unlockpc'),
									ip:obj.parent.ip
								});
							} catch (error) {
								Sentry.captureException(error);
							}
						}
						if(obj.parent.event==='updateApp'){
							try {
								t.proc.socket_server_watch.send({
									event: 'res_'+obj.parent.event,
									msg:{success:'true',message:'Cihaza bilgi gönderildi.IP:'+obj.parent.ip},
									ip:obj.parent.watchIP
								});
							} catch (error) {
								Sentry.captureException(error);
							}
						}
					}else{
						if(obj.parent.event==='lockpc'||obj.parent.event==='unlockpc'){
							try {
								t.proc.socket_server_watch.send({
									event: 'res_'+obj.parent.event,
									msg:{success:'false',message:'Cihaz bağlı değil!IP:'+obj.parent.ip},
									ip:obj.parent.watchIP
								});
							} catch (error) {
								Sentry.captureException(error);
							}
						}
						if(obj.parent.event==='updateApp'){
							try {
								t.proc.socket_server_watch.send({
									event: 'res_'+obj.parent.event,
									msg:{success:'false',message:'Cihaz bağlı değil!IP:'+obj.parent.ip},
									ip:obj.parent.watchIP
								});
							} catch (error) {
								Sentry.captureException(error);
							}
						}
					}
					break;
				}
				case 'res_watch_calc_tempo':{
					try {
						t.proc.socket_server_watch.send({
							event: 'watch_calc_tempo',
							data:obj.data,
							ip:obj.ip
						});
					} catch (error) {
						Sentry.captureException(error);
					}
					break;
				}
				case 'res_watch_info':{
					try {
						t.proc.socket_server_watch.send({
							event: 'watch_info',
							data:obj.data,
							ip:obj.ip
						});
					} catch (error) {
						Sentry.captureException(error);
					}
					break;
				}
				case 'res_device_io_info':{
					try {
						t.proc.socket_server_watch.send({
							event: 'device_io_info',
							data:obj.data,
							ip:obj.ip
						});
					} catch (error) {
						Sentry.captureException(error);
					}
					break;
				}
				case 'req_update_check':{
					if(t.updates){
						if(t.updates.length>0){
							let _updateRequired=false;
							for(let i=0;i<t.updates.length;i++){
								if(!_updateRequired){
									let _item=t.updates[i];
									if(_item._major>obj.major
										||(_item._major==obj.major&&_item._minor>obj.minor)
										||(_item._major==obj.major&&_item._minor==obj.minor&&_item._patch>obj.patch)
										||(_item._major==obj.major&&_item._minor==obj.minor&&_item._patch==obj.patch&&_item._build>obj.build)
									){
										_updateRequired=true;
										try {
											t.proc.socket_server_device.send({
												event: 'res_update_check',
												ip: obj.ip,
												path: _item.path,
												hash: _item.hash,
												version: _item._major+'.'+_item._minor+'.'+_item._patch+'.'+_item._build
											});
										} catch (error) {
											Sentry.captureException(error);
										}
									}
								}
							}
						}
					}
					break;
				}
				case 'res_getDataAllClients':{
					try {
						t.proc.socket_server_watch.send(obj);
					} catch (error) {
						Sentry.captureException(error);
					}
					break;
				}
				case 'req_control_delivery_package_info':{
					if(t.conERP){
						if((obj.barcode==null||obj.barcode=='null')&&t.ip!=='10.10.0.10'&&t.ip!=='192.168.1.40'&&t.ip !== '192.168.1.10'){
							t.log_error('Kasa kaydı bulunamadı.'+obj.client+' KD:'+obj.erprefnumber+' KASA:'+obj.barcode+'...');
							try {
								t.proc.socket_server_device.send({
									event: 'res_control_delivery_package_info',
									ip: obj.ip,
									result: 'err',
									message: 'Kasa kaydı bulunamadı.KD:'+obj.erprefnumber+' KASA:'+obj.barcode+'...',
									casenumber: obj.barcode
								});
							} catch (error) {
								Sentry.captureException(error);
							}
						}else{
							let sql='SELECT cast(x1.xtksk1_qty_ord as integer) "packcapacity", x1.xtksk1_trpart "product", x1.xtksk1_trlot "lotname", x1.xtksk1_miktarstr "casename",x1.xtksk1_nbr "casenumber",x1.xtksk1_lot "erprefnumber" '+
							'FROM pub.xtksk1_mstr x1 WHERE x1.xtksk1_domain=\'ERMETAL\' and x1.xtksk1_lot=\''+obj.erprefnumber+'\' order by x1.xtksk1_nbr WITH (NOLOCK)';
							if(t.ip=='10.10.0.10'||t.ip=='192.168.1.40'|| t.ip === '192.168.1.10'){
								sql='SELECT LTRIM(RTRIM(D.KOD)) AS "casenumber", '+
								'		LTRIM(RTRIM(D.SKOD)) AS "product", '+
								'		LTRIM(RTRIM(D.MPSNO)) AS "erprefnumber", '+
								'		LTRIM(RTRIM(D.REFTEXT02)) AS "casename", '+
								'		D.SF_MIKTAR AS "packcapacity", '+
								'		D.GK_3 "durum" '+
								' FROM D7KIDSLB D '+
								'	LEFT JOIN STOK00 S0 ON S0.KOD = D.SKOD '+
								'	WHERE D.MPSNO <> \'\' '+
								'		AND S0.GK_2 IN(\'PRES\', \'PUNTA\', \'KAYNAK\', \'KYNK-ROBOT\') '+
								'		AND D.GK_4 = \'VERIMOT\' '+
								'		AND LTRIM(RTRIM(D.MPSNO)) = \''+obj.erprefnumber+'\' '+
								' ORDER BY LTRIM(RTRIM(D.KOD))';
							}
							t.conERP.query(sql, function (err, data) {
								if (err){
									Sentry.addBreadcrumb({
										category: 'odbc_error',
										message: sql,
										level: Sentry.Severity.Info,
									});
									Sentry.captureException(err);
									t.log_error(err.toString());
									try {
										t.proc.socket_server_device.send({
											event: 'res_control_delivery_package_info',
											ip: obj.ip,
											result: 'err',
											message: 'QAD Hata Oluştu</br>'+err.toString()+'</br>'+sql,
											casenumber: obj.barcode
										});
									} catch (error) {
										Sentry.captureException(error);
										return;
									}
									return;
								}
								if(data.length>0){
									let _f=false;
									let _tmp=[];
									for(let i=0;i<data.length;i++){
										data[i]['caselot']=(i+1)+'/'+data.length;
										//okutulan kasanın o kd için olup olmadığı kontrolü
										if(_f==false&&data[i]['casenumber']==obj.barcode){
											_f=true;
										}
									}
									if(t.ip==='10.10.0.10'||t.ip=='192.168.1.40'|| t.ip === '192.168.1.10'){
										_f=true;
									}
									if(_f==false){
										t.log_error('Okutulan etiket hatalı.'+obj.client+' KD:'+obj.erprefnumber+' KASA:'+obj.barcode+'...');
										try {
											t.proc.socket_server_device.send({
												event: 'res_control_delivery_package_info',
												ip: obj.ip,
												result: 'err',
												message: 'Okutulan etiketin KD’si seçtiğiniz işin KD’si ile uyuşmuyor.KD:'+obj.erprefnumber+' KASA:'+obj.barcode+'...',
												casenumber: obj.barcode
											});
										} catch (error) {
											Sentry.captureException(error);
										}
										return;
									}
									if(t.ip==='10.10.0.10'||t.ip=='192.168.1.40'|| t.ip === '192.168.1.10'){
										let _data=[];
										for(let i=0;i<data.length;i++){
											if(data[i]['durum']==='URETILMEDI'){
												_data.push(data[i]);
											}
										}
										data=_data;
										if(data.length>0){
											for(let i=0;i<data.length;i++){
												_tmp.push(data[i]['casenumber']);
											}
											return t.db.sequelize.query('SELECT * from task_cases where casenumber in (:casenumber) and status not in (:status) '
												, { replacements: { casenumber: _tmp,status: ['CLOSE','CLOSED','CANCELED','WAIT_FOR_DELIVERY','DELETED','SYSTEMCLOSE','LABELDELETED'] },type: t.db.sequelize.QueryTypes.SELECT})
												.then((results)=>{
													if(results.length>0){
														for(let k=0;k<results.length;k++){
															let _idx_tc=t.getIndex(data,'casenumber',results[k]['casenumber']);
															if(_idx_tc>-1){
																data.splice(_idx_tc,1);
															}
														}
													}
													if(data.length>0){
														let _tmp=[];
														for(let i=0;i<data.length;i++){
															let idx=t.getIndex(_tmp,'casenumber',data[i].casenumber);
															if(idx==-1){
																_tmp.push(data[i]);
															}
														}
														try {
															t.proc.socket_server_device.send({
																event: 'res_control_delivery_package_info',
																ip: obj.ip,
																result: 'ok',
																message: 'İşlem tamamlandı.',
																data: _tmp
															});
														} catch (error) {
															Sentry.captureException(error);
														}
													}else{
														try {
															t.proc.socket_server_device.send({
																event: 'res_control_delivery_package_info',
																ip: obj.ip,
																result: 'ok',
																message: 'İşlem tamamlandı.',
																data: []
															});
														} catch (error) {
															Sentry.captureException(error);
														}
													}
													return;
												});
										}else{
											t.log_error('Kasa kaydı bulunamadı.'+obj.client+' KD:'+obj.erprefnumber+' KASA:'+obj.barcode+'...');
											try {
												t.proc.socket_server_device.send({
													event: 'res_control_delivery_package_info',
													ip: obj.ip,
													result: 'err',
													message: 'Erp sisteminde tesellüm edilmemiş kasa kaydı bulunamadı.KD:'+obj.erprefnumber+' KASA:'+obj.barcode+'...',
													casenumber: obj.barcode
												});
											} catch (error) {
												Sentry.captureException(error);
											}
										}
									}else{
										sql='select xtksk_nbr from pub.xtksk_mstr where xtksk_domain=\'ERMETAL\' and xtksk_lot=\''+obj.erprefnumber+'\' order by xtksk_nbr WITH (NOLOCK)';
										t.conERP.query(sql, function (err, data_child) {
											if (err){
												Sentry.addBreadcrumb({
													category: 'odbc_error',
													message: sql,
													level: Sentry.Severity.Info,
												});
												Sentry.captureException(err);
												t.log_error(err.toString());
												try {
													t.proc.socket_server_device.send({
														event: 'res_control_delivery_package_info',
														ip: obj.ip,
														result: 'err',
														message: 'QAD Hata Oluştu</br>'+err.toString()+'</br>'+sql,
														casenumber: obj.barcode
													});
												} catch (error) {
													Sentry.captureException(error);
												}
												return;
											}
											if(data_child.length>0){//erp sisteminde tesellüm edilmiş kasalar var
												for(let j=0;j<data_child.length;j++){
													let _idx=t.getIndex(data,'casenumber',data_child[j]['xtksk_nbr']);
													if(_idx>-1){
														data.splice(_idx,1);
													}
												}
											}
											if(data.length>0){
												for(let i=0;i<data.length;i++){
													_tmp.push(data[i]['casenumber']);
												}
												let _str='\''+_tmp.join('\',\'')+'\'';
												return t.db.sequelize.query('SELECT * from task_cases where casenumber in (:casenumber)'
													, { replacements: { casenumber: _str },type: t.db.sequelize.QueryTypes.SELECT})
													.then((results)=>{
														if(results.length>0){
															for(let k=0;k<results.length;k++){
																let _idx_tc=t.getIndex(data,'casenumber',results[k]['casenumber']);
																if(_idx_tc>-1){
																	data.splice(_idx_tc,1);
																}
															}
														}
														if(data.length>0){
															let _tmp=[];
															for(let i=0;i<data.length;i++){
																let idx=t.getIndex(_tmp,'casenumber',data[i].casenumber);
																if(idx==-1){
																	_tmp.push(data[i]);
																}
															}
															try {
																t.proc.socket_server_device.send({
																	event: 'res_control_delivery_package_info',
																	ip: obj.ip,
																	result: 'ok',
																	message: 'İşlem tamamlandı.',
																	data: _tmp
																});
															} catch (error) {
																Sentry.captureException(error);
															}
														}else{
															t.log_error('Sistemde tesellüm edilmemiş kasa kaydı bulunamadı.KD:'+obj.erprefnumber+' KASA:'+obj.barcode+'...');
															try {
																t.proc.socket_server_device.send({
																	event: 'res_control_delivery_package_info',
																	ip: obj.ip,
																	result: 'err',
																	message: 'Sistemde tesellüm edilmemiş kasa kaydı bulunamadı.KD:'+obj.erprefnumber+' KASA:'+obj.barcode+'...',
																	casenumber: obj.barcode
																});
															} catch (error) {
																Sentry.captureException(error);
															}
														}
														return;
													});
											}else{
												t.log_error('Kasa kaydı bulunamadı.'+obj.client+' KD:'+obj.erprefnumber+' KASA:'+obj.barcode+'...');
												try {
													t.proc.socket_server_device.send({
														event: 'res_control_delivery_package_info',
														ip: obj.ip,
														result: 'err',
														message: 'Erp sisteminde tesellüm edilmemiş kasa kaydı bulunamadı.KD:'+obj.erprefnumber+' KASA:'+obj.barcode+'...',
														casenumber: obj.barcode
													});
												} catch (error) {
													Sentry.captureException(error);
												}
											}
										});
									}
								}else{
									t.log_error('Kasa kaydı bulunamadı.'+obj.client+' KD:'+obj.erprefnumber+' KASA:'+obj.barcode+'...');
									try {
										t.proc.socket_server_device.send({
											event: 'res_control_delivery_package_info',
											ip: obj.ip,
											result: 'err',
											message: 'Erp sisteminde kasa kaydı bulunamadı.KD:'+obj.erprefnumber+' KASA:'+obj.barcode+'...',
											casenumber: obj.barcode
										});
									} catch (error) {
										Sentry.captureException(error);
									}
								}
							});
						}
					}else{
						if(t.ip=='192.168.1.40'|| t.ip === '192.168.1.10'){
							t.mjd('Kasa kaydı kontrolü.KD:'+obj.erprefnumber+' KASA:'+obj.barcode+'...');
							//okutulan kasa sistemde daha önce okutulmuş mu kontrol ediliyor
							return t.db.sequelize.query('SELECT * from task_cases where casenumber=:casenumber'
								, { replacements: { casenumber: obj.barcode },type: t.db.sequelize.QueryTypes.SELECT})
								.then((/*results*/)=>{
									let data=JSON.parse('[{"packcapacity":5,"product":"16.14085","lotname":"U180305.0131","casename":"ERM_002","casenumber":"1803050131","erprefnumber":"71944404","caselot":"1/3"},{"packcapacity":5,"product":"16.14085","lotname":"U180305.0131","casename":"ERM_002","casenumber":"1803050132","erprefnumber":"71944404","caselot":"2/3"},{"packcapacity":5,"product":"16.14085","lotname":"U180305.0132","casename":"ERM_002","casenumber":"1803050133","erprefnumber":"71944404","caselot":"3/3"}]');
									let _tmp=[];
									for(let i=0;i<data.length;i++){
										let idx=t.getIndex(_tmp,'casenumber',data[i].casenumber);
										if(idx==-1){
											_tmp.push(data[i]);
										}
									}
									try {
										t.proc.socket_server_device.send({
											event: 'res_control_delivery_package_info',
											ip: obj.ip,
											result: 'ok',
											message: 'İşlem tamamlandı.',
											data: _tmp
										});
									} catch (error) {
										Sentry.captureException(error);
									}
									return;
								});
						}else{
							t.log_error('ERP bağlantı yok.'+obj.client+' KD:'+obj.erprefnumber+' KASA:'+obj.barcode+'...');
							try {
								t.proc.socket_server_device.send({
									event: 'res_control_delivery_package_info',
									ip: obj.ip,
									result: 'err',
									message: 'ERP bağlantısı sağlanamadı',
									casenumber: obj.barcode
								});
							} catch (error) {
								Sentry.captureException(error);
							}
						}
					}
					break;
				}
				case 'req_case_delivery_manual':{
					let _barkod=obj.barcode;
					let _erprefnumber=obj.erprefnumber;
					let _client=obj.client;
					if(t.conERP){
						if(_barkod==null||_barkod=='null'||_erprefnumber==null||_erprefnumber=='null'){
							t.mjd('Kasa kaydı bulunamadı-MT.'+_client+' KASA:'+_barkod+' Erp Ref:'+_erprefnumber+'...');
							try {
								t.proc.socket_server_device.send({
									event: 'res_case_delivery_manual',
									ip: obj.ip,
									result: 'err',
									message: 'Kasa kaydı bulunamadı. KASA:'+_barkod+' Erp Ref:'+_erprefnumber+'...',
									casenumber: _barkod,
									erprefnumber:_erprefnumber
								});
							} catch (error) {
								Sentry.captureException(error);
							}
						}else{
							let _part='';
							let sql='SELECT x1.xtksk1_trpart "product", x1.xtksk1_trlot "lotname",x1.xtksk1_nbr "casenumber",x1.xtksk1_lot "erprefnumber" '+
							'FROM pub.xtksk1_mstr x1 WHERE x1.xtksk1_domain=\'ERMETAL\' and x1.xtksk1_nbr=\''+_barkod+'\' order by x1.xtksk1_nbr WITH (NOLOCK)';
							t.conERP.query(sql, function (err, data) {
								if(err){
									Sentry.addBreadcrumb({
										category: 'odbc_error',
										message: sql,
										level: Sentry.Severity.Info,
									});
									Sentry.captureException(err);
									try {
										t.proc.socket_server_device.send({
											event: 'res_case_delivery_manual',
											ip: obj.ip,
											result: 'err',
											message: 'QAD Hata Oluştu</br>'+err.toString()+'</br>'+sql,
											casenumber: _barkod,
											erprefnumber:_erprefnumber
										});
									} catch (error) {
										Sentry.captureException(error);
										return;
									}
									return;
								}
								if(data.length>0){
									if(data[0]['erprefnumber']==_erprefnumber){
										_part=data[0]['product'];
										sql='select xtksk_nbr from pub.xtksk_mstr where xtksk_domain=\'ERMETAL\' and xtksk_nbr=\''+_barkod+'\' order by xtksk_nbr WITH (NOLOCK)';
										t.conERP.query(sql, function (err2, data2) {
											if(err2){
												Sentry.addBreadcrumb({
													category: 'odbc_error',
													message: sql,
													level: Sentry.Severity.Info,
												});
												Sentry.captureException(err2);
												try {
													t.proc.socket_server_device.send({
														event: 'res_case_delivery_manual',
														ip: obj.ip,
														result: 'err',
														message: 'QAD Hata Oluştu</br>'+err2.toString()+'</br>'+sql,
														casenumber: _barkod,
														erprefnumber:_erprefnumber
													});
												} catch (error) {
													Sentry.captureException(error);
													return;
												}
												return;
											}
											if(data2.length>0){//erp sisteminde tesellüm edilmiş kasalar var
												try {
													t.proc.socket_server_device.send({
														event: 'res_case_delivery_manual',
														ip: obj.ip,
														result: 'err',
														message: 'Kasa kartı daha önce okutulmuş-1',
														casenumber: _barkod,
														erprefnumber:_erprefnumber
													});
												} catch (error) {
													Sentry.captureException(error);
												}
											}else{
												return t.db.sequelize.query('SELECT * from task_lists where erprefnumber=:erprefnumber /*and taskfromerp=true*/'
													, { replacements: { erprefnumber:_erprefnumber },type: t.db.sequelize.QueryTypes.SELECT}).then((results_tl)=>{
													if(results_tl.length>0){
														return fs_writeFile(path_export+'tesellum/'+_barkod+'-M.txt', _barkod).then(function(){
															return t.db.sequelize.query('update task_cases set status=:status where casenumber=:casenumber and erprefnumber=:erprefnumber'
																, { replacements: { casenumber: _barkod,erprefnumber:_erprefnumber,status:'DELIVERED' },type: t.db.sequelize.QueryTypes.UPDATE})
																.then(function(){
																	try {
																		t.proc.socket_server_device.send({
																			event: 'res_case_delivery_manual',
																			ip: obj.ip,
																			result: 'ok',
																			message: 'İşlem tamamlandı.',
																			casenumber: _barkod,
																			erprefnumber:_erprefnumber
																		});
																	} catch (error) {
																		Sentry.captureException(error);
																	}
																	return;
																});
														}).catch((error) => {
															Sentry.captureException(error);
															try {
																t.proc.socket_server_device.send({
																	event: 'res_case_delivery_manual',
																	ip: obj.ip,
																	result: 'err',
																	message: 'Dosya yazarken hata oluştu tekrar deneyiniz.',
																	casenumber: _barkod,
																	erprefnumber:_erprefnumber
																});
															} catch (error) {
																Sentry.captureException(error);
															}
														});
													}else{
														return t.db.sequelize.query('select * from clients where uuid is not null and code=:code and workflow=:workflow'
															, { replacements: { code:_client,workflow:'El İşçiliği' },type: t.db.sequelize.QueryTypes.SELECT}).then((results_c)=>{
															if(results_c.length>0){
																return t.db.sequelize.query('select * from reworks where finish is null and opcode=:opcode'
																	, { replacements: { opcode:_part },type: t.db.sequelize.QueryTypes.SELECT}).then((/*results_r*/)=>{
																	return fs_writeFile(path_export+'tesellum/'+_barkod+'-M.txt', _barkod).then(function(){
																		try {
																			t.proc.socket_server_device.send({
																				event: 'res_case_delivery_manual',
																				ip: obj.ip,
																				result: 'ok',
																				message: 'İşlem tamamlandı.',
																				casenumber: _barkod,
																				erprefnumber:_erprefnumber
																			});
																		} catch (error) {
																			Sentry.captureException(error);
																		}
																		return;
																	}).catch((error) => {
																		Sentry.captureException(error);
																		try {
																			t.proc.socket_server_device.send({
																				event: 'res_case_delivery_manual',
																				ip: obj.ip,
																				result: 'err',
																				message: 'Dosya yazarken hata oluştu tekrar deneyiniz..',
																				casenumber: _barkod,
																				erprefnumber:_erprefnumber
																			});
																		} catch (error) {
																			Sentry.captureException(error);
																		}
																	});
																});
															}else{
																try {
																	t.proc.socket_server_device.send({
																		event: 'res_case_delivery_manual',
																		ip: obj.ip,
																		result: 'err',
																		message: 'Girilen kartın KD numarası planda olmadığından tesellüm yapılamaz!',
																		casenumber: _barkod,
																		erprefnumber:_erprefnumber
																	});
																} catch (error) {
																	Sentry.captureException(error);
																}
																return;
															}
														});
													}
												});
											}
										});
									}else{
										try {
											t.proc.socket_server_device.send({
												event: 'res_case_delivery_manual',
												ip: obj.ip,
												result: 'err',
												message: 'Kasa kartı ve KD numarası uyumsuz.KD:'+_erprefnumber+' KASA:'+_barkod+'...',
												casenumber: _barkod,
												erprefnumber:_erprefnumber
											});
										} catch (error) {
											Sentry.captureException(error);
										}
										return;
									}
								}else{
									try {
										t.proc.socket_server_device.send({
											event: 'res_case_delivery_manual',
											ip: obj.ip,
											result: 'err',
											message: 'QAD kasa kaydı bulunamadı.KD:'+_erprefnumber+' KASA:'+_barkod+'...',
											casenumber: _barkod,
											erprefnumber:_erprefnumber
										});
									} catch (error) {
										Sentry.captureException(error);
									}
									return;
								}
							});
						}
					}else{
						t.mjd('Kasa kaydı kontrolü-MT. KASA:'+_barkod+'...');
						//okutulan kasa sistemde daha önce okutulmuş mu kontrol ediliyor
						return t.db.sequelize.query('SELECT * from task_cases where casenumber=:casenumber and erprefnumber=:erprefnumber '
							, { replacements: { casenumber: _barkod,erprefnumber:_erprefnumber},type: t.db.sequelize.QueryTypes.SELECT})
							.then((results)=>{
								if(results.length==0){
									try {
										t.proc.socket_server_device.send({
											event: 'res_case_delivery_manual',
											ip: obj.ip,
											result: 'err',
											message: 'Kasa kartı ve ERP Ref numarası uyumsuz-2-'+_erprefnumber+' '+_barkod,
											casenumber: _barkod,
											erprefnumber:_erprefnumber
										});
									} catch (error) {
										Sentry.captureException(error);
									}
								}else{
									if(results[0]['status']=='DELIVERED'){
										try {
											t.proc.socket_server_device.send({
												event: 'res_case_delivery_manual',
												ip: obj.ip,
												result: 'err',
												message: 'Kasa kartı daha önce okutulmuş-2',
												casenumber: _barkod,
												erprefnumber:_erprefnumber
											});
										} catch (error) {
											Sentry.captureException(error);
										}
									}else{
										return t.db.sequelize.query('SELECT * from task_lists where erprefnumber=:erprefnumber and taskfromerp=true'
											, { replacements: { erprefnumber:_erprefnumber },type: t.db.sequelize.QueryTypes.SELECT})
											.then((results_tl)=>{
												if(results_tl.length>0){
													return fs_writeFile(path_export+'tesellum/'+_barkod+'-M.txt', _barkod)
														.then(function(){
															return t.db.sequelize.query('update task_cases set status=:status where casenumber=:casenumber and erprefnumber=:erprefnumber'
																, { replacements: { casenumber: _barkod,erprefnumber:_erprefnumber,status:'DELIVERED' },type: t.db.sequelize.QueryTypes.UPDATE})
																.then(function(){
																	try {
																		t.proc.socket_server_device.send({
																			event: 'res_case_delivery_manual',
																			ip: obj.ip,
																			result: 'ok',
																			message: 'İşlem tamamlandı.',
																			casenumber: _barkod,
																			erprefnumber:_erprefnumber
																		});
																	} catch (error) {
																		Sentry.captureException(error);
																	}
																	return;
																});
														})
														.catch((error) => {
															t.proc.socket_server_device.send({
																event: 'res_case_delivery_manual',
																ip: obj.ip,
																result: 'err',
																message: 'Dosya yazarken hata oluştu tekrar deneyiniz.',
																casenumber: _barkod,
																erprefnumber:_erprefnumber
															});
															Sentry.captureException(error);
														});
												}else{
													try {
														t.proc.socket_server_device.send({
															event: 'res_case_delivery_manual',
															ip: obj.ip,
															result: 'err',
															message: 'Girilen kartın ERP Ref numarası planda olmadığından tesellüm yapılamaz',
															casenumber: _barkod,
															erprefnumber:_erprefnumber
														});
													} catch (error) {
														Sentry.captureException(error);
													}
												}
											});
									}
								}
								return;
							});
					}
					break;
				}
				case 'req_refresh_delivery_package_info':{
					if(t.conERP){
						if( (obj.erprefnumbers.length>0)&&(t.ip=='10.10.0.10'||t.ip=='192.168.1.40'|| t.ip === '192.168.1.10') ){
							let _str='\''+obj.erprefnumbers.join('\',\'')+'\'';
							let sql='SELECT LTRIM(RTRIM(D.KOD)) AS "casenumber", '+
							'		LTRIM(RTRIM(D.SKOD)) AS "product", '+
							'		LTRIM(RTRIM(D.MPSNO)) AS "erprefnumber", '+
							'		LTRIM(RTRIM(D.REFTEXT02)) AS "casename", '+
							'		D.SF_MIKTAR AS "packcapacity", '+
							'		D.GK_3 "durum" '+
							' FROM D7KIDSLB D '+
							'	LEFT JOIN STOK00 S0 ON S0.KOD = D.SKOD '+
							'	WHERE D.MPSNO <> \'\' '+
							'		AND S0.GK_2 IN(\'PRES\', \'PUNTA\', \'KAYNAK\', \'KYNK-ROBOT\') '+
							'		AND D.GK_4 = \'VERIMOT\' '+
							'		AND LTRIM(RTRIM(D.MPSNO)) in ('+_str+') '+
							' ORDER BY LTRIM(RTRIM(D.KOD))';
							t.conERP.query(sql, function (err, data) {
								if (err){
									Sentry.addBreadcrumb({
										category: 'odbc_error',
										message: sql,
										level: Sentry.Severity.Info,
									});
									Sentry.captureException(err);
									t.log_error(err.toString());
									try {
										t.proc.socket_server_device.send({
											event: 'res_refresh_delivery_package_info',
											ip: obj.ip,
											result: 'err',
											message: 'ERP Hata Oluştu</br>'+err.toString()+'</br>'+sql,
											casenumber: obj.barcode
										});
									} catch (error) {
										Sentry.captureException(error);
										return;
									}
									return;
								}
								if(data.length>0){
									let _tmp=[];
									if(t.ip==='10.10.0.10'||t.ip=='192.168.1.40'|| t.ip === '192.168.1.10'){
										let _data=[];
										for(let i=0;i<data.length;i++){
											if(data[i]['durum']==='URETILMEDI'){
												_data.push(data[i]);
											}
										}
										data=_data;
										if(data.length>0){
											for(let i=0;i<data.length;i++){
												_tmp.push(data[i]['casenumber']);
											}
											return t.db.sequelize.query('SELECT * from task_cases where casenumber in (:casenumber) and status not in (:status) '
												, { replacements: { casenumber: _tmp,status: ['CLOSE','CLOSED','CANCELED','WAIT_FOR_DELIVERY','DELETED','SYSTEMCLOSE','LABELDELETED'] },type: t.db.sequelize.QueryTypes.SELECT})
												.then((results)=>{
													if(results.length>0){
														for(let k=0;k<results.length;k++){
															let _idx_tc=t.getIndex(data,'casenumber',results[k]['casenumber']);
															if(_idx_tc>-1){
																data.splice(_idx_tc,1);
															}
														}
													}
													if(data.length>0){
														let _tmp=[];
														for(let i=0;i<data.length;i++){
															let idx=t.getIndex(_tmp,'casenumber',data[i].casenumber);
															if(idx==-1){
																_tmp.push(data[i]);
															}
														}
														try {
															t.proc.socket_server_device.send({
																event: 'res_refresh_delivery_package_info',
																ip: obj.ip,
																result: 'ok',
																message: 'İşlem tamamlandı.',
																data: _tmp
															});
														} catch (error) {
															Sentry.captureException(error);
														}
													}else{
														try {
															t.proc.socket_server_device.send({
																event: 'res_refresh_delivery_package_info',
																ip: obj.ip,
																result: 'ok',
																message: 'İşlem tamamlandı.',
																data: []
															});
														} catch (error) {
															Sentry.captureException(error);
														}
													}
													return;
												});
										}else{
											t.log_error('Kasa kaydı bulunamadı.'+obj.client+' KD:'+obj.erprefnumber+' KASA:'+obj.barcode+'...');
											try {
												t.proc.socket_server_device.send({
													event: 'res_refresh_delivery_package_info',
													ip: obj.ip,
													result: 'err',
													message: 'Erp sisteminde tesellüm edilmemiş kasa kaydı bulunamadı.KD:'+obj.erprefnumber+' KASA:'+obj.barcode+'...',
													casenumber: obj.barcode
												});
											} catch (error) {
												Sentry.captureException(error);
											}
										}
									}
								}else{
									t.log_error('Kasa kaydı bulunamadı.'+obj.client+' ERP Ref:'+obj.erprefnumber+' KASA:'+obj.barcode+'...');
									try {
										t.proc.socket_server_device.send({
											event: 'res_refresh_delivery_package_info',
											ip: obj.ip,
											result: 'err',
											message: 'Erp sisteminde kasa kaydı bulunamadı.ERP Ref:'+obj.erprefnumber+' KASA:'+obj.barcode+'...',
											casenumber: obj.barcode
										});
									} catch (error) {
										Sentry.captureException(error);
									}
								}
							});
						}else{
							try {
								t.proc.socket_server_device.send({
									event: 'res_refresh_delivery_package_info',
									ip: obj.ip,
									result: 'err',
									message: 'Tesellüm yapılacak bir iş emri bulunamadı. Operasyonun ürün ağacı verisini kontrol ediniz.',
									casenumber: obj.barcode
								});
							} catch (error) {
								Sentry.captureException(error);
							}
						}
					}else{
						if(t.ip=='192.168.1.40'|| t.ip === '192.168.1.10'){
							t.mjd('Kasa kaydı kontrolü.KD:'+obj.erprefnumber+' KASA:'+obj.barcode+'...');
							//okutulan kasa sistemde daha önce okutulmuş mu kontrol ediliyor
							return t.db.sequelize.query('SELECT * from task_cases where casenumber=:casenumber'
								, { replacements: { casenumber: obj.barcode },type: t.db.sequelize.QueryTypes.SELECT})
								.then((/*results*/)=>{
									let data=JSON.parse('[{"packcapacity":5,"product":"16.14085","lotname":"U180305.0131","casename":"ERM_002","casenumber":"1803050131","erprefnumber":"71944404","caselot":"1/3"},{"packcapacity":5,"product":"16.14085","lotname":"U180305.0131","casename":"ERM_002","casenumber":"1803050132","erprefnumber":"71944404","caselot":"2/3"},{"packcapacity":5,"product":"16.14085","lotname":"U180305.0132","casename":"ERM_002","casenumber":"1803050133","erprefnumber":"71944404","caselot":"3/3"}]');
									let _tmp=[];
									for(let i=0;i<data.length;i++){
										let idx=t.getIndex(_tmp,'casenumber',data[i].casenumber);
										if(idx==-1){
											_tmp.push(data[i]);
										}
									}
									try {
										t.proc.socket_server_device.send({
											event: 'res_refresh_delivery_package_info',
											ip: obj.ip,
											result: 'ok',
											message: 'İşlem tamamlandı.',
											data: _tmp
										});
									} catch (error) {
										Sentry.captureException(error);
									}
									return;
								});
						}else{
							t.log_error('ERP bağlantı yok.'+obj.client+' KD:'+obj.erprefnumber+' KASA:'+obj.barcode+'...');
							try {
								t.proc.socket_server_device.send({
									event: 'res_refresh_delivery_package_info',
									ip: obj.ip,
									result: 'err',
									message: 'ERP bağlantısı sağlanamadı',
									casenumber: obj.barcode
								});
							} catch (error) {
								Sentry.captureException(error);
							}
						}
					}
					break;
				}
				case 'req_activity_control_questions|get':{
					let sql='select * from ( '+
					'	SELECT \'activity_control_questions\' qtype,acq.id,acq.type,acq.opname,acq.question,acq.answertype,null path '+
					' ,case acq.answertype when \'Evet-Hayır\' then case when (acq.valuerequire=\'1\' or acq.valuerequire=\'Evet\') then \'Evet\' else \'Hayır\' end else acq.valuerequire end valuerequire0 '+
					' ,\'\' valuerequire '+
					' ,acq.valuemin,acq.valuemax,acq.documentnumber '+
					' ,case acq.answertype when \'Evet-Hayır\' then \'radio\' else \'text\' end cm_row_type '+
					' ,case acq.answertype when \'Rakam Değer\' then \'numpadex\' when \'Evet-Hayır\' then \'radio\' else \'tr_TR\' end cm_row_layout '+
					' ,case acq.period '+
					' 	when \'Uretim\' then '+
					'			case when exists(select * from activity_control_answers aca where aca.type=acq.type and aca.opname=acq.opname and aca.question=acq.question and aca.day=:day limit 1) then true else false end '+
					'		when \'Hafta\' then '+
					'			case when exists(select * from activity_control_answers aca where aca.type=acq.type and aca.opname=acq.opname and aca.question=acq.question and to_char(current_date, \'YYYY-WW\')=to_char(aca.day, \'YYYY-WW\') limit 1) then true else false end '+
					'		when \'Ay\' then '+
					'			case when exists(select * from activity_control_answers aca where aca.type=acq.type and aca.opname=acq.opname and aca.question=acq.question and to_char(current_date, \'YYYY-MM\')=to_char(aca.day, \'YYYY-MM\') limit 1) then true else false end '+
					'		end is_record_exists '+
					' FROM activity_control_questions acq '+
					' where coalesce(acq.finish,CURRENT_TIMESTAMP(0))>=CURRENT_TIMESTAMP(0) and acq.deleted_at is null and acq.opname in (:opname) '+
					' order by acq.type,acq.start '+
					')acq '+
					' where acq.is_record_exists=false ';
					return t.db.sequelize.query(sql
						, { replacements: { opname: obj.opnames, day:obj.day },type: t.db.sequelize.QueryTypes.SELECT}).then((results)=>{
						if(results.length>0){
							try {
								t.proc.socket_server_device.send({
									event: 'activity_control_questions_response',
									ip: obj.ip,
									data: results
								});
							} catch (error) {
								Sentry.captureException(error);
							}
						}
						return;
					});
				}
				case 'case_movements|get':{
					let sql=`SELECT carrier,code,stockcode,casename,starttime,targettime,erprefnumber,empcode,empname 
					from case_movements 
					where status='WAITFORDELIVERY' and client=:client
					order by starttime`;
					return t.db.sequelize.query(sql,{ replacements:{client:obj.client},type: t.db.sequelize.QueryTypes.SELECT}).then((results)=>{
						t.proc.socket_server_device.send({
							event: 'res|case_movements|get',
							ip: obj.ip,
							client:obj.client,
							data: results
						});
						return;
					});
				}
				//case 'req_documents|get':{
				//	let sql='select * from ( '+
				//	'	select d.* '+
				//	'		,case when exists(select * from document_view_logs dvl where dvl.type=d.type and dvl.path=d.path and dvl.opname=d.opname and dvl.currentversion=d.currentversion and dvl.day=:day and dvl.employee=:employee limit 1) then true else false end is_record_exists'+
				//	'	from documents d '+
				//	'	where d.type=:type and d.opname in (:opname) '+
				//	'	order by d.path '+
				//	' )d '+
				//	' where d.is_record_exists=false';
				//	return t.db.sequelize.query(sql
				//		, { replacements: { type:'SOP', opname: obj.opnames, day:obj.day, employee:obj.employee },type: t.db.sequelize.QueryTypes.SELECT}).then((results)=>{
				//		if(results.length>0){
				//			try {
				//				t.proc.socket_server_device.send({
				//					event: 'documents_response',
				//					ip: obj.ip,
				//					client:obj.client,
				//					day:obj.day,
				//					jobrotation:obj.jobrotation,
				//					opnames:obj.opnames,
				//					employee:obj.employee,
				//					data: results
				//				});
				//			} catch (error) {
				//				Sentry.captureException(error);
				//			}
				//		}
				//		return;
				//	});
				//}
				case 'req_penetrations|get':{
					//console.log('req_penetrations|get');
					//console.log(obj);
					//console.log(obj.opnames);
					let sql='SELECT * from penetrations where timeview is null and opname in (:opname)';
					return t.db.sequelize.query(sql
						, { replacements: { opname: obj.opnames },type: t.db.sequelize.QueryTypes.SELECT}).then((results)=>{
						//console.log(results.length);
						if(results.length>0){
							try {
								let row=results[0];
								//console.log(row);
								t.proc.socket_server_device.send({
									event: 'penetrations_response',
									ip: obj.ip,
									title:(row.title?row.title:'Kaynak Penetrasyonu Test Bildirimi'),
									text:(row.message?row.message:'Üretilen ilk ürünleri Kalite Bölümüne Veriniz')
								});
							} catch (error) {
								Sentry.captureException(error);
							}
							sql='update penetrations set timeview=current_timestamp(0) where timeview is null and opname in (:opname)';
							return t.db.sequelize.query(sql, { replacements: { opname: obj.opnames },type: t.db.sequelize.QueryTypes.UPDATE});
						}
						return;
					});
				}
				case 'res_query_losttype':{
					return t.db.sequelize.query('update client_lost_details '+
						' set losttype=:losttypenew  '+
						' where id in ( '+
						'	SELECT cld1.id  '+
						'	from client_lost_details cld0 '+
						'	left join client_lost_details cld1 on cld1.client=cld0.client and cld1.day=cld0.day and cld1.jobrotation=cld0.jobrotation and cld1.start=cld0.start and cld1.finish=cld0.finish '+
						'	where cld0.client=:client and cld0.record_id=:record_id)'
					, { replacements: { losttypenew:obj.losttype,client:obj.client,record_id:obj.record_id },type: t.db.sequelize.QueryTypes.UPDATE})
						.catch(function(err){
							//console.log('catch-res_query_losttype-'+err);
							Sentry.captureException(err);
						});
				}
				case 'task_plan_employees|get':{
					let sql=`select tpe_all.employee,tpe_all.name,tpe_all.client,tpe_all.opname,tpe_all.erprefnumber,tpe_all.plannedstart,tpe_all.plannedfinish,works.estimatedfinish
						,case when works.estimatedfinish>tpe_all.plannedstart then 'danger' else '' end rowclass
					from (
						SELECT tpe.employee,e.name,tpe.client
							,string_agg(tpe.opname,'|' ORDER BY tpe.id) opname
							,string_agg(tpe.erprefnumber,'|' ORDER BY tpe.id) erprefnumber
							,tpe.plannedstart,tpe.plannedfinish
						from task_plan_employees tpe 
						join (
							SELECT id,code,beginval,endval ,current_date,current_time
							,cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)) "day" 
							,concat(cast(case when beginval>endval and cast(current_time as varchar(5))<=endval then current_date - INTERVAL '1 day' else current_date end as varchar(10)),' ',beginval,':00')::timestamp jr_start 
							,concat(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)),' ',endval,':59')::timestamp +interval '1 second' jr_end 
						from job_rotations 	
						where (finish is null or finish<now())	
							and (	
								(endval>beginval and cast(current_time as varchar(5)) between beginval and endval)	
								or 
								(endval<beginval and (cast(current_time as varchar(5))>=beginval or cast(current_time as varchar(5))<=endval) )	
							)
						)sv on tpe.plannedstart>=sv.jr_start and tpe.plannedfinish<=sv.jr_end
						join employees e on e.code=tpe.employee
						where 1=1 and tpe.employee=:employee
						GROUP BY tpe.employee,e.name,tpe.client,tpe.plannedstart,tpe.plannedfinish
					)tpe_all
					join (
						select cpd_w.client,cast(CURRENT_TIMESTAMP(0) + cast(tl_w.tpp*(tl_w.productcount-tl_w.productdonecount) as int)*interval '1' second as timestamp) estimatedfinish
						from task_lists tl_w
						join(
							SELECT cpd2.client,cpd2.day,cpd2.jobrotation,max(cpd2.tasklist) tasklist
							from client_production_details cpd2 
							join (
								SELECT id,code,beginval,endval ,current_date,current_time
									,cast(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)) as date) "day" 
									,concat(cast(case when beginval>endval and cast(current_time as varchar(5))<=endval then current_date - INTERVAL '1 day' else current_date end as varchar(10)),' ',beginval,':00')::timestamp jr_start 
									,concat(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)),' ',endval,':59')::timestamp +interval '1 second' jr_end 
							from job_rotations 	
							where (finish is null or finish<now())	
								and (	
									(endval>beginval and cast(current_time as varchar(5)) between beginval and endval)	
									or 
									(endval<beginval and (cast(current_time as varchar(5))>=beginval or cast(current_time as varchar(5))<=endval) )	
								)
							)sv on sv.day=cpd2.day
						where cpd2.type='c_p' and cpd2.finish is null
						GROUP BY cpd2.client,cpd2.day,cpd2.jobrotation)cpd_w on cpd_w.tasklist=tl_w.code
					)works on works.client=tpe_all.client`;
					if(obj.employee){
						return t.db.sequelize.query(sql,{ replacements:{employee:obj.employee},type: t.db.sequelize.QueryTypes.SELECT}).then((results)=>{
							t.proc.socket_server_device.send({
								event: 'res|task_plan_employees|get',
								ip: obj.ip,
								client:obj.client,
								data: results
							});
							return;
						});
					}else{
						t.proc.socket_server_device.send({
							event: 'res|task_plan_employees|get',
							ip: obj.ip,
							client:obj.client,
							data: []
						});
						return;
					}
				}
				default:{
					break;
				}
				}
			} catch (error) {
				//console.log(error);
				Sentry.captureException(error);
			}
		};
		let onError_sd = function(e) {
			t.mjd('Error_server_socket_server_device');
			//console.log(e);
			Sentry.captureException(e);
		};
		let onDisconnect_sd = function(e) {
			t.mjd('kill-onDisconnect_socket_server_device');
			if(e>0){
				Sentry.captureMessage('[onClose_ssd] ' + e);
			}
			let b_run=t.proc.socket_server_device&&t.proc.socket_server_device.pid?this.pid===t.proc.socket_server_device.pid:false;
			if(b_run){
				try {
					this.kill();
				} catch (error) {
					//classBase.writeError(error,t.ip,'');
				}
			}
			t.proc.socket_server_device=false;
			setTimeout(() => {
				t.p_socket_server_device();
			}, 1000);
		};
		if(t.isDevMachine){
			t.proc.socket_server_device=cp.fork(path.join(__dirname, '/socket_server_device.js'), [], {windowsHide:true,execArgv: ['--inspect=8141']});
		}else{
			t.proc.socket_server_device=cp.fork(path.join(__dirname, '/socket_server_device.js'), [],{windowsHide:true});
		}
		t.proc.socket_server_device.on('message',onMessage);
		t.proc.socket_server_device.on('error',onError_sd);
		t.proc.socket_server_device.on('disconnect',onDisconnect_sd);
		t.proc.socket_server_device.send({
			event: 'start_server',
			pid: t.proc.socket_server_device.pid,
			pin: t.pin,
			ip:t.ip,
			port: t.cfg_dcs_server_PORT,
			odbc_merga: t.cfg_odbc_merga
		});
	}
	p_socket_server_watch(){
		let t=this;
		let onMessage = function(obj) {
			try {
				switch (obj.event) {
				case 'started_socket_server_watch':{
					t.mjd('socket_server_watch '+obj.port+' Process PID:'+obj.pid);				
					break;
				}
				case 'req_getDataAllClients':{
					try {
						t.proc.socket_server_device.send({
							event: 'req_getDataAllClients',
							ip: obj.ip
						});
					} catch (error) {
						Sentry.captureException(error);
					}
					break;
				}
				case 'lockpc':{
					try {
						t.proc.socket_server_device.send({
							event: 'req_device_is_online',
							parent: obj
						});
					} catch (error) {
						Sentry.captureException(error);
					}
					break;
				}
				case 'unlockpc':{
					try {
						t.proc.socket_server_device.send({
							event: 'req_device_is_online',
							parent: obj
						});
					} catch (error) {
						Sentry.captureException(error);
					}
					break;
				}
				case 'updateApp':{
					try {
						t.proc.socket_server_device.send({
							event: 'req_device_is_online',
							parent: obj
						});
					} catch (error) {
						Sentry.captureException(error);
					}
					break;
				}
				case 'error':{
					Sentry.captureException(obj.err);
					//console.log(obj.err);
					this.kill();
					break;
				}
				default:{
					break;
				}
				}
			} catch (error) {
				Sentry.captureException(error);
			}
		};
		let onError_sw = function(e) {
			//console.log(e);
			Sentry.captureException(e);
		};
		let onDisconnect_sw = function(e) {
			t.mjd('kill-onDisconnect_socket_server_watch');
			if(e>0){
				Sentry.captureMessage('[onClose_ssw] ' + e);
			}
			let b_run=t.proc.socket_server_watch&&t.proc.socket_server_watch.pid?this.pid===t.proc.socket_server_watch.pid:false;
			if(b_run){
				try {
					this.kill();
				} catch (error) {
					//classBase.writeError(error,t.ip,'');
				}
			}
			t.proc.socket_server_watch=false;
			setTimeout(() => {
				t.p_socket_server_watch();
			}, 1000);
		};
		if(t.isDevMachine){
			t.proc.socket_server_watch=cp.fork(path.join(__dirname, '/socket_server_watch.js'), [], {windowsHide:true,execArgv: ['--inspect=8142']});
		}else{
			t.proc.socket_server_watch=cp.fork(path.join(__dirname, '/socket_server_watch.js'), [],{windowsHide:true});
		}
		t.proc.socket_server_watch.on('message',onMessage);
		t.proc.socket_server_watch.on('error',onError_sw);
		t.proc.socket_server_watch.on('disconnect',onDisconnect_sw);
		t.proc.socket_server_watch.send({
			event: 'start_server',
			pid: t.proc.socket_server_watch.pid,
			pin: t.pin,
			ip:t.ip,
			port: parseInt(t.cfg_dcs_server_PORT)+5
		});
	}
	p_data_importer(){
		let t=this;
		let onMessage = function(obj) {
			try {
				switch (obj.event) {
				case 'data_importer_started':{
					t.mjd('data_importer Process PID:'+obj.pid);				
					break;
				}
				case 'data_imported_material_prepare':{
					if(t.proc.material_prepare){
						try {
							t.proc.material_prepare.send(obj);
						} catch (error) {
							Sentry.captureException(error);
						}
					}
					break;
				}
				case 'imported_api_call':{
					if(t.proc.socket_server_device){
						try {
							let idx=t.getIndex(t.clients,'code',obj.data.client);
							if(idx>-1){
								try {
									t.proc.socket_server_device.send({
										event: 'imported_api_call',
										ip: t.clients[idx].ip,
										data:obj
									});
								} catch (error) {
									Sentry.captureException(error);
								}
							}else{
								fs.unlinkSync(obj.path);
							}
						} catch (error) {
							Sentry.captureException(error);
						}
					}
					break;
				}
				case 'import_info_message':{
					//console.log(obj.event+'-'+obj.fn);
					//console.log(obj.data);
					break;
				}
				default:{
					break;
				}
				}
			} catch (error) {
				Sentry.captureException(error);
			}
		};
		let onError_di = function(e) {
			t.mjd('Error_server_p_data_importer');
			//console.log(e);
			Sentry.captureException(e);
		};
		let onDisconnect_di = function(e) {
			t.mjd('kill-onDisconnect_data_importer');
			if(e>0){
				Sentry.captureMessage('[onClose_data_importer] ' + e);
			}
			let b_run=t.proc.data_importer&&t.proc.data_importer.pid?this.pid===t.proc.data_importer.pid:false;
			if(b_run){
				try {
					this.kill();
				} catch (error) {
					//classBase.writeError(error,t.ip,'');
				}
			}
			t.proc.data_importer=false;
			setTimeout(() => {
				t.p_data_importer();
			}, 1000);
		};
		if(t.isDevMachine){
			t.proc.data_importer=cp.fork(path.join(__dirname, '/data_importer.js'), [], {windowsHide:true,execArgv: ['--inspect=8143']});
		}else{
			t.proc.data_importer=cp.fork(path.join(__dirname, '/data_importer.js'), [],{windowsHide:true});
		}
		t.proc.data_importer.on('message',onMessage);
		t.proc.data_importer.on('error',onError_di);
		t.proc.data_importer.on('disconnect',onDisconnect_di);
		t.proc.data_importer.send({
			event: 'start_data_importer',
			pid: t.proc.data_importer.pid,
			customer: t.cfg_import_customer,
			app: t.cfg_import_app,
			mh_importpath: t.cfg_mh_importpath
		});
	}
	p_data_exporter(){
		let t=this;
		let onMessage = function(obj) {
			try {
				switch (obj.event) {
				case 'data_exporter_started':{
					t.mjd('data_exporter Process PID:'+obj.pid+' Exp:'+obj.export_data+' L:'+obj.b_e_lost+' P:'+obj.b_e_prod+' R:'+obj.b_e_reject+' Path:'+obj.path);				
					break;
				}
				case 'message':{
					t.mjd('data_exporter message fn:'+obj.fn+' data:'+obj.data);				
					break;
				}
				case 'error':{
					//this.kill();
					break;
				}
				default:{
					break;
				}
				}
			} catch (error) {
				Sentry.captureException(error);
			}
		};
		let onError_de = function(e) {
			t.mjd('Error_server_p_data_exporter');
			//console.log(e);
			Sentry.captureException(e);
		};
		let onDisconnect_de = function(e) {
			t.mjd('kill-onDisconnect_data_exporter');
			if(e>0){
				Sentry.captureMessage('[onClose_data_exporter] ' + e);
			}
			let b_run=t.proc.data_exporter&&t.proc.data_exporter.pid?this.pid===t.proc.data_exporter.pid:false;
			if(b_run){
				try {
					this.kill();
				} catch (error) {
					//classBase.writeError(error,t.ip,'');
				}
			}
			t.proc.data_exporter=false;
			setTimeout(() => {
				t.p_data_exporter();
			}, 1000);
		};
		if(t.isDevMachine){
			t.proc.data_exporter=cp.fork(path.join(__dirname, '/data_exporter.js'), [], {windowsHide:true,execArgv: ['--inspect=8144']});
		}else{
			t.proc.data_exporter=cp.fork(path.join(__dirname, '/data_exporter.js'), [],{windowsHide:true});
		}
		t.proc.data_exporter.on('message',onMessage);
		t.proc.data_exporter.on('error',onError_de);
		t.proc.data_exporter.on('disconnect',onDisconnect_de);
		t.proc.data_exporter.send({
			event: 'start_data_exporter',
			param: t.db.sequelize.options,
			ip: t.ip,
			port:t.cfg_dcs_server_PORT,
			pid: t.proc.data_exporter.pid,
			export_data: t.cfg_export_data,
			customer: t.cfg_import_customer,
			odbc_merga: t.cfg_odbc_merga
		});
	}
	p_rabbitmq_send(){
		let t=this;
		let onMessage_rabbitmq = function(obj) {
			try {
				switch (obj.event) {
				case 'cp_server_inited':{
					t.mjd('rabbitmq_send Process PID:'+obj.pid);
					break;
				}
				case 'set_clients':{
					try {
						t.clients=obj.data;
						t.proc.socket_server_device.send({
							event: 'set_clients',
							data: obj.data
						});
					} catch (error) {
						Sentry.captureException(error);
					}
					break;
				}
				case 'query_losttype':{
					try {
						t.proc.socket_server_device.send({
							event: obj.event,
							client:obj.client,
							ip:obj.ip,
							record_id:obj.record_id
						});
					} catch (error) {
						Sentry.captureException(error);
					}
					break;
				}
				default:{
					break;
				}
				}
			} catch (error) {
				Sentry.captureException(error);
			}
		};
		let onError_rabbitmq = function(e) {
			t.mjd('Error_p_rabbitmq_send');
			//console.log(e);
			Sentry.captureException(e);
		};
		let onDisconnect_rabbitmq = function(e) {
			t.mjd('kill-onDisconnect_p_rabbitmq_send');
			if(e>0){
				Sentry.captureMessage('[onClose_rabbitmq_send] ' + e);
			}
			let b_run=t.proc.rabbitmq_send&&t.proc.rabbitmq_send.pid?this.pid===t.proc.rabbitmq_send.pid:false;
			if(b_run){
				try {
					this.kill();
				} catch (error) {
					//classBase.writeError(error,t.ip,'');
				}
			}
			t.proc.rabbitmq_send=false;
			setTimeout(() => {
				t.p_rabbitmq_send();
			}, 1000);
		};
		if(t.isDevMachine){
			t.proc.rabbitmq_send=cp.fork(path.join(__dirname, '/rabbitmq_send.js'), [], {windowsHide:true,execArgv: ['--inspect=8145']});
		}else{
			t.proc.rabbitmq_send=cp.fork(path.join(__dirname, '/rabbitmq_send.js'), [],{windowsHide:true});
		}
		t.proc.rabbitmq_send.on('message',onMessage_rabbitmq);
		t.proc.rabbitmq_send.on('error',onError_rabbitmq);
		t.proc.rabbitmq_send.on('disconnect',onDisconnect_rabbitmq);
		t.proc.rabbitmq_send.send({
			event: 'init_server',
			param: t.db.sequelize.options,
			ip: t.ip,
			port:t.cfg_dcs_server_PORT,
			customer: t.cfg_import_customer,
			pid: t.proc.rabbitmq_send.pid,
			export_fault: t.cfg_export_fault,
			odbc_merga: t.cfg_odbc_merga,
			odbc_boys: t.cfg_odbc_boys,
			odbc_boys_prod: t.cfg_odbc_boys_prod,
			material_prepare: t.cfg_material_prepare
		});
	}
	p_rabbitmq_receive(){
		let t=this;
		let onMessage_rabbitmq = function(obj) {
			try {
				switch (obj.event) {
				case 'cp_server_inited':{
					t.mjd('rabbitmq_receive Process PID:'+obj.pid);
					break;
				}
				case 'watch_calc_tempo':{
					try {
						t.proc.socket_server_device.send({
							event: obj.event,
							data:obj.data,
							queue:obj.queue
						});
					} catch (error) {
						//
					}
					break;
				}
				case 'watch_info':{
					try {
						t.proc.socket_server_device.send({
							event: obj.event,
							data:obj.data,
							queue:obj.queue
						});
					} catch (error) {
						Sentry.captureException(error);
					}
					break;
				}
				case 'watch_input_signal':{
					try {
						t.proc.socket_server_device.send({
							event: obj.event,
							data:obj.data,
							queue:obj.queue
						});
					} catch (error) {
						Sentry.captureException(error);
					}
					break;
				}
				case 'write_verimotkd':{
					let _erprefnumber=obj.erprefnumber;
					if (fs.existsSync(path_export+'verimotkd/')) {
						fs.writeFile(path_export+'verimotkd/'+_erprefnumber+'.txt', _erprefnumber + '\r\n', function(err) {
							if(err) {
								return;//console.log(err);
							}
						}); 
					}
					break;
				}
				case 'device_io_info':{
					try {
						t.proc.socket_server_device.send({
							event: obj.event,
							data:obj.data,
							queue:obj.queue
						});
					} catch (error) {
						Sentry.captureException(error);
					}
					break;
				}
				case 'device_state_change':{
					// @@todo: burada bir kuyruğa alınıp, export varsa gönder yapılacak
					try {
						t.proc.data_exporter.send({
							event: obj.event,
							data:obj.data
						});
					} catch (error) {
						//
					}
					break;
				}
				case 'sync_message':{
					console.log('sync_message',obj.data);
					break;
				}
				default:{
					break;
				}
				}
			} catch (error) {
				Sentry.captureException(error);
			}
		};
		let onError_rabbitmq = function(e) {
			t.mjd('Error_p_rabbitmq_receive');
			//console.log(e);
			Sentry.captureException(e);
		};
		let onDisconnect_rabbitmq = function(e) {
			t.mjd('kill-onDisconnect_p_rabbitmq_receive');
			if(e>0){
				Sentry.captureMessage('[onClose_rabbitmq_receive] ' + e);
			}
			let b_run=t.proc.rabbitmq_receive&&t.proc.rabbitmq_receive.pid?this.pid===t.proc.rabbitmq_receive.pid:false;
			if(b_run){
				try {
					this.kill();
				} catch (error) {
					//classBase.writeError(error,t.ip,'');
				}
			}
			t.proc.rabbitmq_receive=false;
			setTimeout(() => {
				t.p_rabbitmq_receive();
			}, 1000);
		};
		if(t.isDevMachine){
			t.proc.rabbitmq_receive=cp.fork(path.join(__dirname, '/rabbitmq_receive.js'), [], {windowsHide:true,execArgv: ['--inspect=8146']});
		}else{
			t.proc.rabbitmq_receive=cp.fork(path.join(__dirname, '/rabbitmq_receive.js'), [],{windowsHide:true});
		}
		t.proc.rabbitmq_receive.on('message',onMessage_rabbitmq);
		t.proc.rabbitmq_receive.on('error',onError_rabbitmq);
		t.proc.rabbitmq_receive.on('disconnect',onDisconnect_rabbitmq);
		t.proc.rabbitmq_receive.send({
			event: 'init_server',
			param: t.db.sequelize.options,
			ip: t.ip,
			port:t.cfg_dcs_server_PORT,
			pid: t.proc.rabbitmq_receive.pid,
			export_fault: t.cfg_export_fault,
			odbc_merga: t.cfg_odbc_merga,
			odbc_boys: t.cfg_odbc_boys,
			odbc_boys_prod: t.cfg_odbc_boys_prod
		});
	}
	p_oee_calculate(){
		let t=this;
		let onMessage_oc = function(obj) {
			try {
				switch (obj.event) {
				case 'cp_oee_calculate_inited':{
					t.mjd('OEE Calculate Process PID:'+obj.pid);
					break;
				}
				case 'calculated_oee':{
					let data=obj.data;
					if(t.proc.socket_server_device){
						let idx=t.getIndex(t.clients,'code',data.client);
						if(idx>-1){
							try {
								t.proc.socket_server_device.send({
									event: 'calculated_oee',
									ip: t.clients[idx].ip,
									data:data
								});
							} catch (error) {
								Sentry.captureException(error);
							}
						}
					}
					break;
				}
				case 'calculated_oee_all':{
					let data=obj.data;
					if(t.proc.socket_server_watch){
						try {
							t.proc.socket_server_watch.send({
								event: 'calculated_oee',
								data: data
							});
						} catch (error) {
							Sentry.captureException(error);
						}
					}
					break;
				}
				default:{
					break;
				}
				}
			} catch (error) {
				Sentry.captureException(error);
			}
		};
		let onError_oc = function(e) {
			//console.log(e);
			Sentry.captureException(e);
		};
		let onDisconnect_oc = function(e) {
			t.mjd('kill-onDisconnect_p_oee_calculate');
			if(e>0){
				Sentry.captureMessage('[onClose_oee_calculate] ' + e);
			}
			let b_run=t.proc.oee_calculate&&t.proc.oee_calculate.pid?this.pid===t.proc.oee_calculate.pid:false;
			if(b_run){
				try {
					this.kill();
				} catch (error) {
					//classBase.writeError(error,t.ip,'');
				}
			}
			t.proc.oee_calculate=false;
			setTimeout(() => {
				t.p_oee_calculate();
			}, 1000);
		};
		if(t.isDevMachine){
			t.proc.oee_calculate=cp.fork(path.join(__dirname, '/oee_calculate.js'), [], {windowsHide:true,execArgv: ['--inspect=8147']});
		}else{
			t.proc.oee_calculate=cp.fork(path.join(__dirname, '/oee_calculate.js'), [],{windowsHide:true});
		}
		t.proc.oee_calculate.on('message',onMessage_oc);
		t.proc.oee_calculate.on('error',onError_oc);
		t.proc.oee_calculate.on('disconnect',onDisconnect_oc);
		t.proc.oee_calculate.send({
			event: 'init_oee_calculate',
			param: t.db.sequelize.options,
			pid: t.proc.oee_calculate.pid
		});
	}
	p_material_prepare(){
		let t=this;
		let onMessage_mp = function(obj) {
			try {
				switch (obj.event) {
				case 'cp_material_prepare_inited':{
					t.mjd('material_prepare Process PID:'+obj.pid);
					break;
				}
				case 'started_material_prepare':{
					t.mjd('socket_server_material_prepare '+obj.port+' Process PID:'+obj.pid);
					break;
				}
				default:{
					break;
				}
				}
			} catch (error) {
				Sentry.captureException(error);
			}
		};
		let onError_mp = function(e) {
			//console.log(e);
			Sentry.captureException(e);
		};
		let onDisconnect_mp = function(e) {
			t.mjd('kill-onDisconnect_p_material_prepare');
			if(e>0){
				Sentry.captureMessage('[onClose_material_prepare] ' + e);
			}
			let b_run=t.proc.material_prepare&&t.proc.material_prepare.pid?this.pid===t.proc.material_prepare.pid:false;
			if(b_run){
				try {
					this.kill();
				} catch (error) {
					//classBase.writeError(error,t.ip,'');
				}
			}
			t.proc.material_prepare=false;
			setTimeout(() => {
				t.p_material_prepare();
			}, 1000);
		};
		if(t.isDevMachine){
			t.proc.material_prepare=cp.fork(path.join(__dirname, '/material_prepare.js'), [], {windowsHide:true,execArgv: ['--inspect=8148']});
		}else{
			t.proc.material_prepare=cp.fork(path.join(__dirname, '/material_prepare.js'), [],{windowsHide:true});
		}
		t.proc.material_prepare.on('message',onMessage_mp);
		t.proc.material_prepare.on('error',onError_mp);
		t.proc.material_prepare.on('disconnect',onDisconnect_mp);
		t.proc.material_prepare.send({
			event: 'init_material_prepare',
			param: t.db.sequelize.options,
			pid: t.proc.material_prepare.pid,
			ip: t.ip,
			port: parseInt(t.cfg_dcs_server_PORT)+10
		});
	}
	control_sync_update_records(){
		let t=this;
		let _promises=[];
		let item;
		if(t.arr_sync_update_records.length>0){
			item=t.arr_sync_update_records.shift();
			if(item.tablename!=='case_labels'){
				_promises.push(t.sync_update_records(item.uuid,item.ip,item.tablename,item.updated,item.w,item.queue));
			}
		}
		if(_promises.length>0){
			return Promise.all(_promises).then(()=>{
				setTimeout(() => {
					t.control_sync_update_records();
				}, 50);
			});
		}else{
			setTimeout(() => {
				t.control_sync_update_records();
			}, 50);
		}
	}
	sync_update_records(_uuid,_ip,_tablename,_updated,_w,queue){
		let t=this;
		return t.db.sequelize.query('SELECT * FROM '+_tablename+' WHERE "updated_at">\''+_updated+'\' '+_w+' order by "updated_at" asc '
			, { type: t.db.sequelize.QueryTypes.SELECT})
			.then(function(results) {
				let _promises=[];
				// let _len=results.length;
				for (let i = 0; i < results.length; i++) {
					let row=results[i];
					let obj={};
					let optype=(t.convertFullLocalDateString(row['updated_at'])!=t.convertFullLocalDateString(row['created_at'])&&_updated.split(' ')[0]!=='2000-01-01')?'update':'insert';
					let nlist=['id','create_user_id','created_at','update_user_id','updated_at','delete_user_id','deleted_at','version'];
					for(let prop in row){
						if(nlist.indexOf(prop)==-1) {
							if(row[prop]!=null&&typeof row[prop]=='object'&&typeof row[prop].toLocaleTimeString=='function'){
								obj[prop]=t.convertFullLocalDateString(row[prop]);
							}else{
								obj[prop]=row[prop];
							}
						}else{
							if(prop=='created_at'){
								obj['createdAt']=t.convertFullLocalDateString(row[prop]);
							}
							if(prop=='updated_at'){
								obj['updatedAt']=t.convertFullLocalDateString(row[prop]);
							}
						}
					}
					let _mes=new dcs_message({
						type:'sync_message'
						,ack:false
						,data:{tableName:_tablename,procType:optype,data:obj,id:null,_cur:(i+1)}
					});
					let obj_mes=_mes.getdata();
					obj_mes.uuid=_uuid;
					obj_mes.ip=queue?queue:_ip;
					obj_mes.tryCount=20;
					_promises.push(t.db._sync_messages.create(obj_mes));
				}
				return Promise.all(_promises).then(function(){
					//if(_len>0){
					//	console.log('s_u_r-'+_ip+'-'+_tablename+'-'+_updated+'-'+queue+':'+_len);
					//}
					return null;
				}).catch(function(err){
					//console.log('catch-sync_update_records-'+err);
					//console.log('_uuid:'+_uuid+' _ip:'+_ip+' _tablename:'+_tablename+' _updated:'+_updated);
					Sentry.captureException(err);
				});
			})
			.catch(function(err){
				//console.log('catch-sync_update_records-1-'+err);
				Sentry.captureException(err);
			});
	}
	updateControl(){
		let t=this;
		let p = __dirname+'/../update';
		dir.files(p, function(err, files) {
			if (err) throw err;
			files.sort();
			files = files.filter(function (file) {
				return file.match(/.zip$/)!==null;
			});
			t.updates=[];
			for(let i=0;i<files.length;i++){
				let _file=files[i].split('\\').join('/');
				if(_file.indexOf(t.updatePrefix)>-1){
					let _v=_file.split('/update/'+t.updatePrefix)[1].replace('.zip','').split('.');
					let __name='/update/'+t.updatePrefix+parseInt(_v[0])+'.'+parseInt(_v[1])+'.'+parseInt(_v[2])+'.'+parseInt(_v[3])+'.zip';
					t.updates.push({path:__name,_major:parseInt(_v[0]),_minor:parseInt(_v[1]),_patch:parseInt(_v[2]),_build:parseInt(_v[3]),hash:false,file:_file});
					let shasum = crypto.createHash('sha256');
					let s = fs.ReadStream(_file);
					s.on('error',function(e){
						//console.log(e);
					});
					s.on('data', function(d) { shasum.update(d); });
					s.on('end', function() {
						let d = shasum.digest('hex');
						let idx=t.getIndex(t.updates,'file',_file);
						if(idx>-1){
							t.updates[idx].hash=d;
						}
					});
				}
			}
			setTimeout(() => {
				t.updateControl();
			}, 60000);
		});
	}

}
module.exports=dcs_server;