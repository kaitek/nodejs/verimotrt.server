@echo off
if exist version.txt (
    for /F "delims=" %%i in (version.txt) do (  
		set Build=%%i
		goto BREAK1
	)
	:BREAK1
	echo Build number-old:%Build%
	set /A NBuild=%Build%+1
	echo Build number-new:%NBuild%
	(echo %NBuild%) > version.txt
	git add .
	git commit -am "Build Number:%NBuild% Commit"
	git push
	if exist "D:\_update" rmdir /s /q D:\_update
	if not exist "D:\_update" mkdir D:\_update
	git show --pretty=format: --name-only > temp.txt
	for /F %%a in (temp.txt) do (  
		rem echo %~dp0%%a
		echo f | xcopy /s /e /f "%~dp0%%a" "D:/_update/%%a"
	)
	echo f | xcopy /s /e /f "%~dp0version.txt" "D:/_update/version.txt"
	rem echo %~dp0
	del temp.txt
	for /d %%X in (D:/_update/) do "c:\Program Files\7-Zip\7z.exe" a "verimotRT-Server_%NBuild%.zip" "%%X\"
	rmdir /s /q D:\_update
) else (
	@echo off
	echo version.txt yok
)