var domain=require('domain');
var server = require('./lib');
var _domain = domain.create();
_domain.on('error', function(er){
	console.error('Error: ', er);
});
_domain.on('uncaughtException', function(err) {
	console.log(err);
});
_domain.run(function(){
	server.init();
});