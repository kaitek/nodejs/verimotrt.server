const https = require("https");
const querystring = require("querystring");

function getUrl(data,date){
  const postdata = querystring.stringify(data);
  let options = {
    host: 'cdserver-e9xxk.ondigitalocean.app',
    path: '/sahince/searchDoc?$searchString=modified>='+date,
    method: 'GET',
    headers: {
      'Content-Type':'application/x-www-form-urlencoded'
      ,'Content-Length': Buffer.byteLength(postdata, 'utf8')
    }
  };
  return new Promise((resolve, reject) => {
    const req = https.request(options, res => {
      let rawData = ''
      res.setEncoding('utf8');
      res.on('data', chunk => (rawData += chunk));
      res.on('end', () => {
        try {
          if (res.statusCode !== 200) {
            return reject(
              new Error(`Error getting current NPM version: ${res.statusCode} - ${rawData}`)
            );
          }
          const data = JSON.parse(rawData);
          resolve(data);
        } catch (e) {
          reject(e);
        }
      });
    });

    req.on('error', error => {
      reject(error);
    });
    // console.log('postdata',postdata);
    req.write(postdata);
    req.end();
  })
  .then(async (resp)=>{
    console.log('resp:',resp);
  })
  .catch((err) => {
    return -1;
  });
}
return getUrl({username:"Gurkan.ture@sahince.com.tr",password:"enoviaV2"},"2021-06-21");