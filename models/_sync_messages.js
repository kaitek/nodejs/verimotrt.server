'use strict';
module.exports = function(sequelize, DataTypes) {
  var _sync_messages = sequelize.define('_sync_messages', {
    ip: DataTypes.STRING,
    uuid: DataTypes.STRING(50),
    messageId: DataTypes.STRING,
    tryCount: DataTypes.INTEGER,
    type: DataTypes.STRING(50),
    ack: DataTypes.BOOLEAN,
    queued: DataTypes.INTEGER,
    data: DataTypes.JSON
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return _sync_messages;
};