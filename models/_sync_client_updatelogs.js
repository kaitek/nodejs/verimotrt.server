'use strict';
module.exports = function(sequelize, DataTypes) {
  var _sync_client_updatelogs = sequelize.define('_sync_client_updatelogs', {
    ip: DataTypes.STRING,
    uuid: DataTypes.STRING(50),
    tableName: DataTypes.STRING(100),
    lastUpdate: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return _sync_client_updatelogs;
};